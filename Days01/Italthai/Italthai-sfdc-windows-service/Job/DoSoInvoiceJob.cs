﻿using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Italthai_sfdc_windows_service.Job
{
    public class DoSoInvoiceJob : IJob
    {
        private string master = ConfigurationManager.AppSettings["LoaderPath"] 
            + ConfigurationManager.AppSettings["DoSoInvoiceJob"];

        public void Execute(IJobExecutionContext context)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "=====  Start Run Data Loader");
            try
            {
                RunDataLoader();
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e.Message);
            }

        }
        private void RunDataLoader()
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo(master);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            // process.WaitForExit();


            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();
            exitCode = process.ExitCode;
            logger.Log(LogLevel.Info, "--------Do So Invoice  Status  -------------");
            logger.Log(LogLevel.Info, "    >>>> Process Output ");
            logger.Log(LogLevel.Info, output);
            logger.Log(LogLevel.Info, "    >>>> Process Error  ");
            logger.Log(LogLevel.Error, error);
        }
    }
}

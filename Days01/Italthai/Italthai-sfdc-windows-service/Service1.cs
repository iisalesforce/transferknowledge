﻿using Italthai_sfdc_windows_service.Job;
using NLog;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace Italthai_sfdc_windows_service
{
    public partial class Service1 : ServiceBase
    {
        private ISchedulerFactory schedFact;
        private IScheduler sched;

        private ITrigger trigger;
        private IJobDetail job;

        private ITrigger trigger2;
        private IJobDetail job2;

        private ITrigger trigger3;
        private IJobDetail job3;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {


            string serviceCron = ConfigurationManager.AppSettings["UpdateStatusJobCron"];
            string serviceCron2 = ConfigurationManager.AppSettings["DoSoInvoiceJobCron"];
            string serviceCron3 = ConfigurationManager.AppSettings["SyncAccountJobCron"];

            Logger logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "Service Start");

            schedFact = new StdSchedulerFactory();
            sched = schedFact.GetScheduler();
            sched.Start();

            //################################# Job Initial ###########################################

            JobDataMap jobmap = new JobDataMap();
            JobDataMap jobmap2 = new JobDataMap();
            JobDataMap jobmap3 = new JobDataMap();
            //jobmap.Add("Form", this);
            job = JobBuilder.Create<UpdateStatusJob>()
                                    .WithIdentity("myJob", "group1") // name "myJob", group "group1"
                                    .UsingJobData(jobmap)
                                    .Build();
       
            trigger = TriggerBuilder.Create().WithIdentity("myTrigger", "group1")
                .WithCronSchedule(serviceCron)
               .Build();


            
            //jobmap.Add("Form", this);
            job2 = JobBuilder.Create<DoSoInvoiceJob>()
                                    .WithIdentity("myJob2", "group1") // name "myJob", group "group1"
                                    .UsingJobData(jobmap2)
                                    .Build();
         
            trigger2 = TriggerBuilder.Create().WithIdentity("myTrigger2", "group1")
                .WithCronSchedule(serviceCron2)
               .Build();

            
            //jobmap.Add("Form", this);
            job3 = JobBuilder.Create<SyncAccount>()
                                    .WithIdentity("myJob3", "group1") // name "myJob", group "group1"
                                    .UsingJobData(jobmap3)
                                    .Build();
            trigger3 = TriggerBuilder.Create().WithIdentity("myTrigger3", "group1")
                .WithCronSchedule(serviceCron3)
               .Build();




            // Tell quartz to schedule the job using our trigger
            sched.ScheduleJob(job, trigger);
            sched.ScheduleJob(job2, trigger2);
            sched.ScheduleJob(job3, trigger3);

        }

        protected override void OnStop()
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            try
            {
                sched.DeleteJob(job.Key);
                sched.UnscheduleJob(trigger.Key);

                sched.DeleteJob(job2.Key);
                sched.UnscheduleJob(trigger2.Key);


                sched.DeleteJob(job3.Key);
                sched.UnscheduleJob(trigger3.Key);


            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e.Message);
            }


            logger.Log(LogLevel.Info, "Service Stop");
        }
        public void OnDebug()
        {
            OnStart(null);
        }
    }
}

SET DLPATH="D:\Dev\SalesforceService\Data Loader"
SET DLCONF="D:\Dev\SalesforceService\Data Loader\cliq_process\AccUp\config"
SET DLDATA="D:\Dev\SalesforceService\Data Loader\cliq_process\AccUp\write"
call %DLPATH%\Java\bin\java.exe -cp %DLPATH%\* -Dsalesforce.config.dir=%DLCONF% com.salesforce.dataloader.process.ProcessRunner process.name=AccUp
REM To rotate your export files, uncomment the line below
REM copy %DLDATA%\AccUp.csv %DLDATA%\%date:~10,4%%date:~7,2%%date:~4,2%-%time:~0,2%-AccUp.csv

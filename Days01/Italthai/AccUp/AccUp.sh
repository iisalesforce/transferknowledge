#!/bin/sh
export DLPATH="D:\Dev\SalesforceService\Data Loader"
export DLCONF="D:\Dev\SalesforceService\Data Loader\cliq_process\AccUp\config"
java -cp "$DLPATH/*" -Dsalesforce.config.dir=$DLCONF com.salesforce.dataloader.process.ProcessRunner process.name=AccUp

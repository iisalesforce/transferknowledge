#!/bin/sh
export DLPATH="D:\Dev\SalesforceService\Data Loader"
export DLCONF="D:\Dev\SalesforceService\Data Loader\cliq_process\Ex_Account\config"
java -cp "$DLPATH/*" -Dsalesforce.config.dir=$DLCONF com.salesforce.dataloader.process.ProcessRunner process.name=Ex_Account

SET DLPATH="D:\Dev\SalesforceService\Data Loader"
SET DLCONF="D:\Dev\SalesforceService\Data Loader\cliq_process\Ex_Account\config"
SET DLDATA="D:\Dev\SalesforceService\Data Loader\cliq_process\Ex_Account\write"
call %DLPATH%\Java\bin\java.exe -cp %DLPATH%\* -Dsalesforce.config.dir=%DLCONF% com.salesforce.dataloader.process.ProcessRunner process.name=Ex_Account
REM To rotate your export files, uncomment the line below
REM copy %DLDATA%\Ex_Account.csv %DLDATA%\%date:~10,4%%date:~7,2%%date:~4,2%-%time:~0,2%-Ex_Account.csv

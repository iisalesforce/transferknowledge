﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Italthai_sfdc_services.IService
{
    public interface ICustomerService
    {
        Boolean IsExistCustomer(string customerCode);
        int CreateNewCustomer(string customerCode, string customerName, string organizationName);
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Monitor.aspx.cs" Inherits="Italthai_sfdc_services.Monitor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<style>
    
    </style>

<body>
    <div style="width: 960px; margin: auto">
        <form id="form1" runat="server">
            Customer Number :
        <asp:TextBox ID="txtCusNo" runat="server"></asp:TextBox>
            <br />
            <br />
            Check Result :
        <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
            <br />
            <br />
            <asp:Button ID="btnOk" runat="server" Text="Check Is Exist" OnClick="btnOk_Click" />

            <br />
            <br />
            VIEW LOG<br />
            <br />
            <asp:DropDownList ID="ddlFile" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFile_SelectedIndexChanged">
            </asp:DropDownList>
            <br />
            <asp:GridView Width="100%" ID="gdvLog" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" />
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
            <br />
            &nbsp;&nbsp;
        <asp:Button ID="btnLogout" runat="server" Text="Log Out" OnClick="btn_Logout" />

        </form>
    </div>
</body>
</html>

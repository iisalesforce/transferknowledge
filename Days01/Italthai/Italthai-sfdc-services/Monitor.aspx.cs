﻿using Italthai_sfdc_services.IService;
using Italthai_sfdc_services.Model;
using Italthai_sfdc_services.ServiceImpl;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Italthai_sfdc_services
{
    public partial class Monitor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // ICustomerService cusService = new CustomerService();
            //var result =  cusService.IsExistCustomer("47077");
            Logger logger = LogManager.GetCurrentClassLogger();
            // logger.Log(LogLevel.Info, " TEST INFO");


            if (!IsPostBack)
            {

                string logPath = Server.MapPath("~/logs/");
                System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(logPath);
                IEnumerable<System.IO.FileInfo> fileList = dir.GetFiles("*.*", System.IO.SearchOption.AllDirectories);

                var files = from f in fileList
                            orderby f.LastWriteTime descending
                            select f.Name;

                int limit = 0;
                if (files.Count() > 0)
                {
                    foreach (var filename in files)
                    {
                        ddlFile.Items.Add(new ListItem(filename, filename));
                        limit++;
                        if (limit == 60) break;
                    }
                    ddlFile.SelectedIndex = 0;
                    //   Read Data 


                    var fileRead = logPath + ddlFile.SelectedValue;

                    var input = File.ReadLines(fileRead).Where(s => s != string.Empty && !s.StartsWith(" "));

                    List<LogData> Log = new List<LogData>();

                    foreach (var item in input)
                    {



                        string[] data = item.Split(',');
                        Log.Add(new LogData
                        {
                            DateTime = data[0],
                            Info = data[1],
                            Message = data[2]

                        });
                    }




                    gdvLog.DataSource = Log;
                    gdvLog.DataBind();
                }

            }
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, " Search Customer Id");

            ICustomerService cusService = new CustomerService();
            var result = cusService.IsExistCustomer(txtCusNo.Text);
            lblName.Text = result.ToString();




        }

        protected void btn_Logout(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("Logon.aspx");

        }

        protected void ddlFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshLog();

        }


        private void RefreshLog()
        {
            if (!String.IsNullOrEmpty(ddlFile.SelectedValue))
            {
                string fileRead = Server.MapPath("~/logs/") + ddlFile.SelectedValue;


                if (File.Exists(fileRead))
                {

                    var input = File.ReadLines(fileRead).Where(s => s != string.Empty && !s.StartsWith(" "));

                    List<LogData> Log = new List<LogData>();

                    foreach (var item in input)
                    {



                        string[] data = item.Split(',');
                        Log.Add(new LogData
                        {
                            DateTime = data[0],
                            Info = data[1],
                            Message = data[2]

                        });
                    }

                    gdvLog.DataSource = Log;
                    gdvLog.DataBind();
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SFDCV1;
using NLog;
using Italthai_sfdc_services.ServiceImpl;
using Italthai_sfdc_services.IService;
namespace Italthai_sfdc_services.AR
{

    public class AccountNotificationServiceImpl : INotificationBinding
    {
        public AccountNotificationServiceImpl()
        {

        }
        #region INotificationBinding Members

        public SFDCV1.notificationsResponse notifications(notifications notifications1)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "SFDC Notification Recieve");
            notificationsResponse response = new notificationsResponse();
            response.Ack = true;
            ICustomerService cusService = new CustomerService();

            AccountNotification[] accounts = notifications1.Notification;
            for (int i = 0; i < accounts.Length; i++)
            {
                AccountNotification notification = accounts[i];
                Account account = (Account)notification.sObject;
                logger.Log(LogLevel.Info, "Notification " + account.Customer_Code__c);

                Boolean result = cusService.IsExistCustomer(account.Customer_Code__c);
                logger.Log(LogLevel.Info, "Check Is Exist : " + result);
                if (false == result) // เมื่อไม่มีใน ERP จึงสร้าง
                {
                    try
                    {
                        int code = cusService.CreateNewCustomer(account.Customer_Code__c, account.Name, account.LastModifiedById/* Salesforce User Id That Make Modify To Erp*/);
                        logger.Log(LogLevel.Info, "Successfully to Create Account  " + account.Name + " By User : " + account.LastModifiedById  + " with Code :" + account.Customer_Code__c + " return code : " + code);
                    }
                    catch (Exception ex)
                    {
                        response.Ack = false;
                        logger.Log(LogLevel.Error, ex.Message);
                    }
                }

            }
            return response;
        }
        #endregion

    }
}
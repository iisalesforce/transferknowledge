﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Italthai_sfdc_services.Model
{
    public class LogData
    {
        public string DateTime { get; set; }
        public string Info { get; set; }

        public string Message { get; set; }
    }
}
﻿using Italthai_sfdc_services.IService;
using NLog;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace Italthai_sfdc_services.ServiceImpl
{
    public class CustomerService : ICustomerService
    {
        private OracleConnection con;
        private readonly string connectionStr = ConfigurationManager.AppSettings["ITAL"];
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public CustomerService()
        {
            con = new OracleConnection(connectionStr);
        }
        public bool IsExistCustomer(string customerCode)
        {
            Boolean result = true;
            logger.Log(LogLevel.Info, " Salesforce Request for : " + customerCode);

            if (String.IsNullOrEmpty(customerCode))
            {
                throw new ArgumentNullException("customerCode");
            }
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            /************** Create the command object **********/
            OracleCommand oraCommand = con.CreateCommand();
            try
            {
                oraCommand.CommandText = "select cust_account_id, party_id, account_number, account_name from hz_cust_accounts where  1 = 1 and account_number = '" + customerCode + "'";
                oraCommand.BindByName = true;
                oraCommand.CommandType = CommandType.Text;
                // oraCommand.Parameters.Add(new OracleParameter("@account_number", customerCode));
                var oraReader = oraCommand.ExecuteReader();
                if (!oraReader.HasRows)
                {
                    result = false;
                }
                oraReader.Close();
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Error, "Cannot Create Command", ex);
                result = false;
            }
            finally
            {
                oraCommand.Dispose();
                con.Close();
            }
            return result;
        }
        public int CreateNewCustomer(string customerCode, string customerName, string organizationName)
        {
            int result = -1;
            logger.Log(LogLevel.Info, " Salesforce Request To Create Customer : " + customerName);
            if (con.State != ConnectionState.Open)
            {
                try
                {
                    con.Open();
                }
                catch (Exception ex)
                {
                    logger.Log(LogLevel.Error, "Cannot Open Connection To Oracle : ", ex);
                    return result;
                }
            }
            /************** Create the command object **********/
            result = 0;
            /***************  API VALUE *************/
            OracleDecimal r_cust_account_id = 0;
            OracleString r_account_number = "";
            OracleDecimal r_party_id = 0;
            OracleString r_party_number = "";

            OracleDecimal r_profile_id = 0;
            OracleDecimal r_location_id = 0;
            OracleDecimal r_party_site_id = 0;
            OracleDecimal r_cust_acct_site_id = 0;
            OracleCommand oraCommand = con.CreateCommand();
            try
            {              
                


                oraCommand.CommandText = "II_SFDC_INTRGRATION.SFDC_createcustomer";
                oraCommand.CommandType = CommandType.StoredProcedure;
                oraCommand.Parameters.Add("account_name", OracleDbType.Varchar2).Value = customerName;
                oraCommand.Parameters.Add("organization_name", OracleDbType.Varchar2).Value = organizationName; //  ชื่อบริษัท
                oraCommand.Parameters.Add("account_number", OracleDbType.Varchar2).Value = customerCode;

                //#### OUT PUT ###
                oraCommand.Parameters.Add("r_cust_account_id", OracleDbType.Decimal).Direction = ParameterDirection.Output;



                oraCommand.ExecuteNonQuery();
                r_cust_account_id = (OracleDecimal)oraCommand.Parameters["r_cust_account_id"].Value;
                if (!r_cust_account_id.IsNull)
                {
                    logger.Log(LogLevel.Info, "Create New Customer Successfully > r_cust_account_id : " + r_cust_account_id);
               
                    result = 1;
                }
            }

            catch (Exception ex)
            {
                logger.Log(LogLevel.Error, "Cannot Create Command", ex);
                result = 0;
            }
            finally
            {
                oraCommand.Dispose();
                con.Close();
            }
            return result;
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="Italthai_sfdc_services.Test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        Customer Number :
        <asp:TextBox ID="txtCusNo" runat="server"></asp:TextBox>
        <br />
        <br />
        NAME :
        <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
        <br />
        <br />
        <asp:DropDownList ID="ddlFile" runat="server">
        </asp:DropDownList>
        <br />
        <asp:GridView ID="gdvLog" runat="server">
        </asp:GridView>
        <br />
        <asp:Button ID="btnOk" runat="server" Text="Search" OnClick="btnOk_Click" />

    </form>
</body>
</html>

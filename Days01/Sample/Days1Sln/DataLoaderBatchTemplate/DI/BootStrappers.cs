﻿using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLoaderBatchTemplate.DI
{
    public class BootStrappers
    {

        // Lazy SingleTon
        private static readonly Lazy<IKernel> Lazy = new Lazy<IKernel>(() => new StandardKernel(new NinjectModule[] { new ConfigModule(), new ProcessModule() }));
        public static IKernel Container { get { return (IKernel)Lazy.Value; } }
    }
}

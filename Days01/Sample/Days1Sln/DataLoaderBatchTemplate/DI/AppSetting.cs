﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf.Logging;
using DataLoaderBatchTemplate.DI;

namespace DataLoaderBatchTemplate.DI
{
    public interface IAppSetting
    {
        string UserName { get; }
        string Password { get; }
        string Token { get; }
        string ConsumerKey { get; }
        string ConsumerSecret { get; }
        string EndpointUrl { get; }
        string IsSandbox { get; }
        bool Sandbox { get; }
        int BatchSize { get; }
        string JobTime { get; }
        string LoaderPath { get; }
        string RootPath { get; }

        string DateExtractReport { get; }
        string FtpIP { get; }
        string FtpUser { get; }
        string FtpPassword { get; }

        //20160606 : add for query data by range
        string FromDate { get; }
        string ToDate { get; }
        #region 2016-06-06: add on r8-feature
        List<string> DefaultEmail { get; }
        string BatchResultGroupName { get; }
        #endregion

        bool Login { get; }
        string RemoveAfter { get; }

        Encoding EncodingFormat { get; }
    }

    public class Appsetting : IAppSetting
    {
        private static readonly LogWriter _logger = HostLogger.Get("Info");
        private readonly Guid _corelationGuid;
        public Guid Id()
        {
            return _corelationGuid;
        }

        public Appsetting()
        {
            _corelationGuid = Guid.NewGuid();
        }

        public string UserName
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings["UserName"];
            }
        }


        public string Password
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings["Password"];
            }
        }

        public string Token
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings["Token"];
            }
        }


        public string ConsumerKey
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings["ConsumerKey"];
            }
        }

        public string ConsumerSecret
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings["ConsumerSecret"];
            }
        }
        public string EndpointUrl
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                var url = IsSandbox.Equals("true", StringComparison.CurrentCultureIgnoreCase)
               ? "https://test.salesforce.com/services/oauth2/token"
               : "https://login.salesforce.com/services/oauth2/token";
                return url;
            }
        }

        public string IsSandbox
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings["IsSandboxUser"];
            }
        }

        public bool Sandbox
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                return IsSandbox.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                    ? true
                    : false;
            }
        }

        public int BatchSize
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings.AllKeys.Contains("BatchSize") ? int.Parse(ConfigurationManager.AppSettings["BatchSize"]) : 2000;
            }
        }

        public string JobTime
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings["JobTime"];
            }
        }
        public string LoaderPath
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings["LoaderPath"] + DateTime.Today.ToString("yyyyMMdd") + @"\";

            }
        }
        public string RootPath
        {
            get
            {

                _logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings["LoaderPath"];

            }
        }

        public string DateExtractReport
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings["DateExtractReport"];
            }
        }
        //20160606 : add for query data by range
        public string FromDate
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings["FromDate"];
            }
        }
        //20160606 : add for query data by range
        public string ToDate
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings["ToDate"];
            }
        }

        /********  if use filter for AcctPlanProd   *********/
        //<add key="DataExtractReportforAccPlanProd" value="WHERE SEQ__c NOT IN (4.0,10.0) AND LastModifiedDate &gt;= 2016-02-12T17:00:00.000Z AND LastModifiedDate &lt;= 2016-05-13T11:00:00.000Z"/>
        //public string DataExtractReportforAccPlanProd
        //{
        //    get
        //    {
        //        _logger.Info("Appsetting GUID Id " + Id());
        //        return ConfigurationManager.AppSettings["DataExtractReportforAccPlanProd"];

        //    }
        //}


        public string FtpIP
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                if (ConfigurationManager.AppSettings.AllKeys.Contains("FtpIP"))
                    return ConfigurationManager.AppSettings["FtpIP"];
                throw new Exception("FtpIP element in Configuration File does not exist.");
            }
        }
        public string FtpUser
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                if (ConfigurationManager.AppSettings.AllKeys.Contains("FtpUser"))
                    return ConfigurationManager.AppSettings["FtpUser"];
                throw new Exception("FtpUser element in Configuration File does not exist.");
            }
        }
        public string FtpPassword
        {
            get
            {
                //_logger.Info("Appsetting GUID Id " + Id());
                if (ConfigurationManager.AppSettings.AllKeys.Contains("FtpPassword"))
                    return ConfigurationManager.AppSettings["FtpPassword"];
                throw new Exception("FtpPassword element in Configuration File does not exist.");
            }
        }

        public List<string> DefaultEmail
        {
            get
            {
                _logger.Info("Appsetting GUID Id " + Id());
                if (ConfigurationManager.AppSettings.AllKeys.Contains("DefaultEmail"))
                {
                    return ConfigurationManager.AppSettings["DefaultEmail"].Split(new[] { ";", "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                }
                throw new Exception("DefaultEmail element in Configuration File does not exist.");
            }
        }

        public string BatchResultGroupName
        {
            get
            {
                _logger.Info("Appsetting GUID Id " + Id());
                return ConfigurationManager.AppSettings.AllKeys.Contains("BatchResultGroupName") ? ConfigurationManager.AppSettings["BatchResultGroupName"] : "dri@ii.co.th";
            }
        }

        public bool Login
        {
            get { return true; }
        }

        //2016-06-13: add property EncodingFormat
        public Encoding EncodingFormat
        {
            get { return Encoding.UTF8; }
        }

        public string RemoveAfter
        {
            get
            {
                return ConfigurationManager.AppSettings.AllKeys.Contains("RemoveAfter") ? ConfigurationManager.AppSettings["RemoveAfter"] : "90";

            }
        }
    }
}

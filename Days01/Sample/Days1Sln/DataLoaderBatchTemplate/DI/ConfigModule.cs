﻿using DataLoaderBatchTemplate.Process;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLoaderBatchTemplate.DI
{
    public class ConfigModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAppSetting>().To<Appsetting>().InThreadScope();
            //Bind<ISalesforceConfig>().To<SalesforceConfig>().InThreadScope()
            //    .WithConstructorArgument("username", context => context.Kernel.Get<IAppSetting>().UserName)
            //    .WithConstructorArgument("password", context => context.Kernel.Get<IAppSetting>().Password)
            //    .WithConstructorArgument("securityToken", context => context.Kernel.Get<IAppSetting>().Token)
            //    .WithConstructorArgument("batchSize", context => 200)
            //    .WithConstructorArgument("isSandbox", context => context.Kernel.Get<IAppSetting>().Sandbox);
            //Bind<ISalesforceClient>().To<SalesforceClient>().InThreadScope()
            //    .WithConstructorArgument("ISalesforceConfig", context => context.Kernel.Get<ISalesforceConfig>())
            //    .WithConstructorArgument("login", context => context.Kernel.Get<IAppSetting>().Login);
            //Bind<IAuthenticationClient>().To<AuthenticationClient>().InThreadScope()
            //    .WithConstructorArgument("clientId", context => context.Kernel.Get<IAppSetting>().ConsumerKey)
            //    .WithConstructorArgument("clientSecret", context => context.Kernel.Get<IAppSetting>().ConsumerSecret)
            //    .WithConstructorArgument("username", context => context.Kernel.Get<IAppSetting>().UserName)
            //    .WithConstructorArgument("password", context => context.Kernel.Get<IAppSetting>().Password + context.Kernel.Get<IAppSetting>().Token)
            //    .WithConstructorArgument("tokenRequestEndpointUrl", context => context.Kernel.Get<IAppSetting>().EndpointUrl);

        }

    }
    public class ProcessModule : NinjectModule
    {
        public override void Load()
        {

            Bind<IProcess>().To<DataLoader1>().InThreadScope().Named("DataLoader1");
            // Bind<IProcess>().To<DataLoader2>().InThreadScope().Named("DataLoader2");
            // Bind<IProcess>().To<DataLoader3>().InThreadScope().Named("DataLoader3");


            //Bind<IProcess>().To<AccountNIProjectionProcess>().InThreadScope().Named("AccountNIProjection");
            //Bind<IProcess>().To<TargetProcess>().InThreadScope().Named("Target");
            //Bind<IProcess>().To<AccounttPlanProdStrategyProcess>().InThreadScope().Named("AccounttPlanProdStrategy");
            //Bind<IProcess>().To<CallReportProcess>().InThreadScope().Named("CallReport");
            //Bind<IProcess>().To<LeadProcess>().InThreadScope().Named("Lead");
            //Bind<IProcess>().To<LeadRMDProdProcess>().InThreadScope().Named("LeadRMDProd");
            //Bind<IProcess>().To<OpportunityHistoryProcess>().InThreadScope().Named("OpportunityHistory");
            //Bind<IProcess>().To<OpportunityProductProcess>().InThreadScope().Named("OpportunityProduct");
            //Bind<IProcess>().To<OpportunityTeamProcess>().InThreadScope().Named("OpportunityTeam");
            //Bind<IProcess>().To<VisitPlanInviteeProcess>().InThreadScope().Named("VisitPlanInvitee");
            //Bind<IEmail>().To<EmailProcess>().InThreadScope().Named("Email");
            //Bind<IFileManager>().To<FileManager>().InThreadScope();
        }
    }
}

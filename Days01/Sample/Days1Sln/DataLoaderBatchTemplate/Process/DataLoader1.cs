﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf.Logging;
namespace DataLoaderBatchTemplate.Process
{
    public class DataLoader1 : ProcessBase, IProcess
    {
        public void DoWork()
        {
            _logger.Info("DataLoader1");
            // Console
            Console.WriteLine(" RUN DataLoader1 ");

            var bat = @"D:\DataLoader\cliq_process\Pommatt\Pommatt.bat";

            int exitCode;
            ProcessStartInfo processInfo;
            System.Diagnostics.Process process;

            processInfo = new ProcessStartInfo(bat);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = System.Diagnostics.Process.Start(processInfo);
            // process.WaitForExit();

            // *** Read the streams ***
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();
            exitCode = process.ExitCode;
            _logger.Info("-------- Actual Invoice Process -------------");
            _logger.Info(" Process Output ");
            _logger.Info(output);
            _logger.Info(" Process Error ");
            _logger.Info(error);

        }

        public void Dispose()
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf.Logging;

namespace DataLoaderBatchTemplate.Process
{
    public abstract class ProcessBase
    {
        protected readonly LogWriter _logger = HostLogger.Get("Info");
    }
}

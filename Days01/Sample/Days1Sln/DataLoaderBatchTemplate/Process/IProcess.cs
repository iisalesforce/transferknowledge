﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLoaderBatchTemplate.Process
{
    public interface IProcess : IDisposable
    {
        void DoWork();      

    }
}

﻿using DataLoaderBatchTemplate.DI;
using DataLoaderBatchTemplate.Process;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf.Logging;
using Ninject;

namespace DataLoaderBatchTemplate.JOB
{
    public class JobManager : IJob, IDisposable
    {
        public JobManager()
        {
            
        }
        private static readonly LogWriter _logger = HostLogger.Get("Info");

        public void Execute(IJobExecutionContext context)
        {
           
            foreach (var process in BootStrappers.Container.GetAll<IProcess>())
            {
                try
                {
                    _logger.Info("Run Process Name : " + process.GetType().Name);
                    process.DoWork();                  
                }
                catch (Exception ex)
                {

                    _logger.LogFormat(LoggingLevel.Error, "Error Process {0} : {1}", process.GetType().Name, ex);
                }
            }
           

        }

        public void Dispose()
        {
            _logger.Info("End JobManager");
            BootStrappers.Container.Dispose();
        }
    }

}

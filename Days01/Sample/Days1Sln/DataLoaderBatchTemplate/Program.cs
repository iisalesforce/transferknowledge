﻿using DataLoaderBatchTemplate.JOB;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Quartz;
using Topshelf;
using Topshelf.Quartz;

namespace DataLoaderBatchTemplate
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(c =>
            {
                c.UseNLog();
                c.SetServiceName("DataLoader_Template");
                c.SetDescription("Batch Data Loader");
                c.SetDisplayName("Batch Data Loader");
                //  c.StartAutomatically(); // Start the service automatically
                c.RunAsLocalService();
                c.Service<ContainerService>(s =>
                {
                    s.ConstructUsing(() => new ContainerService());
                    s.WhenStarted((service, control) => service.Start());
                    s.WhenStopped((service, control) => service.Stop());

                });
                // more infomation goto :  http://www.quartz-scheduler.org/documentation/quartz-2.x/tutorials/tutorial-lesson-06.html
                var serviceCron = DateTime.Now.AddSeconds(5).ToString("ss mm HH ") + " * * ?";
                c.ScheduleQuartzJobAsService(q =>
                     q.WithJob(() =>
                         JobBuilder.Create<JobManager>().Build())
                     .AddTrigger(() =>
                         TriggerBuilder.Create()
                         .WithCronSchedule(serviceCron) // <<=====  Config Cron Job
                         .Build())
                     );

            });
        }
    }
    public class ContainerService
    {
        public ContainerService()
        {

        }
        public bool Start()
        {
            return true;
        }

        public bool Stop()
        {
            return true;
        }
    }
}

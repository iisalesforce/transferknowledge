﻿using System;
using System.Net;

namespace SalesforceMagic.Configuration
{
    /*
     *  var config = new SalesforceConfig
    {
        Environment = "", // Can be used to differentiate between multiple environments
        Username = "salesforceUsername",
        Password = "salesforcePassword",
        SecurityToken = "securityToken",
        IsSandbox = true
    }
     * */
    public interface ISalesforceConfig
    {
        SalesforceSession Session { get; set; }
        bool IsSandbox { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        string SecurityToken { get; set; }
        string Environment { get; set; }
        bool LogoutOnDisposal { get; set; }
        bool UseSessionStore { get; set; }
        string ApiVersion { get; set; }
        string InstanceUrl { get; set; }
        WebProxy Proxy { get; set; }
        int? BatchSize { get; set; }
    }
    public class SalesforceConfig : ISalesforceConfig
    {

        public SalesforceConfig(string username, string password, string securityToken,  bool isSandbox = true)
        {
            if (username == null) throw new ArgumentNullException("username");
            if (password == null) throw new ArgumentNullException("password");
            if (securityToken == null) throw new ArgumentNullException("securityToken");
            Username = username;
            Password = password;
            SecurityToken = securityToken;
            IsSandbox = isSandbox;
            BatchSize = 2000;
        }
        public SalesforceConfig(string username, string password, string securityToken, int batchSize = 500 ,bool isSandbox = true)
        {
            if (username == null) throw new ArgumentNullException("username");
            if (password == null) throw new ArgumentNullException("password");
            if (securityToken == null) throw new ArgumentNullException("securityToken");
            Username = username;
            Password = password;
            SecurityToken = securityToken;
            IsSandbox = isSandbox;
            BatchSize = batchSize;
        }
        public SalesforceSession Session { get; set; }
        public bool IsSandbox { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string SecurityToken { get; set; }
        public string Environment { get; set; }
        public bool LogoutOnDisposal { get; set; }
        public bool UseSessionStore { get; set; }
        public string ApiVersion { get; set; }
        public string InstanceUrl { get; set; }
        public WebProxy Proxy { get; set; }

        /// <summary>
        /// Min: 200, Max: 2000, Default: 500.
        /// </summary>
        public int? BatchSize { get; set; }
    }
}
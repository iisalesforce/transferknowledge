﻿using System.Collections.Generic;
using System.Net;
using SalesforceMagic.Http.Enums;

namespace SalesforceMagic.Http.Models
{
    public  class HttpRequest
    {
        public  HttpRequest()
        {
            Method = RequestType.POST;
            ContentType = "text/xml";
            Headers = new Dictionary<string, string>();
        }

        public  string Url { get; set; }
        public  RequestType Method { get; set; }
        public  Dictionary<string, string> Headers { get; set; }
        public  string Body { get; set; }
        public  string ContentType { get; set; }

        public  WebProxy Proxy { get; set; }

        public  bool IsValid
        {
            get
            {
                bool success = Url != null;

                if (Method != RequestType.GET && Body == null)
                    success =  false;

                return success;
            }
        }
    }
}
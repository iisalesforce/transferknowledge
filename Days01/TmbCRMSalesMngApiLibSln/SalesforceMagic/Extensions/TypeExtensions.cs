﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;
using SalesforceMagic.Attributes;
using SalesforceMagic.Exceptions;

namespace SalesforceMagic.Extensions
{
    public  static class TypeExtensions
    {
        public  static IEnumerable<PropertyInfo> GetFilteredProperties<T>(this Type type) where T : Attribute
        {
            return type.GetProperties().Where(x => x.GetCustomAttribute<T>() != null);
        }

        public  static IEnumerable<PropertyInfo> FilterProperties<T>(this Type type) where T : Attribute
        {
            return type.GetProperties().Where(x => x.GetCustomAttribute<T>() == null);
        }

        public  static IEnumerable<PropertyInfo> FilterProperties<T, TK>(this Type type) where T : Attribute where TK : Attribute
        {
            return type.GetProperties().Where(x => x.GetCustomAttribute<T>() == null && x.GetCustomAttribute<TK>() == null);
        }

        public  static IEnumerable<string> GetPropertyNames(this Type type, bool skipIgnored = false)
        {
            return type.GetProperties().GetNames(skipIgnored);
        }

        public  static IEnumerable<string> GetNames(this PropertyInfo[] infos, bool skipIgnored = false)
        {
            // TODO: There has to be a better way to do this, the Id field needs to be first.
            List<string> names = new List<string> { GetName(infos.FirstOrDefault(x => x.Name == "Id")) };
            names.AddRange(infos.Where(x => x.Name != "Id" && x.GetCustomAttribute<SalesforceIgnore>() == null).Select(x => x.GetName()));

            return names;
        }

        public  static string GetName(this PropertyInfo info)
        {
            return info.GetCustomAttribute<SalesforceNameAttribute>() != null
                ? info.GetCustomAttribute<SalesforceNameAttribute>().Name
                : info.Name;
        }

        public  static string GetCsvHeaders<T>(this IEnumerable<T> items)
        {
            Type type = typeof(T);
            string[] values = type.GetProperties().Select(x => x.GetName()).ToArray();

            return String.Join(",", values);
        }

        public  static string GetName(this Type type)
        {
            SalesforceNameAttribute attribute = type.GetCustomAttribute<SalesforceNameAttribute>();
            return attribute != null
                ? attribute.Name
                : type.Name;
        }

        public  static string GetValue(this XmlNode node, string name)
        {
            var result = node[name];
            return result != null ? result.InnerText : null;
        }

        public  static bool ToBoolean(this string value)
        {
            try
            {
                return Convert.ToBoolean(value);
            }
            catch (FormatException e) { throw new SalesforceRequestException(e.Message); }
        }

        public  static int ToInt(this string value)
        {
            try
            {
                if (string.IsNullOrEmpty(value)) return default(int);

                return value.Contains(".")
                    ? (int) Convert.ToDecimal(value)
                    : Convert.ToInt32(value);
            }
            catch (FormatException e) { throw new SalesforceRequestException(e.Message); }
        }

        public  static double ToDouble(this string value)
        {
            try
            {
                return !string.IsNullOrEmpty(value) 
                    ? Convert.ToDouble(value)
                    : default(double);
            }
            catch (FormatException e) { throw new SalesforceRequestException(e.Message); }
        }

        public  static decimal ToDecimal(this string value)
        {
            try
            {
                return Convert.ToDecimal(value);
            }
            catch (FormatException e) { throw new SalesforceRequestException(e.Message); }
        }

        public  static float ToFloat(this string value)
        {
            try
            {
                return value.Contains(".")
                    ? (int)Convert.ToDecimal(value)
                    : Convert.ToInt32(value);
            }
            catch (FormatException e) { throw new SalesforceRequestException(e.Message); }
        }

        public  static DateTime ToDateTime(this string value)
        {
            try
            {
                return string.IsNullOrEmpty(value)
                    ? DateTime.MinValue
                    : Convert.ToDateTime(value);
            }
            catch (FormatException e) { throw new SalesforceRequestException(e.Message); }
        }



        public  static bool? ToNullableBoolean(this string value)
        {
            if (string.IsNullOrEmpty(value)) return null;
            try
            {
                return Convert.ToBoolean(value);
            }
            catch (FormatException e) { throw new SalesforceRequestException(e.Message); }
        }

        public  static int? ToNullableInt(this string value)
        {
            if (string.IsNullOrEmpty(value)) return null;
            try
            {
                return value.Contains(".")
                    ? (int)Convert.ToDecimal(value)
                    : Convert.ToInt32(value);
            }
            catch (FormatException e) { throw new SalesforceRequestException(e.Message); }
        }

        public  static double? ToNullableDouble(this string value)
        {
            if (string.IsNullOrEmpty(value)) return null;
            try
            {
                return Convert.ToDouble(value);
            }
            catch (FormatException e) { throw new SalesforceRequestException(e.Message); }
        }

        public  static decimal? ToNullableDecimal(this string value)
        {
            if (string.IsNullOrEmpty(value)) return null;
            try
            {
                return Convert.ToDecimal(value);
            }
            catch (FormatException e) { throw new SalesforceRequestException(e.Message); }
        }

        public  static float? ToNullableFloat(this string value)
        {
            if (string.IsNullOrEmpty(value)) return null;
            try
            {
                return value.Contains(".")
                    ? (int)Convert.ToDecimal(value)
                    : Convert.ToInt32(value);
            }
            catch (FormatException e) { throw new SalesforceRequestException(e.Message); }
        }

        public  static DateTime? ToNullableDateTime(this string value)
        {
            if (string.IsNullOrEmpty(value)) return null;
            try
            {
                return string.IsNullOrEmpty(value)
                    ? DateTime.MinValue
                    : Convert.ToDateTime(value);
            }
            catch (FormatException e) { throw new SalesforceRequestException(e.Message); }
        }

        public  static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> sequence, int size)
        {
            IList<T> partition = new List<T>(size);
            foreach (var item in sequence)
            {
                partition.Add(item);
                if (partition.Count == size)
                {
                    yield return partition;
                    partition = new List<T>(size);
                }
            }

            if (partition.Count > 0) yield return partition;
        }
    }
}

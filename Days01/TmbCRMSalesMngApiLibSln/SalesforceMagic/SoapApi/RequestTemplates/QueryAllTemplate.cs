﻿using SalesforceMagic.ORM.BaseRequestTemplates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SalesforceMagic.SoapApi.RequestTemplates
{
    [Serializable]
    public class QueryAllTemplate
    {
        public QueryAllTemplate()
        {
        }

        public QueryAllTemplate(string queryString)
        {
            QueryString = queryString;
        }

        [XmlElement("queryString", Namespace = SalesforceNamespaces.SalesforceRequest)]
        public string QueryString { get; set; }
    }
}

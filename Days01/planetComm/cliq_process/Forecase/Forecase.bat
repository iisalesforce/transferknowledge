SET DLPATH="D:\SF\Data Loader"
SET DLCONF="D:\SF\Data Loader\cliq_process\Forecase\config"
SET DLDATA="D:\SF\Data Loader\cliq_process\Forecase\write"
call %DLPATH%\Java\bin\java.exe -cp %DLPATH%\* -Dsalesforce.config.dir=%DLCONF% com.salesforce.dataloader.process.ProcessRunner process.name=Forecase
REM To rotate your export files, uncomment the line below
REM copy %DLDATA%\Forecase.csv %DLDATA%\%date:~10,4%%date:~7,2%%date:~4,2%-%time:~0,2%-Forecase.csv

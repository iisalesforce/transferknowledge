SET DLPATH="D:\SF\Data Loader"
SET DLCONF="D:\SF\Data Loader\cliq_process\SalesHeader\config"
SET DLDATA="D:\SF\Data Loader\cliq_process\SalesHeader\write"
call %DLPATH%\Java\bin\java.exe -cp %DLPATH%\* -Dsalesforce.config.dir=%DLCONF% com.salesforce.dataloader.process.ProcessRunner process.name=SalesHeader
REM To rotate your export files, uncomment the line below
REM copy %DLDATA%\SalesHeader.csv %DLDATA%\%date:~10,4%%date:~7,2%%date:~4,2%-%time:~0,2%-SalesHeader.csv

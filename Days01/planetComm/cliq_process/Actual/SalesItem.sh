#!/bin/sh
export DLPATH="D:\SF\Data Loader"
export DLCONF="D:\SF\Data Loader\cliq_process\SalesItem\config"
java -cp "$DLPATH/*" -Dsalesforce.config.dir=$DLCONF com.salesforce.dataloader.process.ProcessRunner process.name=SalesItem

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using NLog;
using System.Configuration;
using System.Diagnostics;
using PlanetComm.DataAccess;

namespace PlanetComm.Salesforce
{
    [DisallowConcurrentExecution]
    public class InvoiceCNToSalesforce : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var logger = LogManager.GetCurrentClassLogger();
            try
            {
                var bat = ConfigurationManager.AppSettings["actualdataload"];
                int exitCode;
                ProcessStartInfo processInfo;
                Process process;

                processInfo = new ProcessStartInfo(bat);
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                // *** Redirect the output ***
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;

                process = Process.Start(processInfo);
                // process.WaitForExit();

                // *** Read the streams ***
                string output = process.StandardOutput.ReadToEnd();
                string error = process.StandardError.ReadToEnd();
                exitCode = process.ExitCode;
                logger.Log(LogLevel.Info, "-------- Actual Invoice Process -------------");
                logger.Log(LogLevel.Info, " Process Output ");
                logger.Log(LogLevel.Info, output);
                logger.Log(LogLevel.Info, " Process Error ");
                logger.Log(LogLevel.Error, error);


                var bat2 = ConfigurationManager.AppSettings["forecastdataload"];
                processInfo = new ProcessStartInfo(bat2);
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                // *** Redirect the output ***
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;

                process = Process.Start(processInfo);
                // process.WaitForExit();

                // *** Read the streams ***
                output = process.StandardOutput.ReadToEnd();
                error = process.StandardError.ReadToEnd();
                exitCode = process.ExitCode;
                logger.Log(LogLevel.Info, "-------- Forecast Invoice Process -------------");
                logger.Log(LogLevel.Info, " Process Output ");
                logger.Log(LogLevel.Info, output);
                logger.Log(LogLevel.Info, " Process Error ");
                logger.Log(LogLevel.Error, error);


                var bat3 = ConfigurationManager.AppSettings["cndataload"];
                processInfo = new ProcessStartInfo(bat3);
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                // *** Redirect the output ***
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;

                process = Process.Start(processInfo);
                // process.WaitForExit();

                // *** Read the streams ***
                output = process.StandardOutput.ReadToEnd();
                error = process.StandardError.ReadToEnd();
                exitCode = process.ExitCode;
                logger.Log(LogLevel.Info, "-------- Credit Note Process -------------");
                logger.Log(LogLevel.Info, " Process Output ");
                logger.Log(LogLevel.Info, output);
                logger.Log(LogLevel.Info, " Process Error ");
                logger.Log(LogLevel.Error, error);
            }
            catch (Exception ex)
            {

                logger.Log(LogLevel.Error, ex.Message);

                // Write the fault message to the console
                logger.Log(LogLevel.Error, "An unexpected error has occurred: " + ex.InnerException);

                // Write the stack trace to the console
                logger.Log(LogLevel.Error, ex.StackTrace);
            }
        }
    }
}

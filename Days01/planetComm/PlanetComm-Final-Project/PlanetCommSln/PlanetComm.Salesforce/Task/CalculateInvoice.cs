﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using PlanetComm.Salesforce.SFService;
using PlanetComm.Lib;
using System.Web.Services.Protocols;
using System.Diagnostics;
using NLog;
using PlanetComm.DataAccess;
using PlanetComm.Salesforce.Helper;

namespace PlanetComm.Salesforce
{
    [DisallowConcurrentExecution]
    public class CalculateInvoice : IJob
    {
        private SforceService binding;

        public void Execute(IJobExecutionContext context)
        {
            var logger = LogManager.GetCurrentClassLogger();

            try
            {
                using (SalesforceServiceProvider sfservice = new SalesforceServiceProvider())
                {
                    PlanetCommDataContext db = new PlanetCommDataContext();
                    var actuals = from x in db.ACTUALs
                                  where x.IsDone == "N"
                                  select x;

                    var forecast = from x in db.FORECASTs
                                   where x.IsDone == "N"
                                   select x;


                    var cn = from x in db.CREDITNOTEs
                             where x.IsDone == "N"
                             select x;

                    logger.Log(LogLevel.Info, "=====   Read Data From Database   ====");
                    logger.Log(LogLevel.Info, "=====   " + actuals.Count() + "   ====");
                    logger.Log(LogLevel.Info, "=====   " + forecast.Count() + "   ====");
                    logger.Log(LogLevel.Info, "=====   " + cn.Count() + "   ====");

                    try
                    {
                        var rs = sfservice.Login();
                        var session = rs.SessionHeaderValue.sessionId;
                        PlanetComm.SalesTargetService.PlannetCommCalculateSalesTargetService service
                            = new PlanetComm.SalesTargetService.PlannetCommCalculateSalesTargetService();
                        service.SessionHeaderValue = new PlanetComm.SalesTargetService.SessionHeader();
                        service.SessionHeaderValue.sessionId = session;
                        service.Url = rs.Url;
                        //  Actual
                        foreach (var item in actuals)
                        {
                            try
                            {
                                var result = service.CalculateByInvoiceId(item.INVOICENUMBER.Trim());
                                logger.Log(LogLevel.Info, "Send Invoice : " + item.INVOICENUMBER.Trim() + " result : " + result);
                                item.IsDone = "Y";
                            }
                            catch (SoapException e)
                            {

                                logger.Log(LogLevel.Error, e.Code);

                                // Write the fault message to the console
                                logger.Log(LogLevel.Error, "An unexpected error has occurred: " + e.Message);

                                // Write the stack trace to the console
                                logger.Log(LogLevel.Error, e.StackTrace);

                            }
                            catch (Exception ex)
                            {
                                // Write the fault message to the console
                                logger.Log(LogLevel.Error, "An unexpected error has occurred: " + ex.Message);
                            }

                        }

                        // Forecast
                        foreach (var item in forecast)
                        {
                            try
                            {
                                var result = service.CalculateByInvoiceId(item.INVOICENO.Trim());
                                logger.Log(LogLevel.Info, "Send Invoice : " + item.INVOICENO.Trim() + " result : " + result);
                                item.IsDone = "Y";
                            }
                            catch (SoapException e)
                            {

                                logger.Log(LogLevel.Error, e.Code);

                                // Write the fault message to the console
                                logger.Log(LogLevel.Error, "An unexpected error has occurred: " + e.Message);

                                // Write the stack trace to the console
                                logger.Log(LogLevel.Error, e.StackTrace);

                            }
                            catch (Exception ex)
                            {
                                // Write the fault message to the console
                                logger.Log(LogLevel.Error, "An unexpected error has occurred: " + ex.Message);
                            }
                        }

                        // Credit Note
                        foreach (var item in cn)
                        {
                            try
                            {
                                var result = service.CalculateByCNId(item.CREDITNOTENUMBER.Trim());
                                logger.Log(LogLevel.Info, "Send Invoice : " + item.CREDITNOTENUMBER.Trim() + " result : " + result);
                                item.IsDone = "Y";
                            }
                            catch (SoapException e)
                            {

                                logger.Log(LogLevel.Error, e.Code);

                                // Write the fault message to the console
                                logger.Log(LogLevel.Error, "An unexpected error has occurred: " + e.Message);

                                // Write the stack trace to the console
                                logger.Log(LogLevel.Error, e.StackTrace);

                            }
                            catch (Exception ex)
                            {
                                // Write the fault message to the console
                                logger.Log(LogLevel.Error, "An unexpected error has occurred: " + ex.Message);
                            }
                        }

                    }
                    catch (SoapException e)
                    {

                        logger.Log(LogLevel.Error, e.Code);

                        // Write the fault message to the console
                        logger.Log(LogLevel.Error, " Main Loop : An unexpected error has occurred: " + e.Message);

                        // Write the stack trace to the console
                        logger.Log(LogLevel.Error, e.StackTrace);

                    }
                    catch (Exception ex)
                    {
                        // Write the fault message to the console
                        logger.Log(LogLevel.Error, " Main Loop : An unexpected error has occurred: " + ex.Message);
                    }


                    try
                    {
                        db.SubmitChanges();
                    }
                    catch (Exception ex)
                    {
                        logger.Log(LogLevel.Error, " SubmitChanges Error  : An unexpected error has occurred: " + ex.Message);

                    }
                }
            }
            catch (Exception e)
            {

                // Write the fault message to the console
                logger.Log(LogLevel.Error, "Last : An unexpected error has occurred: " + e.Message);

                // Write the stack trace to the console
                logger.Log(LogLevel.Error, e.StackTrace);
            }

        }
    }
}

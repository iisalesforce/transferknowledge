﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using NLog;
using System.Configuration;
using System.Diagnostics;
using PlanetComm.DataAccess;

namespace PlanetComm.Salesforce
{
    [DisallowConcurrentExecution]
    public class SalesHeaderSalesItemDataLoad : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            #region ------------------------------ SalesOrder Header --------------------------------------

            var logger = LogManager.GetCurrentClassLogger();
            // Run Apex Data Loader
            var batOrder = ConfigurationManager.AppSettings["salesheaddataload"];
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo(batOrder);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            // process.WaitForExit();

            // *** Read the streams ***
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();
            exitCode = process.ExitCode;
            logger.Log(LogLevel.Info, "-------- Sales Order Headder Process -------------");
            logger.Log(LogLevel.Info, " Process Output ");
            logger.Log(LogLevel.Info, output);
            logger.Log(LogLevel.Info, " Process Error ");
            logger.Log(LogLevel.Error, error);
            #endregion
            #region ------------------------------ SalesOrder Detial --------------------------------------


            var batOrderItem = ConfigurationManager.AppSettings["salesitemdataload"];
            processInfo = new ProcessStartInfo(batOrderItem);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            // process.WaitForExit();
            // *** Read the streams ***
            output = process.StandardOutput.ReadToEnd();
            error = process.StandardError.ReadToEnd();
            exitCode = process.ExitCode;
            logger.Log(LogLevel.Info, "-------- Sales Order Item Process -------------");
            logger.Log(LogLevel.Info, " Process Output ");
            logger.Log(LogLevel.Info, output);
            logger.Log(LogLevel.Info, " Process Error ");
            logger.Log(LogLevel.Error, error);
            #endregion
            // Update Sales Header and salse order field isDone to Y
            using (PlanetCommDataContext db = new PlanetCommDataContext())
            {
                db.ExecuteCommand(" UPDATE SALEORDER SET IsDone='Y' WHERE IsDone='N' ");
                db.ExecuteCommand(" UPDATE SALEORDER_ITEM SET IsDone='Y' WHERE IsDone='N' ");
                db.SubmitChanges();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PlanetComm.DataAccess;
using System.Configuration;



namespace PlanetComm.WebManagement
{
    public partial class Admin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User"] == null)
            {
                Login.Visible = false;
                Anonymous.Visible = true;
            }
            else
            {
                Login.Visible = true;
                Anonymous.Visible = false;

            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (inputEmail.Text == "planetcomm" && inputPassword.Text == "superice11")
            {

                Session["User"] = "planetcomm";
                Login.Visible = true;
                Anonymous.Visible = false;
            }
        }

        protected void btnClearLog_Click(object sender, EventArgs e)
        {

            using (PlanetCommDataContext db = new PlanetCommDataContext())
            {
                db.ExecuteCommand(" DELETE  FROM Logs ");
                db.SubmitChanges();
            }
        }

        protected void btnClearSo_Click(object sender, EventArgs e)
        {
            using (PlanetCommDataContext db = new PlanetCommDataContext())
            {
                db.ExecuteCommand(" DELETE  FROM SALEORDER ");
                db.SubmitChanges();
            }
        }

        protected void btnClearSoDetail_Click(object sender, EventArgs e)
        {
            using (PlanetCommDataContext db = new PlanetCommDataContext())
            {
                db.ExecuteCommand(" DELETE  FROM SALEORDER_ITEM ");
                db.SubmitChanges();
            }
        }

        protected void btnClearActual_Click(object sender, EventArgs e)
        {
            using (PlanetCommDataContext db = new PlanetCommDataContext())
            {
                db.ExecuteCommand(" DELETE  FROM ACTUAL ");
                db.SubmitChanges();
            }
        }

        protected void btnClearCreditNote_Click(object sender, EventArgs e)
        {
            using (PlanetCommDataContext db = new PlanetCommDataContext())
            {
                db.ExecuteCommand(" DELETE  FROM CREDITNOTE ");
                db.SubmitChanges();
            }
        }

        protected void btnClearForecast_Click(object sender, EventArgs e)
        {
            using (PlanetCommDataContext db = new PlanetCommDataContext())
            {
                db.ExecuteCommand(" DELETE  FROM FORECAST ");
                db.SubmitChanges();
            }
        }

        protected void btnCopy_Click(object sender, EventArgs e)
        {

            string csvtosalesorder = ConfigurationManager.AppSettings["ftp"];

            string sourcePath = csvtosalesorder+@"backup";
            string targetPath = csvtosalesorder;

            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);

                // Copy the files and overwrite destination files if they already exist. 
                foreach (string s in files)
                {
                    // Use static Path methods to extract only the file name from the path.
                    var fileName = System.IO.Path.GetFileName(s);
                    var destFile = System.IO.Path.Combine(targetPath, fileName);
                    System.IO.File.Copy(s, destFile, true);
                }
            }








        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrystalQuartz.Core.SchedulerProviders;
using Quartz;
using PlanetComm.CsvTaskToSql;
using PlanetComm.Salesforce;
using System.Configuration;
namespace PlanetComm.WebManagement
{
    public class PlanetCommTaskProvider : StdSchedulerProvider
    {
        protected override void InitScheduler(IScheduler scheduler)
        {

            string csvtosalesorder = ConfigurationManager.AppSettings["csvtosalesorder"];
            string csvtosalesorderitem = ConfigurationManager.AppSettings["csvtosalesorderitem"];
            string csvtoforecast = ConfigurationManager.AppSettings["csvtoforecast"];
            string csvtoactual = ConfigurationManager.AppSettings["csvtoactual"];
            string csvtocreditnote = ConfigurationManager.AppSettings["csvtocreditnote"];


            string dataloadersalesorder = ConfigurationManager.AppSettings["dataloadersalesorder"];
            string dataloadersalesorderitim = ConfigurationManager.AppSettings["dataloadersalesorderitim"];
            string dataloadercreditnote = ConfigurationManager.AppSettings["dataloadercreditnote"];
           



            #region =============   CSV TO SQL TASK    ==========================

            /*           
            *     Config Name : csvtosalesorder
            *     Job :   Save CSV File To SqlServer Table : Sales Order 
            */
            IJobDetail jobDetail1 = JobBuilder.Create<SalesOrderTaskIJob>()
                .WithIdentity("Save CSV File To SqlServer Table : Sales Order")
                .Build();
            ITrigger trigger1 = TriggerBuilder.Create()
                .ForJob(jobDetail1)
                .WithCronSchedule(csvtosalesorder)  // �ء 30 �ҷ�  8.00 - 18.00
                .WithIdentity("Sales Order")
               // .StartNow()
                .Build();
            scheduler.ScheduleJob(jobDetail1, trigger1);


            /*           
            *     Config Name : csvtosalesorderitem
            *     Job :   Save CSV File To SqlServer Table : Sales Order 
            */
            IJobDetail jobDetail2 = JobBuilder.Create<SalesOrderDetailTask>()
               .WithIdentity("Save CSV File To SqlServer Table : Sales Order Items")
               .Build();
            ITrigger trigger2 = TriggerBuilder.Create()
               .ForJob(jobDetail2)
              .WithCronSchedule(csvtosalesorderitem) // �ء 30 �ҷ�  8.00 - 18.00
               .WithIdentity("Sales Order Item")
               //.StartNow()
               .Build();
            scheduler.ScheduleJob(jobDetail2, trigger2);


            /*           
            *     Config Name : csvtoforecast
            *     Job :   Save CSV File To SqlServer Table : Forecast Invoice 
            */
            IJobDetail jobDetail4 = JobBuilder.Create<ForecastTask>()
          .WithIdentity("Save CSV File To SqlServer Table : Forecast Invoice")
          .Build();
            ITrigger trigger4 = TriggerBuilder.Create()
                .ForJob(jobDetail4)
                .WithCronSchedule(csvtoforecast) // �ء 30 �ҷ�  8.00 - 18.00
                .WithIdentity("Forecast Invoice")
               // .StartNow()
                .Build();
            scheduler.ScheduleJob(jobDetail4, trigger4);



            /*           
           *     Config Name : csvtoactual
           *     Job :   Save CSV File To SqlServer Table : Forecast Invoice 
           */

            IJobDetail jobDetail5 = JobBuilder.Create<ActualTask>()
          .WithIdentity("Save CSV File To SqlServer Table : Actual Invoice")
          .Build();
            ITrigger trigger5 = TriggerBuilder.Create()
                .ForJob(jobDetail5)
                .WithCronSchedule(csvtoactual) // �ء 30 �ҷ�  8.00 - 18.00
                .WithIdentity("Actual Invoice")
                //.StartNow()
                .Build();
            scheduler.ScheduleJob(jobDetail5, trigger5);

            /*           
          *     Config Name : csvtocreditnote
          *     Job :   Save CSV File To SqlServer Table : Forecast Invoice 
          */
            IJobDetail jobDetail6 = JobBuilder.Create<CreditnoteTask>()
          .WithIdentity("Save CSV File To SqlServer Table : Credit Note")
          .Build();
            ITrigger trigger6 = TriggerBuilder.Create()
                .ForJob(jobDetail6)
                .WithCronSchedule(csvtocreditnote) // �ء 30 �ҷ�  8.00 - 18.00
                .WithIdentity("Credit Note")
                //.StartNow()
                .Build();
            scheduler.ScheduleJob(jobDetail6, trigger6);

            #endregion

            #region  ========== Run Date Loader ================

            //   Sales Order  ��� Sales Item
            IJobDetail jobDetail7 = JobBuilder.Create<SalesHeaderSalesItemDataLoad>()
              .WithIdentity("Sales Order Header And Sales Order Item Data Loader")
              .Build();
            ITrigger trigger7 = TriggerBuilder.Create()
                .ForJob(jobDetail7)
                .WithCronSchedule(dataloadersalesorderitim)
                .WithIdentity("Sales Order Header And Sales Order Item Data Loader")
                //.StartNow()
                .Build();
            scheduler.ScheduleJob(jobDetail7, trigger7);
            //=============================================================================
            //  �ǡ Invoice  ������
            IJobDetail jobDetail8 = JobBuilder.Create<InvoiceCNToSalesforce>()
              .WithIdentity("Invoice And Credit Note Data Loader")
              .Build();
            ITrigger trigger8 = TriggerBuilder.Create()
                .ForJob(jobDetail8)
                .WithCronSchedule(dataloadercreditnote)
                .WithIdentity("Invoice And Credit Note Data Loader")
                //.StartNow()
                .Build();
            scheduler.ScheduleJob(jobDetail8, trigger8);



            //===============================================================================
            // �ӡ�äӹǹ  
            IJobDetail jobDetail3 = JobBuilder.Create<CalculateInvoice>()
         .WithIdentity("Calculate Invoice ")
         .Build();
            ITrigger trigger3 = TriggerBuilder.Create()
                .ForJob(jobDetail3)
                .WithCronSchedule(dataloadersalesorder)
                .WithIdentity("Calculate Invoice")
                //.StartNow()
                .Build();
            scheduler.ScheduleJob(jobDetail3, trigger3);


            scheduler.Start();
            scheduler.PauseAll();
            #endregion

        }
    }
}
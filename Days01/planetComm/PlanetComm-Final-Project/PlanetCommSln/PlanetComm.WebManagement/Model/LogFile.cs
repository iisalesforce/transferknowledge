﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanetComm.WebManagement.Model
{
    public class LogFile
    {
        public String FileName { get; set; }
        public String FilePath { get; set; }
    }
}
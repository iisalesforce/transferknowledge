﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SalesOrderDetail.aspx.cs"
    MasterPageFile="~/Main.Master" MaintainScrollPositionOnPostback="true" Inherits="PlanetComm.WebManagement.SalesOrderDetail" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ID="MainContent">

    <h3>
        Sales Order Item
    </h3>
    <div>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" DataSourceID="LinqDataSource1" RowStyle-CssClass="td"
            HeaderStyle-CssClass="th" CssClass="table table-striped table-bordered table-condensed"
            EnableModelValidation="True">
            <Columns>
                <asp:BoundField DataField="ORDNUMBER" HeaderText="ORDNUMBER" ReadOnly="True" SortExpression="ORDNUMBER" />
                <asp:BoundField DataField="UNITPRICEINCDIS" HeaderText="UNITPRICEINCDIS" ReadOnly="True"
                    SortExpression="UNITPRICEINCDIS" />
                <asp:BoundField DataField="DISCOUNTAMOUNT" HeaderText="DISCOUNTAMOUNT" ReadOnly="True"
                    SortExpression="DISCOUNTAMOUNT" />
                <asp:BoundField DataField="PRODUCTDESC" HeaderText="PRODUCTDESC" ReadOnly="True"
                    SortExpression="PRODUCTDESC" />
                <asp:BoundField DataField="QUANTITY" HeaderText="QUANTITY" ReadOnly="True" SortExpression="QUANTITY" />
                <asp:BoundField DataField="PRODUCTCODE" HeaderText="PRODUCTCODE" ReadOnly="True"
                    SortExpression="PRODUCTCODE" />
                <asp:BoundField DataField="ITEM" HeaderText="ITEM" ReadOnly="True" SortExpression="ITEM" />
                <asp:BoundField DataField="INVSTOKOUT" HeaderText="INVSTOKOUT" ReadOnly="True" SortExpression="INVSTOKOUT" />
                <asp:BoundField DataField="SHIPSTOKOUT" HeaderText="SHIPSTOKOUT" ReadOnly="True"
                    SortExpression="SHIPSTOKOUT" />
                <asp:BoundField DataField="POQTY" HeaderText="POQTY" ReadOnly="True" SortExpression="POQTY" />
                <asp:BoundField DataField="BOQTY" HeaderText="BOQTY" ReadOnly="True" SortExpression="BOQTY" />
                <asp:BoundField DataField="BALQTY" HeaderText="BALQTY" ReadOnly="True" SortExpression="BALQTY" />
                <asp:BoundField DataField="COMMITQTY" HeaderText="COMMITQTY" ReadOnly="True" SortExpression="COMMITQTY" />
                <asp:BoundField DataField="STOCKAVA" HeaderText="STOCKAVA" ReadOnly="True" SortExpression="STOCKAVA" />
                <asp:BoundField DataField="TOTALAMOUNT" HeaderText="TOTALAMOUNT" ReadOnly="True"
                    SortExpression="TOTALAMOUNT" />
                <asp:BoundField DataField="SITE" HeaderText="SITE" ReadOnly="True" SortExpression="SITE" />
                <asp:BoundField DataField="STATUS" HeaderText="STATUS" ReadOnly="True" SortExpression="STATUS" />
                <asp:BoundField DataField="STOCKOUTDATE" HeaderText="STOCKOUTDATE" ReadOnly="True"
                    SortExpression="STOCKOUTDATE" />
                <asp:BoundField DataField="IsDone" HeaderText="IsDone" ReadOnly="True" SortExpression="IsDone" />
            </Columns>
            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </asp:GridView>
    </div>
    <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="PlanetComm.DataAccess.PlanetCommDataContext"
        Select="new (ORDNUMBER, ITEM, PRODUCTCODE, QUANTITY, UNITPRICEINCDIS, PRODUCTDESC, DISCOUNTAMOUNT, TOTALAMOUNT, STOCKAVA, COMMITQTY, BOQTY, POQTY, SHIPSTOKOUT, INVSTOKOUT, SITE, BALQTY, IsDone, STATUS, STOCKOUTDATE)"
        TableName="SALEORDER_ITEMs">
    </asp:LinqDataSource>
</asp:Content>

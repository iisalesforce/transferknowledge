﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataLoaderLog.aspx.cs"
    Inherits="PlanetComm.WebManagement.DataLoaderLog" MasterPageFile="~/Main.Master" %>

<%@ Import Namespace="PlanetComm.WebManagement.Model" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ID="MainContent">
    <asp:HiddenField ID="hdfPath" runat="server" />
    <asp:DropDownList ID="ddlDataLoadLog" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDataLoadLog_SelectedIndexChanged">
        <asp:ListItem Selected="True" Value="0">--- Please Select ---</asp:ListItem>
        <asp:ListItem Value="1">Actual</asp:ListItem>
        <asp:ListItem Value="2">CreditNote</asp:ListItem>
        <asp:ListItem Value="3">Forecase</asp:ListItem>
        <asp:ListItem Value="4">SalesHeader</asp:ListItem>
        <asp:ListItem Value="5">SalesItem</asp:ListItem>
    </asp:DropDownList>
    <asp:Button ID="btnDelete" runat="server" class="btn btn-small btn-primary" 
        Text="Clear Log" onclick="btnDelete_Click" />

    <div>
        <asp:Repeater ID="rptFile" runat="server">
            <HeaderTemplate>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>
                                Log File
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:LinkButton ID="btnFile" CommandArgument='<%# ((LogFile)Container.DataItem).FilePath %>'
                            OnCommand="LinkButton_Command" O runat="server" Text='<%# ((LogFile)Container.DataItem).FileName %>' />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"
    CodeBehind="Default.aspx.cs" Inherits="PlanetComm.WebManagement._Default" MasterPageFile="~/Main.Master" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ID="MainContent">
    <div>
        Log Date :
        <asp:DropDownList ID="ddlDate" runat="server" AutoPostBack="True">
        </asp:DropDownList>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnDownload" runat="server" Text="Download Log" OnClick="btnDownload_Click" />
    </div>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-condensed"
        DataKeyNames="Id" DataSourceID="LogContext" EnableModelValidation="True" AllowPaging="True"
        AllowSorting="True" PageSize="40" RowStyle-CssClass="td" HeaderStyle-CssClass="th">
        <Columns>
            <%--  <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" 
                        ReadOnly="True" SortExpression="Id" />--%>
            <asp:BoundField DataField="EventDateTime" HeaderText="EventDateTime" SortExpression="EventDateTime" />
            <asp:BoundField DataField="EventLevel" HeaderText="EventLevel" SortExpression="EventLevel" />
            <%--<asp:BoundField DataField="UserName" HeaderText="UserName" 
                        SortExpression="UserName" />--%>
            <%-- <asp:BoundField DataField="MachineName" HeaderText="MachineName" 
                        SortExpression="MachineName" />--%>
            <asp:BoundField DataField="EventMessage" HeaderText="EventMessage" SortExpression="EventMessage" />
            <asp:BoundField DataField="ErrorSource" HeaderText="ErrorSource" SortExpression="ErrorSource" />
            <%--  <asp:BoundField DataField="ErrorClass" HeaderText="ErrorClass" 
                        SortExpression="ErrorClass" />--%>
            <%--<asp:BoundField DataField="ErrorMethod" HeaderText="ErrorMethod" 
                        SortExpression="ErrorMethod" />--%>
            <asp:BoundField DataField="ErrorMessage" HeaderText="ErrorMessage" SortExpression="ErrorMessage" />
            <asp:BoundField DataField="InnerErrorMessage" HeaderText="InnerErrorMessage" SortExpression="InnerErrorMessage" />
        </Columns>
        <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
    </asp:GridView>
    <asp:LinqDataSource ID="LogContext" runat="server" ContextTypeName="PlanetComm.DataAccess.PlanetCommDataContext"
        OrderBy="Id" TableName="Logs" OnSelecting="LogContext_Selecting">
        <WhereParameters>
            <asp:ControlParameter Name="EventDateTime" ControlID="ddlDate" PropertyName="Text"
                Type="String" />
        </WhereParameters>
    </asp:LinqDataSource>
</asp:Content>

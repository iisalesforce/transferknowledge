﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SalesHeader.aspx.cs" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Main.Master" Inherits="PlanetComm.WebManagement.SalesHeader" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ID="MainContent">
    <h3>
        Sales Order
    </h3>
    <div>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" CssClass="table table-striped table-bordered table-condensed"
            AutoGenerateColumns="False" DataSourceID="LinqDataSource1" PageSize="40" RowStyle-CssClass="td"
            HeaderStyle-CssClass="th" EnableModelValidation="True" AllowSorting="True">
            <Columns>
                <asp:BoundField DataField="PONUMBER" HeaderText="PONUMBER" ReadOnly="True" SortExpression="PONUMBER" />
                <asp:BoundField DataField="SHIPTO" HeaderText="SHIPTO" ReadOnly="True" SortExpression="SHIPTO" />
                <asp:BoundField DataField="SELLTO" HeaderText="SELLTO" ReadOnly="True" SortExpression="SELLTO" />
                <asp:BoundField DataField="SALESPERSON" HeaderText="SALESPERSON" ReadOnly="True"
                    SortExpression="SALESPERSON" />
                <asp:BoundField DataField="REFERENCE" HeaderText="REFERENCE" ReadOnly="True" SortExpression="REFERENCE" />
                <asp:BoundField DataField="ORDDATE" HeaderText="ORDDATE" ReadOnly="True" SortExpression="ORDDATE" />
                <asp:BoundField DataField="ORDNUMBER" HeaderText="ORDNUMBER" ReadOnly="True" SortExpression="ORDNUMBER" />
                <asp:BoundField DataField="PODATE" HeaderText="PODATE" ReadOnly="True" SortExpression="PODATE" />
                <asp:BoundField DataField="TERM" HeaderText="TERM" ReadOnly="True" SortExpression="TERM" />
                <asp:BoundField DataField="CREDITTERM" HeaderText="CREDITTERM" ReadOnly="True" SortExpression="CREDITTERM" />
                <asp:BoundField DataField="EXPDELIVERYDATE" HeaderText="EXPDELIVERYDATE" ReadOnly="True"
                    SortExpression="EXPDELIVERYDATE" />
                <asp:BoundField DataField="REQDATE" HeaderText="REQDATE" ReadOnly="True" SortExpression="REQDATE" />
                <asp:BoundField DataField="REFQUOTE" HeaderText="REFQUOTE" ReadOnly="True" SortExpression="REFQUOTE" />
                <asp:BoundField DataField="REMARK" HeaderText="REMARK" ReadOnly="True" SortExpression="REMARK" />
                <asp:BoundField DataField="SOTYPE" HeaderText="SOTYPE" ReadOnly="True" SortExpression="SOTYPE" />
                <asp:BoundField DataField="IsDone" HeaderText="IsDone" ReadOnly="True" SortExpression="IsDone" />
            </Columns>
            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </asp:GridView>
    </div>
    <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="PlanetComm.DataAccess.PlanetCommDataContext"
        Select="new (PONUMBER, SHIPTO, SELLTO, SALESPERSON, REFERENCE, ORDDATE, ORDNUMBER, PODATE, TERM, CREDITTERM, EXPDELIVERYDATE, REQDATE, REFQUOTE, REMARK, SOTYPE, IsDone)"
        TableName="SALEORDERs">
    </asp:LinqDataSource>
</asp:Content>

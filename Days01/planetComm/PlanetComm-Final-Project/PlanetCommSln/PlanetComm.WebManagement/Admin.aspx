﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="PlanetComm.WebManagement.Admin"
    MasterPageFile="~/Main.Master" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ID="MainContent">
    <asp:Panel ID="Anonymous" runat="server">
        <div class="control-group">
            <label class="control-label" for="inputEmail">
                User Name :
            </label>
            <div class="controls">
                <asp:TextBox ID="inputEmail" runat="server"></asp:TextBox>
                <%--<input type="text" id="inputEmail" placeholder="Email">--%>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputPassword">
                Password :
            </label>
            <div class="controls">
                <asp:TextBox TextMode="Password" ID="inputPassword" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <asp:Button ID="btnLogin" class="btn" runat="server" Text=" Sign in" OnClick="btnLogin_Click" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="Login" runat="server">
        <h3>
            Admin Panel
        </h3>
        <p>
            <asp:Button class="btn btn-large btn-primary" ID="btnClearLog" runat="server" Text="Delete Log"
                OnClick="btnClearLog_Click" />
            <asp:Button class="btn btn-large btn-primary" ID="btnClearSo" runat="server" Text="Delete SO"
                OnClick="btnClearSo_Click" />
            <asp:Button class="btn btn-large btn-primary" ID="btnClearSoDetail" runat="server"
                Text="Delete SODetail" OnClick="btnClearSoDetail_Click" />
            <asp:Button class="btn btn-large btn-primary" ID="btnClearActual" runat="server"
                Text="Delete Actual" OnClick="btnClearActual_Click" />
            <asp:Button class="btn btn-large btn-primary" ID="btnClearForecast" runat="server"
                Text="Delete Forecast" OnClick="btnClearForecast_Click" />
            <asp:Button class="btn btn-large btn-primary" ID="btnClearCreditNote" runat="server"
                Text="Delete Credit Note" OnClick="btnClearCreditNote_Click" />
        </p>
        <h4>
            Copy XML File</h4>
        <p>
            <asp:Button class="btn btn-large btn-primary" ID="btnCopy" runat="server" Text="Copy"
                OnClick="btnCopy_Click" />
        </p>
    </asp:Panel>
</asp:Content>

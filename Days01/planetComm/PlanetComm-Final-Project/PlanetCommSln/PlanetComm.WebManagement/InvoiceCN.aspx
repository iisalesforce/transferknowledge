﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoiceCN.aspx.cs" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Main.Master" Inherits="PlanetComm.WebManagement.InvoiceCN" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ID="MainContent">
    <div>
        <h3>
            Actual Invoice
        </h3>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="LinqDataSource1"
            PageSize="10" RowStyle-CssClass="td" HeaderStyle-CssClass="th" CssClass="table table-striped table-bordered table-condensed"
            EnableModelValidation="True">
            <Columns>
                <asp:BoundField DataField="ORDNUMBER" HeaderText="ORDNUMBER" ReadOnly="True" SortExpression="ORDNUMBER" />
                <asp:BoundField DataField="INVOICENUMBER" HeaderText="INVOICENUMBER" ReadOnly="True"
                    SortExpression="INVOICENUMBER" />
                <asp:BoundField DataField="ACTUALINVOICEDATE" HeaderText="ACTUALINVOICEDATE" ReadOnly="True"
                    SortExpression="ACTUALINVOICEDATE" />
                <asp:BoundField DataField="ACTUALINVOICEAMOUNT" HeaderText="ACTUALINVOICEAMOUNT"
                    ReadOnly="True" SortExpression="ACTUALINVOICEAMOUNT" />
                <asp:BoundField DataField="ACTUALLASTMODIFIEDDATE" HeaderText="ACTUALLASTMODIFIEDDATE"
                    ReadOnly="True" SortExpression="ACTUALLASTMODIFIEDDATE" />
                <asp:BoundField DataField="IsDone" HeaderText="IsDone" ReadOnly="True" SortExpression="IsDone" />
            </Columns>
            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </asp:GridView>
        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="PlanetComm.DataAccess.PlanetCommDataContext"
            Select="new (ORDNUMBER, INVOICENUMBER, ACTUALINVOICEDATE, ACTUALINVOICEAMOUNT, IsDone, ACTUALLASTMODIFIEDDATE)"
            TableName="ACTUALs">
        </asp:LinqDataSource>
    </div>
    <div>
        <h3>
            Forecast Invoice
        </h3>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataSourceID="LinqDataSource2"
            PageSize="10" RowStyle-CssClass="td" HeaderStyle-CssClass="th" CssClass="table table-striped table-bordered table-condensed"
            EnableModelValidation="True">
            <Columns>
                <asp:BoundField DataField="INVOICENO" HeaderText="INVOICENO" ReadOnly="True" SortExpression="INVOICENO" />
                <asp:BoundField DataField="FORECASTINVOICEYEARMONTH" HeaderText="FORECASTINVOICEYEARMONTH"
                    ReadOnly="True" SortExpression="FORECASTINVOICEYEARMONTH" />
                <asp:BoundField DataField="FORECASTINVOICEAMOUNT" HeaderText="FORECASTINVOICEAMOUNT"
                    ReadOnly="True" SortExpression="FORECASTINVOICEAMOUNT" />
                <asp:BoundField DataField="ORDNUMBER" HeaderText="ORDNUMBER" ReadOnly="True" SortExpression="ORDNUMBER" />
                <asp:BoundField DataField="FORECASTINVOICEDATE" HeaderText="FORECASTINVOICEDATE"
                    ReadOnly="True" SortExpression="FORECASTINVOICEDATE" />
                <asp:BoundField DataField="IsDone" HeaderText="IsDone" ReadOnly="True" SortExpression="IsDone" />
            </Columns>
            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </asp:GridView>
        <asp:LinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="PlanetComm.DataAccess.PlanetCommDataContext"
            Select="new (INVOICENO, FORECASTINVOICEYEARMONTH, IsDone, FORECASTINVOICEAMOUNT, ORDNUMBER, FORECASTINVOICEDATE)"
            TableName="FORECASTs">
        </asp:LinqDataSource>
    </div>
    <div>
        <h3>
            Credit Note
        </h3>
        <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" DataKeyNames="CREDITNOTENUMBER"
            DataSourceID="LinqDataSource3" PageSize="10" RowStyle-CssClass="td" HeaderStyle-CssClass="th"
            CssClass="table table-striped table-bordered table-condensed" EnableModelValidation="True">
            <Columns>
                <asp:BoundField DataField="ORDNUMBER" HeaderText="ORDNUMBER" SortExpression="ORDNUMBER" />
                <asp:BoundField DataField="CREDITNOTENUMBER" HeaderText="CREDITNOTENUMBER" ReadOnly="True"
                    SortExpression="CREDITNOTENUMBER" />
                <asp:BoundField DataField="CREDITNOTEDATE" HeaderText="CREDITNOTEDATE" SortExpression="CREDITNOTEDATE" />
                <asp:BoundField DataField="CREDITNOTEAMOUNT" HeaderText="CREDITNOTEAMOUNT" SortExpression="CREDITNOTEAMOUNT" />
                <asp:BoundField DataField="CREDITNOTELASTMODIFIEDDATE" HeaderText="CREDITNOTELASTMODIFIEDDATE"
                    SortExpression="CREDITNOTELASTMODIFIEDDATE" />
                <asp:BoundField DataField="IsDone" HeaderText="IsDone" SortExpression="IsDone" />
            </Columns>
            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </asp:GridView>
        <asp:LinqDataSource ID="LinqDataSource3" runat="server" ContextTypeName="PlanetComm.DataAccess.PlanetCommDataContext"
            TableName="CREDITNOTEs">
        </asp:LinqDataSource>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using PlanetComm.WebManagement.Model;
using NLog;


namespace PlanetComm.WebManagement
{
    public partial class DataLoaderLog : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ddlDataLoadLog_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdfPath.Value = "";
            if (ddlDataLoadLog.SelectedValue != "0")
            {
                var path = ConfigurationManager.AppSettings["cliq"];
                var logPath = @"\log\";
                var logFile = "";
                switch (ddlDataLoadLog.SelectedValue)
                {
                    case "1": //Actual
                        logFile = path + "Actual" + logPath;
                        break;
                    case "2": //CreditNote
                        logFile = path + "CreditNote" + logPath;
                        break;
                    case "3"://Forecase
                        logFile = path + "Forecase" + logPath;
                        break;
                    case "4"://SalesHeader
                        logFile = path + "SalesHeader" + logPath;
                        break;
                    case "5"://SalesHeaderSalesItem
                        logFile = path + "SalesItem" + logPath;
                        break;
                    default:
                        break;
                }
                hdfPath.Value = logFile;
                if (!String.IsNullOrEmpty(logFile))
                {
                    List<LogFile> dirs = new List<LogFile>();
                    Directory.GetFiles(logFile, "*.csv").ToList().ForEach(file =>
                    {
                        dirs.Add(new LogFile
                        {
                            FileName = Path.GetFileName(file),
                            FilePath = file
                        });
                    });
                    if (dirs.Count > 0)
                    {
                        rptFile.DataSource = dirs;
                        rptFile.DataBind();
                    }
                }


            }
        }

        protected void LinkButton_Command(object sender, CommandEventArgs e)
        {
            Response.ContentType = "Application/x-msexcel";
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", ((LinkButton)sender).Text));

            //Get the physical path to the file.
            string FilePath = (String)e.CommandArgument;
            //Write the file directly to the HTTP content output stream.
            Response.WriteFile(FilePath);
            Response.End();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(hdfPath.Value))
            {
                try
                {
                    Array.ForEach(Directory.GetFiles(hdfPath.Value), File.Delete);
                    rptFile.DataSource = null;
                    rptFile.DataBind();
                }
                catch (System.IO.IOException ex)
                {
                    var logger = LogManager.GetCurrentClassLogger();
                    logger.Log(LogLevel.Error, ex.Message);
                    return;
                }
            }
        }
    }
}
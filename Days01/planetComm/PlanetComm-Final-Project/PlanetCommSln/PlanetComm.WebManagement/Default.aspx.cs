﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PlanetComm.DataAccess;
using System.Globalization;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using NLog;

namespace PlanetComm.WebManagement
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // var logger = LogManager.GetCurrentClassLogger();
            if (!IsPostBack)
            {
                //  logger.Log(LogLevel.Info, "   Browns Default  ");
                DateTime date = DateTime.Now;
                for (int i = 0; i <= 15; i++)
                {
                    ddlDate.Items.Add(new ListItem(date.ToString("MM/dd/yyyy"), date.ToString("MM/dd/yyyy")));
                    date = date.AddDays(-1);
                }
            }
        }
        protected void LogContext_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var db = new PlanetCommDataContext();
            var subjectFilter = e.WhereParameters["EventDateTime"].ToString();
            DateTime dt = DateTime.ParseExact(subjectFilter, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            var dtstart = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
            var dtend = dtstart.AddDays(1).AddMilliseconds(-1);
            var reporters = from spName in db.Logs
                            where spName.EventDateTime > dtstart && spName.EventDateTime <= dtend
                            select spName;
            e.Result = reporters;
        }
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            //  var logger = LogManager.GetCurrentClassLogger();
            //   logger.Log(LogLevel.Info, "   Download File  Default  ");
            var subjectFilter = ddlDate.SelectedValue;
            DateTime dt = DateTime.ParseExact(subjectFilter, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            var dtstart = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
            var dtend = dtstart.AddDays(1).AddMilliseconds(-1);
            var db = new PlanetCommDataContext();
            var reporters = from spName in db.Logs
                            where spName.EventDateTime > dtstart && spName.EventDateTime <= dtend
                            select spName;

            if (reporters.Count() > 0)
            {
                using (ExcelPackage pck = new ExcelPackage())
                {
                    //Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Log File");

                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                    ws.Cells["A1"].LoadFromCollection(reporters, true);
                    using (ExcelRange rng = ws.Cells["A1:K1"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }

                    using (ExcelRange col = ws.Cells[2, 2, 2 + reporters.Count(), 2])
                    {
                        col.Style.Numberformat.Format = "dd-mm-yyyy HH:ss";

                    }
                    ws.Cells["A:K"].AutoFitColumns();
                    //Write it back to the client
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=Log_On_" + subjectFilter + ".xlsx");
                    Response.BinaryWrite(pck.GetAsByteArray());
                    Response.End();
                }

            }
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using System.IO;
using System.Data;
using NLog;
using System.Configuration;
using PlanetComm.DataAccess;
using PlanetComm.Lib;

namespace PlanetComm.CsvTaskToSql
{
    [DisallowConcurrentExecution]
    public class CreditnoteTask : IJob
    {
        public void Execute(IJobExecutionContext context)
        {

            var logger = LogManager.GetCurrentClassLogger();
            String filePath = ConfigurationManager.AppSettings["ftp"];
            if (Directory.GetFiles(filePath).Count() > 0)
            {
                using (PlanetCommDataContext db = new PlanetCommDataContext())
                {
                    var file = filePath + "\\CREDITNOTE.xml";

                    if (File.Exists(file))
                    {
                        logger.Log(LogLevel.Info, "CREDITNOTE.xml File Exist");
                        string xml = System.IO.File.ReadAllText(file);
                        var xmlSR = new System.IO.StringReader(XmlHelper.CleanInvalidXmlChars(xml));
                        using (DataSet dsStore = new DataSet())
                        {
                            dsStore.ReadXml(xmlSR);
                            var CreditNotes = dsStore.Tables[0];
                            xmlSR.Close();
                            xmlSR.Dispose();
                            logger.Log(LogLevel.Info, "Read " + CreditNotes.Rows.Count + " Credit Note Item Record ");
                            foreach (DataRow item in CreditNotes.Rows)
                            {

                                logger.Log(LogLevel.Info, "Read Credit Note number : " + item["CREDITNOTENUMBER"].ToString());
                                bool isNew = false;
                                CREDITNOTE cn;
                                cn = db.CREDITNOTEs.FirstOrDefault(x => x.CREDITNOTENUMBER == item["CREDITNOTENUMBER"].ToString().Trim());

                                if (cn == null)
                                {
                                    cn = new CREDITNOTE();
                                    isNew = true;
                                    cn.ORDNUMBER = item["ORDNUMBER"].ToString().Trim();
                                    cn.CREDITNOTENUMBER = item["CREDITNOTENUMBER"].ToString().Trim();
                                    var date = item["CREDITNOTEDATE"].ToString();
                                    cn.CREDITNOTEDATE = DateTime.ParseExact(date.Substring(0, 4) + "-" + date.Substring(4, 2) + "-" + date.Substring(6, 2), "yyyy-MM-dd", null);

                                }
                                cn.IsDone = "N";
                                var time = Double.Parse(item["CREDITNOTELASTMODDIFIELDTIME"].ToString());
                                TimeSpan ts = TimeSpan.FromHours(time);
                                int H = ts.Hours;
                                int M = ts.Minutes;
                                var cndate = item["CREDITNOTEDATE"].ToString();
                                DateTime cdate = new DateTime(int.Parse(cndate.Substring(0, 4)),
                                    int.Parse(cndate.Substring(4, 2)),
                                    int.Parse(cndate.Substring(6, 2)),
                                    H, M, 0);
                                cn.CREDITNOTEAMOUNT = Decimal.Parse(item["CREDITNOTEAMOUNT"].ToString().Trim()).ToString("0.00");
                                cn.CREDITNOTELASTMODIFIEDDATE = cdate;
                                if (isNew)
                                {
                                    db.CREDITNOTEs.InsertOnSubmit(cn);
                                }
                            }
                            try
                            {
                                db.SubmitChanges();
                                logger.Log(LogLevel.Info, "Save Credit Note To DataBase");
                            }
                            catch (Exception ex)
                            {

                                logger.Log(LogLevel.Error, ex.Message);
                            }

                        }
                        System.Threading.Thread.Sleep(1000);
                        int loop = 0;
                        while (loop < 3)
                        {
                            try
                            {
                                File.Delete(file);
                                loop = 3;
                            }
                            catch (Exception ex)
                            {
                                System.Threading.Thread.Sleep(1000);
                                loop++;
                            }
                        }
                    }
                }
            }
        }
    }
}

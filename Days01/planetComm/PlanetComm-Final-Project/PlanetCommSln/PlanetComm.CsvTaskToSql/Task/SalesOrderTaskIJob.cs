﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using System.IO;
using System.Data;
using NLog;
using System.Configuration;
using PlanetComm.DataAccess;
using PlanetComm.Lib;

namespace PlanetComm.CsvTaskToSql
{
    [DisallowConcurrentExecution]
    public class SalesOrderTaskIJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var logger = LogManager.GetCurrentClassLogger();
            String filePath = ConfigurationManager.AppSettings["ftp"];
            if (Directory.GetFiles(filePath).Count() > 0)
            {
                using (PlanetCommDataContext db = new PlanetCommDataContext())
                {
                    var file = filePath + "\\SO.xml";
                    if (File.Exists(file))
                    {

                        logger.Log(LogLevel.Info, "SO.xml File Exist");


                        if (File.Exists(file))
                        {
                            using (DataSet dsStore = new DataSet())
                            {

                                string xml = System.IO.File.ReadAllText(file);
                                var xmlSR = new System.IO.StringReader(XmlHelper.CleanInvalidXmlChars(xml));
                                dsStore.ReadXml(xmlSR);
                                var SalesOrders = dsStore.Tables[0];
                                xmlSR.Close();

                                logger.Log(LogLevel.Info, "Read  " + SalesOrders.Rows.Count + " Sales Order Record ");

                                foreach (DataRow item in SalesOrders.Rows)
                                {
                                    logger.Log(LogLevel.Info, "Read Order Number  " + item["ORDNUMBER"]);

                                    try
                                    {
                                        bool isNew = false;
                                        SALEORDER or;
                                        or = db.SALEORDERs.FirstOrDefault(x => x.ORDNUMBER == item["ORDNUMBER"].ToString().Trim());
                                        if (or == null)
                                        {
                                            // logger.Log(LogLevel.Info, "or  is null ");
                                            or = new SALEORDER();
                                            isNew = true;
                                            or.ORDNUMBER = item["ORDNUMBER"].ToString().Trim();
                                        }
                                        or.SELLTO = item["SELLTO"].ToString().Trim();


                                        or.IsDone = "N";
                                        var ordate = item["ORDDATE"].ToString();
                                        or.CUSTOMERCODE = item["SELLTO"].ToString();
                                        or.ORDDATE = DateTime.ParseExact(ordate.Substring(0, 4) + "-" + ordate.Substring(4, 2) + "-" + ordate.Substring(6, 2), "yyyy-MM-dd", null);
                                        or.REFERENCE = item["REFERENCE"].ToString().Trim();
                                        or.SALESPERSON = item["SALESPERSON"].ToString().Trim();
                                        or.SELLTO = item["SELLTO"].ToString().Trim();
                                        or.SHIPTO = item["SHIPTO"].ToString().Trim();
                                        or.PONUMBER = item["PONUMBER"].ToString().Trim();
                                        var podate = item["PODATE"].ToString();
                                        or.PODATE = DateTime.ParseExact(podate.Substring(0, 4) + "-" + podate.Substring(4, 2) + "-" + podate.Substring(6, 2), "yyyy-MM-dd", null);
                                        or.TERM = item["TERM"].ToString().Trim();
                                        or.CREDITTERM = item["CREDITTERM"].ToString().Trim();
                                        var reqdate = item["REQDATE"].ToString();
                                        or.REQDATE = DateTime.ParseExact(reqdate.Substring(0, 4) + "-" + reqdate.Substring(4, 2) + "-" + reqdate.Substring(6, 2), "yyyy-MM-dd", null);
                                        var exdate = item["EXPDELIVERYDATE"].ToString();
                                        or.EXPDELIVERYDATE = DateTime.ParseExact(exdate.Substring(0, 4) + "-" + exdate.Substring(4, 2) + "-" + exdate.Substring(6, 2), "yyyy-MM-dd", null);
                                        or.REFQUOTE = item["REFQUOTE"].ToString().Trim();
                                        or.REMARK = item["SOTYPE"].ToString().Trim();
                                        or.SOTYPE = item["SOTYPE"].ToString().Trim();
                                        var lastdate = item["LASTMODIFYDATE"].ToString();

                                        // status
                                        string intstatus = item["STATUS"].ToString();
                                        switch (intstatus)
                                        {
                                            case "1":
                                                or.STATUS = "Incomplete/Not Included";
                                                break;
                                            case "2":
                                                or.STATUS = "Incomplete/Included";
                                                break;
                                            case "3":
                                                or.STATUS = "Complete/Not Included";
                                                break;
                                            case "4":
                                                or.STATUS = "Complete/Included";
                                                break;
                                            case "5":
                                                or.STATUS = "Complete/Day End";
                                                break;
                                            default:
                                                break;
                                        }
                                        or.LASTMODIFYDATE = DateTime.ParseExact(lastdate.Substring(0, 4) + "-" + lastdate.Substring(4, 2) + "-" + lastdate.Substring(6, 2), "yyyy-MM-dd", null);
                                        if (isNew)
                                        {
                                            db.SALEORDERs.InsertOnSubmit(or);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Log(LogLevel.Error, "Error : " + ex.Message);
                                    }
                                }
                                try
                                {
                                    db.SubmitChanges();
                                    logger.Log(LogLevel.Info, "Save All Sale Order To DataBase");
                                }
                                catch (Exception ex)
                                {

                                    logger.Log(LogLevel.Error, ex.Message);
                                }
                            }
                        }

                        System.Threading.Thread.Sleep(1000);
                        int loop = 0;
                        while (loop < 3)
                        {
                            try
                            {
                                File.Delete(file);
                                loop = 3;
                            }
                            catch (Exception ex)
                            {
                                System.Threading.Thread.Sleep(1000);
                                loop++;
                            }
                        }
                    }
                }
            }
            else
            {
                logger.Log(LogLevel.Info, "SO.xml File Is Not Exist");
            }
        }
    }
}

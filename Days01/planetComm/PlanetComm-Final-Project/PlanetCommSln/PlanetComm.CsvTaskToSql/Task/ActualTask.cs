﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using System.IO;
using System.Data;
using NLog;
using System.Configuration;
using PlanetComm.DataAccess;
using PlanetComm.Lib;


namespace PlanetComm.CsvTaskToSql
{
    [DisallowConcurrentExecution]
    public class ActualTask : IJob
    {
        public void Execute(IJobExecutionContext context)
        {

            var logger = LogManager.GetCurrentClassLogger();
            String filePath = ConfigurationManager.AppSettings["ftp"];
            if (Directory.GetFiles(filePath).Count() > 0)
            {
                using (PlanetCommDataContext db = new PlanetCommDataContext())
                {
                    var file = filePath + "\\ACTUAL.xml";

                    if (File.Exists(file))
                    {
                        logger.Log(LogLevel.Info, "ACTUAL.xml File Exist");
                        string xml = System.IO.File.ReadAllText(file);
                        var xmlSR = new System.IO.StringReader(XmlHelper.CleanInvalidXmlChars(xml));
                        using (DataSet dsStore = new DataSet())
                        {

                            dsStore.ReadXml(xmlSR);
                            var SalesOrderItems = dsStore.Tables[0];
                            xmlSR.Close();
                            xmlSR.Dispose();
                            logger.Log(LogLevel.Info, "Read " + SalesOrderItems.Rows.Count + " Actual Invoice Item Record ");
                            foreach (DataRow item in SalesOrderItems.Rows)
                            {
                                logger.Log(LogLevel.Info, "Read Invoice number : " + item["INVNUMBER"].ToString());
                                bool isNew = false;
                                ACTUAL ac;
                                ac = db.ACTUALs.FirstOrDefault(x => x.INVOICENUMBER == item["INVNUMBER"].ToString().Trim());
                                if (ac == null)
                                {
                                    ac = new ACTUAL();
                                    isNew = true;
                                    ac.ORDNUMBER = item["ORDNUMBER"].ToString().Trim();
                                    var podate = item["INVDATE"].ToString();
                                    ac.INVOICENUMBER = item["INVNUMBER"].ToString();
                                    ac.ACTUALINVOICEDATE = DateTime.ParseExact(podate.Substring(0, 4) + "-" + podate.Substring(4, 2) + "-" + podate.Substring(6, 2), "yyyy-MM-dd", null);
                                }
                                ac.IsDone = "N";
                                ac.ACTUALINVOICEAMOUNT = Decimal.Parse(item["AMOUNT"].ToString().Trim()).ToString("0.00");
                                if (isNew)
                                {
                                    db.ACTUALs.InsertOnSubmit(ac);
                                }
                            }

                            try
                            {
                                db.SubmitChanges();
                                logger.Log(LogLevel.Info, "Save Invoice Actual To DataBase");
                            }
                            catch (Exception ex)
                            {

                                logger.Log(LogLevel.Error, ex.Message);
                            }
                        }
                        System.Threading.Thread.Sleep(1000);
                        int loop = 0;
                        while (loop < 3)
                        {
                            try
                            {
                                File.Delete(file);
                                loop = 3;
                            }
                            catch (Exception ex)
                            {
                                System.Threading.Thread.Sleep(1000);
                                loop++;
                            }
                        }
                    }
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using System.IO;
using System.Data;
using NLog;
using System.Configuration;
using PlanetComm.DataAccess;
using PlanetComm.Lib;
namespace PlanetComm.CsvTaskToSql
{
    [DisallowConcurrentExecution]
    public class SalesOrderDetailTask : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var logger = LogManager.GetCurrentClassLogger();
            String filePath = ConfigurationManager.AppSettings["ftp"];
            if (Directory.GetFiles(filePath).Count() > 0)
            {
                using (PlanetCommDataContext db = new PlanetCommDataContext())
                {
                    var file = filePath + "\\SODetail.xml";




                    if (File.Exists(file))
                    {
                        logger.Log(LogLevel.Info, "SODetail.xml File Exist");
                        string xml = System.IO.File.ReadAllText(file);
                        var xmlSR = new System.IO.StringReader(XmlHelper.CleanInvalidXmlChars(xml));
                        using (DataSet dsStore = new DataSet())
                        {

                            dsStore.ReadXml(xmlSR);
                            var SalesOrderItems = dsStore.Tables[0];


                            xmlSR.Close();
                            xmlSR.Dispose();

                            logger.Log(LogLevel.Info, "Read " + SalesOrderItems.Rows.Count + " Sales Order Item Record ");
                            foreach (DataRow item in SalesOrderItems.Rows)
                            {
                                logger.Log(LogLevel.Info, "Read Order Number : " + item["ORDNUMBER"] + "  Line : " + item["LINENUM"].ToString().Trim());
                                try
                                {
                                    bool isNew = false;
                                    SALEORDER_ITEM or;
                                    or = db.SALEORDER_ITEMs.FirstOrDefault(x => x.ORDNUMBER == item["ORDNUMBER"].ToString().Trim() && x.ITEM == item["LINENUM"].ToString().Trim());
                                    if (or == null)
                                    {
                                        or = new SALEORDER_ITEM();
                                        isNew = true;
                                        or.ORDNUMBER = item["ORDNUMBER"].ToString().Trim();
                                    }



                                    or.IsDone = "N";

                                    or.ORDNUMBER = item["ORDNUMBER"].ToString().Trim();
                                    or.ITEM = item["LINENUM"].ToString().Trim();
                                    or.PRODUCTCODE = item["PRODUCTCODE"].ToString().Trim();
                                    or.QUANTITY = item["QUANTITY"].ToString().Trim();
                                    or.PRODUCTDESC = item["PRODUCTDESC"].ToString().Trim();
                                    or.DISCOUNTAMOUNT = item["DISCOUNTAMOUNT"].ToString().Trim();
                                    or.UNITPRICEINCDIS = item["UNITPRICEINCDIS"].ToString().Trim();
                                    or.TOTALAMOUNT = item["TOTALAMOUNT"].ToString().Trim();
                                    or.STOCKAVA = item["STOCKAVA"].ToString().Trim();
                                    or.COMMITQTY = item["COMMITQTY"].ToString().Trim();
                                    or.BALQTY = item["BALQTY"].ToString().Trim();
                                    or.BOQTY = item["BOQTY"].ToString().Trim();
                                    or.POQTY = item["POQTY"].ToString().Trim();
                                    or.SHIPSTOKOUT = item["SHIPSTOCKOUT"].ToString().Trim();
                                    or.INVSTOKOUT = item["INVSTOCKOUT"].ToString().Trim();
                                    or.POQTY = item["POQTY"].ToString().Trim();



                                    var stockdate = item["STOCKOUTDATE"].ToString().Trim();
                                    if (stockdate == "0")
                                    {
                                        or.STOCKOUTDATE = DateTime.Now;
                                    }
                                    else
                                    {
                                        or.STOCKOUTDATE = DateTime.ParseExact(stockdate.Substring(0, 4) + "-" + stockdate.Substring(4, 2) + "-" + stockdate.Substring(6, 2), "yyyy-MM-dd", null);
                                    }
                                    if (item["SITE"].ToString().Trim() == "")
                                    {

                                        or.SITE = "HO";
                                    }
                                    else
                                    {
                                        or.SITE = item["SITE"].ToString().Trim();
                                    }

                                    string intstatus = item["STATUS"].ToString();
                                    switch (intstatus)
                                    {
                                        case "0":
                                            or.STATUS = "Not completed";
                                            break;
                                        case "1":
                                            or.STATUS = "Completed/Not In Database";
                                            break;
                                        case "2":
                                            or.STATUS = "Completed";
                                            break;
                                        case "3":
                                            or.STATUS = "Processed by day end";
                                            break;
                                        default:
                                            break;
                                    }


                                    if (isNew)
                                    {
                                        db.SALEORDER_ITEMs.InsertOnSubmit(or);

                                    }
                                }
                                catch (Exception ex)
                                {

                                    logger.Log(LogLevel.Info, "Error  " + ex.Message);
                                }

                            }


                            try
                            {

                                db.SubmitChanges();
                                logger.Log(LogLevel.Info, "Save All Sale Order Item To DataBase");
                            }
                            catch (Exception ex)
                            {
                                logger.Log(LogLevel.Error, "Error : " + ex.Message);
                            }
                        }

                        System.Threading.Thread.Sleep(1000);
                        int loop = 0;
                        while (loop < 3)
                        {
                            try
                            {
                                File.Delete(file);
                                loop = 3;
                            }
                            catch (Exception ex)
                            {
                                System.Threading.Thread.Sleep(1000);
                                loop++;
                            }
                        }
                    }
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using System.IO;
using System.Data;
using NLog;
using System.Configuration;
using PlanetComm.DataAccess;
using PlanetComm.Lib;

namespace PlanetComm.CsvTaskToSql
{
    [DisallowConcurrentExecution]
    public class ForecastTask : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var logger = LogManager.GetCurrentClassLogger();
            String filePath = ConfigurationManager.AppSettings["ftp"];
            if (Directory.GetFiles(filePath).Count() > 0)
            {

                logger.Log(LogLevel.Info, "Read Directory For Forcast");
                using (PlanetCommDataContext db = new PlanetCommDataContext())
                {
                    var file = filePath + "\\FORECAST.xml";
                    if (File.Exists(file))
                    {
                        logger.Log(LogLevel.Info, "FORECAST.xml File Exist");
                        string xml = System.IO.File.ReadAllText(file);
                        var xmlSR = new System.IO.StringReader(XmlHelper.CleanInvalidXmlChars(xml));


                        using (DataSet dsStore = new DataSet())
                        {

                            logger.Log(LogLevel.Info, "Start to read Forecast");

                            dsStore.ReadXml(xmlSR);
                            var Forcast = dsStore.Tables[0];
                            xmlSR.Close();
                            xmlSR.Dispose();
                            logger.Log(LogLevel.Info, "Read " + Forcast.Rows.Count + " Invoice Forecast Record ");
                            foreach (DataRow item in Forcast.Rows)
                            {

                                logger.Log(LogLevel.Info, "Read Forecase number : " + item["SO"].ToString());

                                bool isNew = false;
                                FORECAST fo;

                                try
                                {
                                    PlanetCommDataContext dbf = new PlanetCommDataContext();
                                    var forecasts = dbf.FORECASTs.Where(x => x.ORDNUMBER == item["SO"].ToString().Trim()).ToList();
                                    if (forecasts != null)
                                    {
                                        foreach (var forecast in forecasts)
                                        {
                                            forecast.FORECASTINVOICEAMOUNT = "0";
                                        }
                                    }
                                    dbf.SubmitChanges();
                                }
                                catch (Exception ex)
                                {
                                    logger.Log(LogLevel.Info, "Error : " + ex.Message);
                                }



                                fo = db.FORECASTs.FirstOrDefault(x => x.ORDNUMBER == item["SO"].ToString().Trim()
                                    && x.FORECASTINVOICEYEARMONTH == item["FORECASTDATE"].ToString().Trim()
                                    );
                                if (fo == null)
                                {
                                    fo = new FORECAST();
                                    isNew = true;
                                    fo.INVOICENO = DateTime.Now.Ticks.ToString();
                                    fo.ORDNUMBER = item["SO"].ToString().Trim();
                                    var podate = item["FORECASTDATE"].ToString();
                                    fo.FORECASTINVOICEYEARMONTH = item["FORECASTDATE"].ToString().Trim();
                                    fo.FORECASTINVOICEDATE = DateTime.ParseExact(podate.Substring(0, 4) + "-" + podate.Substring(4, 2) + "-01", "yyyy-MM-dd", null);

                                }
                                // Update  Invoice forecast amout
                                fo.IsDone = "N";
                                fo.FORECASTINVOICEAMOUNT = Decimal.Parse(item["FORECASTAMOUNT"].ToString().Trim()).ToString("0.00");
                                if (isNew)
                                {
                                    db.FORECASTs.InsertOnSubmit(fo);
                                }
                            }
                            try
                            {
                                db.SubmitChanges();
                                logger.Log(LogLevel.Info, "Save Invoice Forecast To DataBase");
                            }
                            catch (Exception ex)
                            {

                                logger.Log(LogLevel.Error, ex.Message);
                            }

                        }
                        System.Threading.Thread.Sleep(1000);
                        int loop = 0;
                        while (loop < 3)
                        {
                            try
                            {
                                File.Delete(file);
                                loop = 3;
                            }
                            catch (Exception ex)
                            {
                                System.Threading.Thread.Sleep(1000);
                                loop++;
                            }
                        }
                    }
                }
            }
        }
    }
}

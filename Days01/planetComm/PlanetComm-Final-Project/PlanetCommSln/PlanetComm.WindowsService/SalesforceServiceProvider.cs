﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using NLog;
using System.Web.Services.Protocols;
using PlanetComm.WindowsService.SFService;

namespace PlanetComm.WindowsService
{
    public class SalesforceServiceProvider : IDisposable
    {
        private string _username;
        public string Username
        {
            get
            {
                if (String.IsNullOrEmpty(_username))
                {
                    _username = ConfigurationManager.AppSettings["user"];
                }
                return _username;
            }
        }
        private string _prousername;
        public string ProductUsername
        {
            get
            {
                if (String.IsNullOrEmpty(_prousername))
                {
                    _prousername = ConfigurationManager.AppSettings["productuser"];
                }
                return _prousername;
            }
        }
        private string _productpassword;
        public string Productpassword
        {
            get
            {
                if (String.IsNullOrEmpty(_productpassword))
                {
                    _productpassword = ConfigurationManager.AppSettings["productpassword"];
                }
                return _productpassword;
            }
        }
        private string _tokenPro;
        public string TokenPro
        {
            get
            {
                if (String.IsNullOrEmpty(_tokenPro))
                {
                    _tokenPro = ConfigurationManager.AppSettings["producttoken"];
                }
                return _tokenPro;
            }
        }
        private string _pass;
        public string Password
        {
            get
            {
                if (String.IsNullOrEmpty(_pass))
                {
                    _pass = ConfigurationManager.AppSettings["pass"];
                }
                return _pass;
            }
        }
        private string _token;
        public string Token
        {
            get
            {
                if (String.IsNullOrEmpty(_token))
                {
                    _token = ConfigurationManager.AppSettings["token"];
                }
                return _token;
            }
        }
        private SforceService sf;
        private LoginResult rs;
        private Logger logger;
        public SalesforceServiceProvider()
        {
            logger = LogManager.GetCurrentClassLogger();
        }
        public SforceService Login()
        {
            sf = new SforceService();
            try
            {
                logger.Log(LogLevel.Info, "Logging in...");
                rs = sf.login(Username, Password + Token);
                // rs = sf.login("ii_support@planetcomm.com", "superice11SiGHy31dEbe0h3FqbqvXgAyqo");
            }
            catch (SoapException e)
            {
                // Write the fault code to the console
                logger.Log(LogLevel.Error, e.Code);

                // Write the fault message to the console
                logger.Log(LogLevel.Error, "An unexpected error has occurred: " + e.Message);

                // Write the stack trace to the console
                logger.Log(LogLevel.Error, e.StackTrace);
                return null;
                // Return False to indicate that the login was not successful
                //return false;
            }
            // Check if the password has expired
            if (rs.passwordExpired)
            {
                logger.Log(LogLevel.Info, "An error has occurred. Your password has expired.");
                return null;
                // return false;
            }
            // Set the returned service endpoint URL
            sf.Url = rs.serverUrl;
            // Set the SOAP header with the session ID returned by
            // the login result. This will be included in all
            // API calls.
            sf.SessionHeaderValue = new SessionHeader();
            sf.SessionHeaderValue.sessionId = rs.sessionId;
            return sf;
        }
        public SforceService LoginProduction()
        {
            sf = new SforceService();
            try
            {
                logger.Log(LogLevel.Info, "Logging in...");
                rs = sf.login(ProductUsername, Productpassword + TokenPro);
            }
            catch (SoapException e)
            {
                // Write the fault code to the console
                logger.Log(LogLevel.Error, e.Code);

                // Write the fault message to the console
                logger.Log(LogLevel.Error, "An unexpected error has occurred: " + e.Message);

                // Write the stack trace to the console
                logger.Log(LogLevel.Error, e.StackTrace);
                return null;
                // Return False to indicate that the login was not successful
                //return false;
            }
            // Check if the password has expired
            if (rs.passwordExpired)
            {
                logger.Log(LogLevel.Info, "An error has occurred. Your password has expired.");
                return null;
                // return false;
            }
            // Set the returned service endpoint URL
            sf.Url = rs.serverUrl;
            // Set the SOAP header with the session ID returned by
            // the login result. This will be included in all
            // API calls.
            sf.SessionHeaderValue = new SessionHeader();
            sf.SessionHeaderValue.sessionId = rs.sessionId;
            return sf;
        }
        public void Dispose()
        {

            sf.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceProcess;
using CronNET;
using System.IO;
using NLog;
using System.Configuration;
using PlanetComm.DataAccess;
using System.Data;
using PlanetComm.Lib;
using System.Diagnostics;
using PlanetComm.WindowsService.SFService;
using System.Web.Services.Protocols;

namespace PlanetComm.WindowsService
{
    partial class PlanetCommService : ServiceBase
    {
        private System.ComponentModel.IContainer components = null;
        static void Main()
        {
            System.ServiceProcess.ServiceBase[] ServicesToRun;
            ServicesToRun =
              new System.ServiceProcess.ServiceBase[] { new PlanetCommService() };
            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = "PlanetCommService";
        }
        public PlanetCommService()
        {
            InitializeComponent();
            this.ServiceName = "PlannetComm Service";
        }
        private static readonly CronDaemon cron_daemon = new CronDaemon();
        protected override void OnStart(string[] args)
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "PlannetComm Service Started !!!");

            //   Cron Expression
            string csvtosalesorder = ConfigurationManager.AppSettings["csvtosalesorder"];
            string csvtosalesorderitem = ConfigurationManager.AppSettings["csvtosalesorderitem"];
            string csvtoforecast = ConfigurationManager.AppSettings["csvtoforecast"];
            string csvtoactual = ConfigurationManager.AppSettings["csvtoactual"];
            string csvtocreditnote = ConfigurationManager.AppSettings["csvtocreditnote"];


            string dataloadersalesorder = ConfigurationManager.AppSettings["dataloadersalesorder"];
            string dataloadersalesorderitim = ConfigurationManager.AppSettings["dataloadersalesorderitim"];
            string dataloadercreditnote = ConfigurationManager.AppSettings["dataloadercreditnote"];


            // SalesOrder  Cron Job
            cron_daemon.AddJob(csvtosalesorder, SalesOrderTask);
            cron_daemon.AddJob(csvtosalesorderitem, SalesOrderDetailTask);
            cron_daemon.AddJob(csvtoforecast, ForecastTask);
            cron_daemon.AddJob(csvtoactual, ActualTask);
            cron_daemon.AddJob(csvtocreditnote, CreditNoteTask);

            //  Data Loader
            cron_daemon.AddJob(dataloadersalesorderitim, SalesHeaderSalesItemDataLoad);
            cron_daemon.AddJob(dataloadercreditnote, InvoiceCNToSalesforce);

            cron_daemon.Start();


        }
        protected override void OnStop()
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "   PlannetComm Service Stoped !!!  ");
        }
        #region    =====================  Service  ====================
        private void SalesOrderTask()
        {
            var logger = LogManager.GetCurrentClassLogger();
            String filePath = ConfigurationManager.AppSettings["ftp"];
            if (Directory.GetFiles(filePath).Count() > 0)
            {
                using (PlanetCommDataContext db = new PlanetCommDataContext())
                {
                    var file = filePath + "\\SO.xml";
                    if (File.Exists(file))
                    {
                        logger.Log(LogLevel.Info, "SO.xml File Exist");

                        if (File.Exists(file))
                        {
                            using (DataSet dsStore = new DataSet())
                            {

                                string xml = System.IO.File.ReadAllText(file);
                                var xmlSR = new System.IO.StringReader(XmlHelper.CleanInvalidXmlChars(xml));
                                dsStore.ReadXml(xmlSR);
                                var SalesOrders = dsStore.Tables[0];
                                xmlSR.Close();

                                logger.Log(LogLevel.Info, "Read " + SalesOrders.Rows.Count + " Sales Order Record ");

                                foreach (DataRow item in SalesOrders.Rows)
                                {
                                    logger.Log(LogLevel.Info, "Read Order Number  " + item["ORDNUMBER"]);
                                    try
                                    {
                                        bool isNew = false;
                                        SALEORDER or;
                                        or = db.SALEORDERs.FirstOrDefault(x => x.ORDNUMBER == item["ORDNUMBER"].ToString().Trim());
                                        if (or == null)
                                        {
                                            logger.Log(LogLevel.Info, "or  is null ");
                                            or = new SALEORDER();
                                            isNew = true;
                                            or.ORDNUMBER = item["ORDNUMBER"].ToString().Trim();
                                        }
                                        or.SELLTO = item["SELLTO"].ToString().Trim();
                                        or.IsDone = "N";
                                        var ordate = item["ORDDATE"].ToString();
                                        or.CUSTOMERCODE = item["SELLTO"].ToString();
                                        or.ORDDATE = DateTime.ParseExact(ordate.Substring(0, 4) + "-" + ordate.Substring(4, 2) + "-" + ordate.Substring(6, 2), "yyyy-MM-dd", null);
                                        or.REFERENCE = item["REFERENCE"].ToString().Trim();
                                        or.SALESPERSON = item["SALESPERSON"].ToString().Trim();
                                        or.SELLTO = item["SELLTO"].ToString().Trim();
                                        or.SHIPTO = item["SHIPTO"].ToString().Trim();
                                        or.PONUMBER = item["PONUMBER"].ToString().Trim();
                                        var podate = item["PODATE"].ToString();
                                        or.PODATE = DateTime.ParseExact(podate.Substring(0, 4) + "-" + podate.Substring(4, 2) + "-" + podate.Substring(6, 2), "yyyy-MM-dd", null);
                                        or.TERM = item["TERM"].ToString().Trim();
                                        or.CREDITTERM = item["CREDITTERM"].ToString().Trim();
                                        var reqdate = item["REQDATE"].ToString();
                                        or.REQDATE = DateTime.ParseExact(reqdate.Substring(0, 4) + "-" + reqdate.Substring(4, 2) + "-" + reqdate.Substring(6, 2), "yyyy-MM-dd", null);
                                        var exdate = item["EXPDELIVERYDATE"].ToString();
                                        or.EXPDELIVERYDATE = DateTime.ParseExact(exdate.Substring(0, 4) + "-" + exdate.Substring(4, 2) + "-" + exdate.Substring(6, 2), "yyyy-MM-dd", null);
                                        or.REFQUOTE = item["REFQUOTE"].ToString().Trim();
                                        or.REMARK = item["SOTYPE"].ToString().Trim();
                                        or.SOTYPE = item["SOTYPE"].ToString().Trim();
                                        var lastdate = item["LASTMODIFYDATE"].ToString();

                                        // status
                                        string intstatus = item["STATUS"].ToString();
                                        switch (intstatus)
                                        {
                                            case "1":
                                                or.STATUS = "Incomplete/Not Included";
                                                break;
                                            case "2":
                                                or.STATUS = "Incomplete/Included";
                                                break;
                                            case "3":
                                                or.STATUS = "Complete/Not Included";
                                                break;
                                            case "4":
                                                or.STATUS = "Complete/Included";
                                                break;
                                            case "5":
                                                or.STATUS = "Complete/Day End";
                                                break;
                                            default:
                                                break;
                                        }
                                        or.LASTMODIFYDATE = DateTime.ParseExact(lastdate.Substring(0, 4) + "-" + lastdate.Substring(4, 2) + "-" + lastdate.Substring(6, 2), "yyyy-MM-dd", null);
                                        if (isNew)
                                        {
                                            db.SALEORDERs.InsertOnSubmit(or);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Log(LogLevel.Error, "Error : " + ex.Message);
                                    }
                                }
                                try
                                {
                                    db.SubmitChanges();
                                    logger.Log(LogLevel.Info, "Save All Sale Order To DataBase");
                                }
                                catch (Exception ex)
                                {

                                    logger.Log(LogLevel.Error, ex.Message);
                                }
                            }
                        }

                        System.Threading.Thread.Sleep(1000);
                        int loop = 0;
                        while (loop < 3)
                        {
                            try
                            {
                                File.Delete(file);
                                loop = 3;
                            }
                            catch (Exception ex)
                            {
                                System.Threading.Thread.Sleep(1000);
                                loop++;
                            }
                        }
                    }
                }
            }
            else
            {
                logger.Log(LogLevel.Info, "SO.xml File Is Not Exist");
            }
        }
        private void SalesOrderDetailTask()
        {
            var logger = LogManager.GetCurrentClassLogger();
            String filePath = ConfigurationManager.AppSettings["ftp"];
            if (Directory.GetFiles(filePath).Count() > 0)
            {
                using (PlanetCommDataContext db = new PlanetCommDataContext())
                {
                    var file = filePath + "\\SODetail.xml";
                    if (File.Exists(file))
                    {
                        logger.Log(LogLevel.Info, "SODetail.xml File Exist");
                        string xml = System.IO.File.ReadAllText(file);
                        var xmlSR = new System.IO.StringReader(XmlHelper.CleanInvalidXmlChars(xml));
                        using (DataSet dsStore = new DataSet())
                        {
                            dsStore.ReadXml(xmlSR);
                            var SalesOrderItems = dsStore.Tables[0];
                            xmlSR.Close();
                            xmlSR.Dispose();
                            logger.Log(LogLevel.Info, "Read " + SalesOrderItems.Rows.Count + " Sales Order Item Record ");
                            foreach (DataRow item in SalesOrderItems.Rows)
                            {
                                logger.Log(LogLevel.Info, "Read Order Number : " + item["ORDNUMBER"] + "  Line : " + item["ORDNUMBER"].ToString().Trim());
                                try
                                {
                                    bool isNew = false;
                                    SALEORDER_ITEM or;
                                    or = db.SALEORDER_ITEMs.FirstOrDefault(x => x.ORDNUMBER == item["ORDNUMBER"].ToString().Trim() && x.ITEM == item["LINENUM"].ToString().Trim());
                                    if (or == null)
                                    {
                                        or = new SALEORDER_ITEM();
                                        isNew = true;
                                        or.ORDNUMBER = item["ORDNUMBER"].ToString().Trim();
                                    }
                                    or.IsDone = "N";
                                    or.ORDNUMBER = item["ORDNUMBER"].ToString().Trim();
                                    or.ITEM = item["LINENUM"].ToString().Trim();
                                    or.PRODUCTCODE = item["PRODUCTCODE"].ToString().Trim();
                                    or.QUANTITY = item["QUANTITY"].ToString().Trim();
                                    or.PRODUCTDESC = item["PRODUCTDESC"].ToString().Trim();
                                    or.DISCOUNTAMOUNT = item["DISCOUNTAMOUNT"].ToString().Trim();
                                    or.UNITPRICEINCDIS = item["UNITPRICEINCDIS"].ToString().Trim();
                                    or.TOTALAMOUNT = item["TOTALAMOUNT"].ToString().Trim();
                                    or.STOCKAVA = item["STOCKAVA"].ToString().Trim();
                                    or.COMMITQTY = item["COMMITQTY"].ToString().Trim();
                                    or.BALQTY = item["BALQTY"].ToString().Trim();
                                    or.BOQTY = item["BOQTY"].ToString().Trim();
                                    or.POQTY = item["POQTY"].ToString().Trim();
                                    or.SHIPSTOKOUT = item["SHIPSTOCKOUT"].ToString().Trim();
                                    or.INVSTOKOUT = item["INVSTOCKOUT"].ToString().Trim();
                                    or.POQTY = item["POQTY"].ToString().Trim();
                                    if (item["SITE"].ToString().Trim() == "")
                                    {

                                        or.SITE = "HO";
                                    }
                                    else
                                    {
                                        or.SITE = item["SITE"].ToString().Trim();
                                    }

                                    var stockdate = item["STOCKOUTDATE"].ToString().Trim();
                                    if (stockdate == "0")
                                    {
                                        or.STOCKOUTDATE = DateTime.Now;
                                    }
                                    else
                                    {
                                        or.STOCKOUTDATE = DateTime.ParseExact(stockdate.Substring(0, 4) + "-" + stockdate.Substring(4, 2) + "-" + stockdate.Substring(6, 2), "yyyy-MM-dd", null);
                                    }

                                    string intstatus = item["STATUS"].ToString();
                                    switch (intstatus)
                                    {
                                        case "0":
                                            or.STATUS = "Not completed";
                                            break;
                                        case "1":
                                            or.STATUS = "Completed/Not In Database";
                                            break;
                                        case "2":
                                            or.STATUS = "Completed";
                                            break;
                                        case "3":
                                            or.STATUS = "Processed by day end";
                                            break;
                                        default:
                                            break;
                                    }


                                    if (isNew)
                                    {
                                        db.SALEORDER_ITEMs.InsertOnSubmit(or);

                                    }
                                }
                                catch (Exception ex)
                                {

                                    logger.Log(LogLevel.Info, "Error  " + ex.Message);
                                }

                            }


                            try
                            {

                                db.SubmitChanges();
                                logger.Log(LogLevel.Info, "Save All Sale Order Item To DataBase");
                            }
                            catch (Exception ex)
                            {
                                logger.Log(LogLevel.Error, "Error : " + ex.Message);
                            }
                        }

                        System.Threading.Thread.Sleep(1000);
                        int loop = 0;
                        while (loop < 3)
                        {
                            try
                            {
                                File.Delete(file);
                                loop = 3;
                            }
                            catch (Exception ex)
                            {
                                System.Threading.Thread.Sleep(1000);
                                loop++;
                            }
                        }
                    }
                }
            }
        }
        private void ActualTask()
        {

            var logger = LogManager.GetCurrentClassLogger();
            String filePath = ConfigurationManager.AppSettings["ftp"];
            if (Directory.GetFiles(filePath).Count() > 0)
            {
                using (PlanetCommDataContext db = new PlanetCommDataContext())
                {
                    var file = filePath + "\\ACTUAL.xml";

                    if (File.Exists(file))
                    {
                        logger.Log(LogLevel.Info, "ACTUAL.xml File Exist");
                        string xml = System.IO.File.ReadAllText(file);
                        var xmlSR = new System.IO.StringReader(XmlHelper.CleanInvalidXmlChars(xml));
                        using (DataSet dsStore = new DataSet())
                        {

                            dsStore.ReadXml(xmlSR);
                            var SalesOrderItems = dsStore.Tables[0];
                            xmlSR.Close();
                            xmlSR.Dispose();
                            logger.Log(LogLevel.Info, "Read " + SalesOrderItems.Rows.Count + " Actual Invoice Item Record ");
                            foreach (DataRow item in SalesOrderItems.Rows)
                            {
                                bool isNew = false;
                                ACTUAL ac;
                                ac = db.ACTUALs.FirstOrDefault(x => x.INVOICENUMBER == item["INVNUMBER"].ToString().Trim());
                                if (ac == null)
                                {
                                    ac = new ACTUAL();
                                    isNew = true;
                                    ac.ORDNUMBER = item["ORDNUMBER"].ToString().Trim();
                                    var podate = item["INVDATE"].ToString();
                                    ac.INVOICENUMBER = item["INVNUMBER"].ToString();
                                    ac.ACTUALINVOICEDATE = DateTime.ParseExact(podate.Substring(0, 4) + "-" + podate.Substring(4, 2) + "-" + podate.Substring(6, 2), "yyyy-MM-dd", null);
                                }
                                ac.IsDone = "N";
                                ac.ACTUALINVOICEAMOUNT = Decimal.Parse(item["AMOUNT"].ToString().Trim()).ToString("0.00");
                                if (isNew)
                                {
                                    db.ACTUALs.InsertOnSubmit(ac);
                                }
                            }

                            try
                            {
                                db.SubmitChanges();
                                logger.Log(LogLevel.Info, "Save Invoice Actual To DataBase");
                            }
                            catch (Exception ex)
                            {

                                logger.Log(LogLevel.Error, ex.Message);
                            }
                        }
                        System.Threading.Thread.Sleep(1000);
                        int loop = 0;
                        while (loop < 3)
                        {
                            try
                            {
                                File.Delete(file);
                                loop = 3;
                            }
                            catch (Exception ex)
                            {
                                System.Threading.Thread.Sleep(1000);
                                loop++;
                            }
                        }
                    }
                }
            }
        }
        private void ForecastTask()
        {
            var logger = LogManager.GetCurrentClassLogger();
            String filePath = ConfigurationManager.AppSettings["ftp"];
            if (Directory.GetFiles(filePath).Count() > 0)
            {

                logger.Log(LogLevel.Info, "Read Directory For Forcast");
                using (PlanetCommDataContext db = new PlanetCommDataContext())
                {
                    var file = filePath + "\\FORECAST.xml";
                    if (File.Exists(file))
                    {
                        logger.Log(LogLevel.Info, "FORECAST.xml File Exist");
                        string xml = System.IO.File.ReadAllText(file);
                        var xmlSR = new System.IO.StringReader(XmlHelper.CleanInvalidXmlChars(xml));
                        using (DataSet dsStore = new DataSet())
                        {

                            dsStore.ReadXml(xmlSR);
                            var Forcast = dsStore.Tables[0];
                            xmlSR.Close();
                            xmlSR.Dispose();
                            logger.Log(LogLevel.Info, "Read " + Forcast.Rows.Count + " Invoice Forecast Record ");
                            foreach (DataRow item in Forcast.Rows)
                            {
                                bool isNew = false;
                                FORECAST fo;

                                try
                                {
                                    PlanetCommDataContext dbf = new PlanetCommDataContext();
                                    var forecasts = dbf.FORECASTs.Where(x => x.ORDNUMBER == item["SO"].ToString().Trim()).ToList();
                                    if (forecasts != null)
                                    {
                                        foreach (var forecast in forecasts)
                                        {
                                            forecast.FORECASTINVOICEAMOUNT = "0";
                                        }
                                    }
                                    dbf.SubmitChanges();
                                }
                                catch (Exception ex)
                                {
                                    logger.Log(LogLevel.Info, "Error : " + ex.Message);
                                }



                                fo = db.FORECASTs.FirstOrDefault(x => x.ORDNUMBER == item["SO"].ToString().Trim()
                                    && x.FORECASTINVOICEYEARMONTH == item["FORECASTDATE"].ToString().Trim()
                                    );
                                if (fo == null)
                                {
                                    fo = new FORECAST();
                                    isNew = true;
                                    fo.INVOICENO = DateTime.Now.Ticks.ToString();
                                    fo.ORDNUMBER = item["SO"].ToString().Trim();
                                    var podate = item["FORECASTDATE"].ToString();
                                    fo.FORECASTINVOICEYEARMONTH = item["FORECASTDATE"].ToString().Trim();
                                    fo.FORECASTINVOICEDATE = DateTime.ParseExact(podate.Substring(0, 4) + "-" + podate.Substring(4, 2) + "-01", "yyyy-MM-dd", null);

                                }
                                // Update  Invoice forecast amout
                                fo.IsDone = "N";
                                fo.FORECASTINVOICEAMOUNT = Decimal.Parse(item["FORECASTAMOUNT"].ToString().Trim()).ToString("0.00");
                                if (isNew)
                                {
                                    db.FORECASTs.InsertOnSubmit(fo);
                                }
                            }
                            try
                            {
                                db.SubmitChanges();
                                logger.Log(LogLevel.Info, "Save Invoice Forecast To DataBase");
                            }
                            catch (Exception ex)
                            {

                                logger.Log(LogLevel.Error, ex.Message);
                            }

                        }
                        System.Threading.Thread.Sleep(1000);
                        int loop = 0;
                        while (loop < 3)
                        {
                            try
                            {
                                File.Delete(file);
                                loop = 3;
                            }
                            catch (Exception ex)
                            {
                                System.Threading.Thread.Sleep(1000);
                                loop++;
                            }
                        }
                    }
                }
            }
        }
        private void CreditNoteTask()
        {

            var logger = LogManager.GetCurrentClassLogger();
            String filePath = ConfigurationManager.AppSettings["ftp"];
            if (Directory.GetFiles(filePath).Count() > 0)
            {
                using (PlanetCommDataContext db = new PlanetCommDataContext())
                {
                    var file = filePath + "\\CREDITNOTE.xml";

                    if (File.Exists(file))
                    {
                        logger.Log(LogLevel.Info, "CREDITNOTE.xml File Exist");
                        string xml = System.IO.File.ReadAllText(file);
                        var xmlSR = new System.IO.StringReader(XmlHelper.CleanInvalidXmlChars(xml));
                        using (DataSet dsStore = new DataSet())
                        {
                            dsStore.ReadXml(xmlSR);
                            var CreditNotes = dsStore.Tables[0];
                            xmlSR.Close();
                            xmlSR.Dispose();
                            logger.Log(LogLevel.Info, "Read " + CreditNotes.Rows.Count + " Credit Note Item Record ");
                            foreach (DataRow item in CreditNotes.Rows)
                            {
                                bool isNew = false;
                                CREDITNOTE cn;
                                cn = db.CREDITNOTEs.FirstOrDefault(x => x.CREDITNOTENUMBER == item["CREDITNOTENUMBER"].ToString().Trim());

                                if (cn == null)
                                {
                                    cn = new CREDITNOTE();
                                    isNew = true;
                                    cn.ORDNUMBER = item["ORDNUMBER"].ToString().Trim();
                                    cn.CREDITNOTENUMBER = item["CREDITNOTENUMBER"].ToString().Trim();
                                    var date = item["CREDITNOTEDATE"].ToString();
                                    cn.CREDITNOTEDATE = DateTime.ParseExact(date.Substring(0, 4) + "-" + date.Substring(4, 2) + "-" + date.Substring(6, 2), "yyyy-MM-dd", null);

                                }
                                cn.IsDone = "N";
                                var time = Double.Parse(item["CREDITNOTELASTMODDIFIELDTIME"].ToString());
                                TimeSpan ts = TimeSpan.FromHours(time);
                                int H = ts.Hours;
                                int M = ts.Minutes;
                                var cndate = item["CREDITNOTEDATE"].ToString();
                                DateTime cdate = new DateTime(int.Parse(cndate.Substring(0, 4)),
                                    int.Parse(cndate.Substring(4, 2)),
                                    int.Parse(cndate.Substring(6, 2)),
                                    H, M, 0);
                                cn.CREDITNOTEAMOUNT = Decimal.Parse(item["CREDITNOTEAMOUNT"].ToString().Trim()).ToString("0.00");
                                cn.CREDITNOTELASTMODIFIEDDATE = cdate;
                                if (isNew)
                                {
                                    db.CREDITNOTEs.InsertOnSubmit(cn);
                                }
                            }
                            try
                            {
                                db.SubmitChanges();
                                logger.Log(LogLevel.Info, "Save Credit Note To DataBase");
                            }
                            catch (Exception ex)
                            {

                                logger.Log(LogLevel.Error, ex.Message);
                            }

                        }
                        System.Threading.Thread.Sleep(1000);
                        int loop = 0;
                        while (loop < 3)
                        {
                            try
                            {
                                File.Delete(file);
                                loop = 3;
                            }
                            catch (Exception ex)
                            {
                                System.Threading.Thread.Sleep(1000);
                                loop++;
                            }
                        }
                    }
                }
            }
        }
        private void SalesHeaderSalesItemDataLoad()
        {
            #region ------------------------------ SalesOrder Header --------------------------------------

            var logger = LogManager.GetCurrentClassLogger();
            // Run Apex Data Loader
            var batOrder = ConfigurationManager.AppSettings["salesheaddataload"];
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo(batOrder);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            // process.WaitForExit();

            // *** Read the streams ***
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();
            exitCode = process.ExitCode;
            logger.Log(LogLevel.Info, "-------- Sales Order Headder Process -------------");
            logger.Log(LogLevel.Info, " Process Output ");
            logger.Log(LogLevel.Info, output);
            logger.Log(LogLevel.Info, " Process Error ");
            logger.Log(LogLevel.Error, error);
            #endregion
            #region ------------------------------ SalesOrder Detial --------------------------------------


            var batOrderItem = ConfigurationManager.AppSettings["salesitemdataload"];
            processInfo = new ProcessStartInfo(batOrderItem);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            // process.WaitForExit();
            // *** Read the streams ***
            output = process.StandardOutput.ReadToEnd();
            error = process.StandardError.ReadToEnd();
            exitCode = process.ExitCode;
            logger.Log(LogLevel.Info, "-------- Sales Order Item Process -------------");
            logger.Log(LogLevel.Info, " Process Output ");
            logger.Log(LogLevel.Info, output);
            logger.Log(LogLevel.Info, " Process Error ");
            logger.Log(LogLevel.Error, error);
            #endregion
            // Update Sales Header and salse order field isDone to Y
            using (PlanetCommDataContext db = new PlanetCommDataContext())
            {
                db.ExecuteCommand(" UPDATE SALEORDER SET IsDone='Y' WHERE IsDone='N' ");
                db.ExecuteCommand(" UPDATE SALEORDER_ITEM SET IsDone='Y' WHERE IsDone='N' ");
                db.SubmitChanges();
            }
        }
        private void InvoiceCNToSalesforce()
        {
            {
                var logger = LogManager.GetCurrentClassLogger();
                try
                {



                    var bat = ConfigurationManager.AppSettings["actualdataload"];
                    int exitCode;
                    ProcessStartInfo processInfo;
                    Process process;

                    processInfo = new ProcessStartInfo(bat);
                    processInfo.CreateNoWindow = true;
                    processInfo.UseShellExecute = false;
                    // *** Redirect the output ***
                    processInfo.RedirectStandardError = true;
                    processInfo.RedirectStandardOutput = true;

                    process = Process.Start(processInfo);
                    // process.WaitForExit();

                    // *** Read the streams ***
                    string output = process.StandardOutput.ReadToEnd();
                    string error = process.StandardError.ReadToEnd();
                    exitCode = process.ExitCode;
                    logger.Log(LogLevel.Info, "-------- Actual Invoice Process -------------");
                    logger.Log(LogLevel.Info, " Process Output ");
                    logger.Log(LogLevel.Info, output);
                    logger.Log(LogLevel.Info, " Process Error ");
                    logger.Log(LogLevel.Error, error);


                    var bat2 = ConfigurationManager.AppSettings["forecastdataload"];
                    processInfo = new ProcessStartInfo(bat2);
                    processInfo.CreateNoWindow = true;
                    processInfo.UseShellExecute = false;
                    // *** Redirect the output ***
                    processInfo.RedirectStandardError = true;
                    processInfo.RedirectStandardOutput = true;

                    process = Process.Start(processInfo);
                    // process.WaitForExit();

                    // *** Read the streams ***
                    output = process.StandardOutput.ReadToEnd();
                    error = process.StandardError.ReadToEnd();
                    exitCode = process.ExitCode;
                    logger.Log(LogLevel.Info, "-------- Forecast Invoice Process -------------");
                    logger.Log(LogLevel.Info, " Process Output ");
                    logger.Log(LogLevel.Info, output);
                    logger.Log(LogLevel.Info, " Process Error ");
                    logger.Log(LogLevel.Error, error);


                    var bat3 = ConfigurationManager.AppSettings["cndataload"];
                    processInfo = new ProcessStartInfo(bat3);
                    processInfo.CreateNoWindow = true;
                    processInfo.UseShellExecute = false;
                    // *** Redirect the output ***
                    processInfo.RedirectStandardError = true;
                    processInfo.RedirectStandardOutput = true;

                    process = Process.Start(processInfo);
                    // process.WaitForExit();

                    // *** Read the streams ***
                    output = process.StandardOutput.ReadToEnd();
                    error = process.StandardError.ReadToEnd();
                    exitCode = process.ExitCode;
                    logger.Log(LogLevel.Info, "-------- Credit Note Process -------------");
                    logger.Log(LogLevel.Info, " Process Output ");
                    logger.Log(LogLevel.Info, output);
                    logger.Log(LogLevel.Info, " Process Error ");
                    logger.Log(LogLevel.Error, error);
                }
                catch (Exception ex)
                {

                    logger.Log(LogLevel.Error, ex.Message);

                    // Write the fault message to the console
                    logger.Log(LogLevel.Error, "An unexpected error has occurred: " + ex.InnerException);

                    // Write the stack trace to the console
                    logger.Log(LogLevel.Error, ex.StackTrace);
                }
            }
        }
        private void CalculateInvoice()
        {
            SforceService binding;
            var logger = LogManager.GetCurrentClassLogger();

            try
            {
                using (SalesforceServiceProvider sfservice = new SalesforceServiceProvider())
                {
                    using (PlanetCommDataContext db = new PlanetCommDataContext())
                    {
                        var actuals = from x in db.ACTUALs
                                      where x.IsDone == "N"
                                      select x;

                        var forecast = from x in db.FORECASTs
                                       where x.IsDone == "N"
                                       select x;


                        var cn = from x in db.CREDITNOTEs
                                 where x.IsDone == "N"
                                 select x;

                        logger.Log(LogLevel.Info, "=====   Read Data From Database   ====");
                        logger.Log(LogLevel.Info, "=====   " + actuals.Count() + "   ====");
                        logger.Log(LogLevel.Info, "=====   " + forecast.Count() + "   ====");
                        logger.Log(LogLevel.Info, "=====   " + cn.Count() + "   ====");

                        try
                        {
                            var rs = sfservice.Login();
                            var session = rs.SessionHeaderValue.sessionId;
                            PlanetComm.SalesTargetService.PlannetCommCalculateSalesTargetService service
                                = new PlanetComm.SalesTargetService.PlannetCommCalculateSalesTargetService();
                            service.SessionHeaderValue = new PlanetComm.SalesTargetService.SessionHeader();
                            service.SessionHeaderValue.sessionId = session;
                            //  Actual
                            foreach (var item in actuals)
                            {
                                try
                                {
                                    var result = service.CalculateByInvoiceId(item.INVOICENUMBER.Trim());
                                    logger.Log(LogLevel.Info, "Send Invoice : " + item.INVOICENUMBER.Trim() + " result : " + result);
                                    item.IsDone = "Y";
                                }
                                catch (SoapException e)
                                {

                                    logger.Log(LogLevel.Error, e.Code);

                                    // Write the fault message to the console
                                    logger.Log(LogLevel.Error, "An unexpected error has occurred: " + e.Message);

                                    // Write the stack trace to the console
                                    logger.Log(LogLevel.Error, e.StackTrace);

                                }
                                catch (Exception ex)
                                {
                                    // Write the fault message to the console
                                    logger.Log(LogLevel.Error, "An unexpected error has occurred: " + ex.Message);
                                }

                            }

                            // Forecast
                            foreach (var item in forecast)
                            {
                                try
                                {
                                    var result = service.CalculateByInvoiceId(item.INVOICENO.Trim());
                                    logger.Log(LogLevel.Info, "Send Invoice : " + item.INVOICENO.Trim() + " result : " + result);
                                    item.IsDone = "Y";
                                }
                                catch (SoapException e)
                                {

                                    logger.Log(LogLevel.Error, e.Code);

                                    // Write the fault message to the console
                                    logger.Log(LogLevel.Error, "An unexpected error has occurred: " + e.Message);

                                    // Write the stack trace to the console
                                    logger.Log(LogLevel.Error, e.StackTrace);

                                }
                                catch (Exception ex)
                                {
                                    // Write the fault message to the console
                                    logger.Log(LogLevel.Error, "An unexpected error has occurred: " + ex.Message);
                                }
                            }

                            // Credit Note
                            foreach (var item in cn)
                            {
                                try
                                {
                                    var result = service.CalculateByCNId(item.CREDITNOTENUMBER.Trim());
                                    logger.Log(LogLevel.Info, "Send Invoice : " + item.CREDITNOTENUMBER.Trim() + " result : " + result);
                                    item.IsDone = "Y";
                                }
                                catch (SoapException e)
                                {

                                    logger.Log(LogLevel.Error, e.Code);

                                    // Write the fault message to the console
                                    logger.Log(LogLevel.Error, "An unexpected error has occurred: " + e.Message);

                                    // Write the stack trace to the console
                                    logger.Log(LogLevel.Error, e.StackTrace);

                                }
                                catch (Exception ex)
                                {
                                    // Write the fault message to the console
                                    logger.Log(LogLevel.Error, "An unexpected error has occurred: " + ex.Message);
                                }
                            }

                        }
                        catch (SoapException e)
                        {

                            logger.Log(LogLevel.Error, e.Code);

                            // Write the fault message to the console
                            logger.Log(LogLevel.Error, " Main Loop : An unexpected error has occurred: " + e.Message);

                            // Write the stack trace to the console
                            logger.Log(LogLevel.Error, e.StackTrace);

                        }
                        catch (Exception ex)
                        {
                            // Write the fault message to the console
                            logger.Log(LogLevel.Error, " Main Loop : An unexpected error has occurred: " + ex.Message);
                        }


                        try
                        {
                            db.SubmitChanges();
                        }
                        catch (Exception ex)
                        {
                            logger.Log(LogLevel.Error, " SubmitChanges Error  : An unexpected error has occurred: " + ex.Message);

                        }
                    }
                }
            }
            catch (Exception e)
            {

                // Write the fault message to the console
                logger.Log(LogLevel.Error, "Last : An unexpected error has occurred: " + e.Message);

                // Write the stack trace to the console
                logger.Log(LogLevel.Error, e.StackTrace);
            }

        }
        #endregion
    }
}

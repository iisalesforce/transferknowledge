﻿using Quartz;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaskScheduler.Lib;

using System.Diagnostics;

namespace TaskScheduler.Job
{
    public class ProductMasterSync : IJob
    {
        //-----  FIX FORM  ------//
        private DataLoader form; 
        private string master = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DataLoader\\cliq_process\\Productmaster\\Productmaster.bat");
        public void Execute(IJobExecutionContext context)
        {
            JobKey key = context.JobDetail.Key;
            JobDataMap dataMap = context.JobDetail.JobDataMap;
            form = (DataLoader)dataMap.Get("Form");
            form.SafeInvoke(d => d.UpdateStatus("Chiameny Integration is executing. " + DateTime.Now.ToLocalTime()));


            try
            {
                // Main Programe
                RunTask();
                form.SafeInvoke(d => d.UpdateStatus("Chiameny Integration Was Done. " + DateTime.Now.ToLocalTime()));
                //Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data")

            }
            catch (Exception e)
            {
                form.SafeInvoke(d => d.UpdateStatus(e.Message));
                form.SafeInvoke(d => d.UpdateStatus(e.StackTrace));

                var innerException = e.InnerException;
                while (innerException != null)
                {
                    form.SafeInvoke(d => d.UpdateStatus(innerException.Message));
                    form.SafeInvoke(d => d.UpdateStatus(innerException.StackTrace));
                    innerException = innerException.InnerException;
                }
            }

        }

        private void RunTask()
        {
            ProcessCaller processCaller = new ProcessCaller(form);
            var pro = ConfigurationManager.AppSettings["productdataload"];
            processCaller.FileName = pro;
            processCaller.Arguments = "";
            processCaller.StdErrReceived += new DataReceivedHandler(writeStreamInfo);
            processCaller.StdOutReceived += new DataReceivedHandler(writeStreamInfo);
            processCaller.Completed += new EventHandler(processCompletedOrCanceled);
            processCaller.Cancelled += new EventHandler(processCompletedOrCanceled);
            form.SafeInvoke(d => d.UpdateStatus("Upsert Product master To Salesforce. Please Wait..."));
            try
            {
                processCaller.Start();
                form.SafeInvoke(d => d.UpdateStatus("Done pushing Product master Data"));

            }
            catch (Exception ex)
            {
                form.SafeInvoke(d => d.UpdateStatus("Error Messgae : " + ex.Message));
            }

        }
        private void writeStreamInfo(object sender, TaskScheduler.Lib.DataReceivedEventArgs e)
        {

            form.SafeInvoke(d => d.UpdateStatus(e.Text));

        }

        /// <summary>
        /// Handles the events of processCompleted & processCanceled
        /// </summary>
        private void processCompletedOrCanceled(object sender, EventArgs e)
        {

        }
    }
}

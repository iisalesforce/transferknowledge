﻿using NLog;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TaskScheduler.Job;
using TaskScheduler.Lib;
using System.Configuration;
namespace TaskScheduler
{
    public partial class DataLoader : Form
    {

        //  PRODUCT MASTER SYNC VAR
        private ISchedulerFactory schedFact;
        private IScheduler sched;
        private ITrigger trigger;
        private IJobDetail job;
        
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public DataLoader()
        {
            InitializeComponent();
        }

        private void DataLoader_Load(object sender, EventArgs e)
        {
            schedFact = new StdSchedulerFactory();
            // get a scheduler
            sched = schedFact.GetScheduler();
            sched.Start();
            btnStart.Enabled = true;
            btnStop.Enabled = false;
        }





        public void UpdateStatus(string status)
        {
             txtStatus.AppendText(status + Environment.NewLine);
             logger.Info(status);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            RunServiceProductMaster();
        }
        public void RunServiceProductMaster()
        {
            //  first Job
            JobDataMap jobmap = new JobDataMap();
            jobmap.Add("Form", this);
            job = JobBuilder.Create<ProductMasterSync>()
                                    .WithIdentity("myJob", "group1") // name "myJob", group "group1"
                                    .UsingJobData(jobmap)
                                    .Build();
            // Trigger the job to run now, and then every 30 seconds
            trigger = TriggerBuilder.Create()
               .WithIdentity("myTrigger", "group1")
               .WithCronSchedule(txtCron.Text.Trim())
               .Build();

            // Tell quartz to schedule the job using our trigger
            sched.ScheduleJob(job, trigger);
        }



        private void writeStreamInfo(object sender, DataReceivedEventArgs e)
        {

            this.SafeInvoke(d => d.UpdateStatus(e.Text));
           
        }

        /// <summary>
        /// Handles the events of processCompleted & processCanceled
        /// </summary>
        private void processCompletedOrCanceled(object sender, EventArgs e)
        {

        }

        private void btnSrtMaster_Click(object sender, EventArgs e)
        {
            
            this.SafeInvoke(d => d.UpdateStatus("-------- Product Master Process Start -------------"));
            var pro = ConfigurationManager.AppSettings["productdataload"];

            ProcessCaller processCaller = new ProcessCaller(this);
            processCaller.FileName = pro;
            processCaller.Arguments = "";
            processCaller.StdErrReceived += new DataReceivedHandler(writeStreamInfo);
            processCaller.StdOutReceived += new DataReceivedHandler(writeStreamInfo);
            processCaller.Completed += new EventHandler(processCompletedOrCanceled);
            processCaller.Cancelled += new EventHandler(processCompletedOrCanceled);

            this.SafeInvoke(d => d.UpdateStatus("Upsert Product master To Salesforce. Please Wait..."));
            try
            {
                processCaller.Start();
                this.SafeInvoke(d => d.UpdateStatus("Done pushing Product master Data"));

            }
            catch (Exception ex)
            {
                this.SafeInvoke(d => d.UpdateStatus("Error Messgae : " + ex.Message));
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            sched.DeleteJob(job.Key);
            sched.UnscheduleJob(trigger.Key);
            btnStart.Enabled = true;
            btnStop.Enabled = false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NLog;

namespace WebsiteToSf
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            var logger = LogManager.GetCurrentClassLogger();
             logger.Log(LogLevel.Info, "IVR  Test");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Text;

namespace WebsiteToSfIvr.Util
{
    public static class SfUtility
    {
        public static string CollapseSpaces(string value)
        {
            return Regex.Replace(value, @"\s+", " ");
        }
        public static String beautifyPhoneNumber(String number, String extension)
        {
            String beautifulNumber = "(" + number.Substring(0, 3) + ") " +
                                     number.Substring(3, 3) + " " +
                                     number.Substring(6, 4);
            if (!String.IsNullOrEmpty(extension))
            {
                beautifulNumber += " x" + extension;
            }
            return beautifulNumber;
        }
        public static string windows874_unicode(string src)
        {
            Encoding iso = Encoding.GetEncoding("windows-874");
            Encoding unicode = Encoding.UTF8;
            byte[] isoBytes = iso.GetBytes(src);
            return unicode.GetString(isoBytes);
        }
        public static string tis620_unicode(string src)
        {
            Encoding thai = Encoding.GetEncoding("TIS-620");
            byte[] isoBytes = thai.GetBytes(src);
            Encoding unicode = Encoding.UTF8;
            byte[] utf8 = Encoding.Convert(Encoding.GetEncoding(874), Encoding.UTF8, isoBytes);
            return unicode.GetString(utf8);

        }

    }
}
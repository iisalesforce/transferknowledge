﻿using System;
using System.Web.Services;
using NLog;
using System.Text;
using WebsiteToSfIvr.SForce;

namespace WebsiteToSfIvr.Services
{
    using WebsiteToSfIvr.SForce;
    using WebsiteToSfIvr.SForce.Avaya.Ivr;

    /// <summary>
    /// Summary description for avayaivr
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class avayaivr : System.Web.Services.WebService
    {

        [WebMethod]
        public string SaveIvrScore(string UCID, string ANI, int QuestionNo = 0, int Score = 0)
        {
            var logger = LogManager.GetCurrentClassLogger();
            try
            {

                StringBuilder logMsg = new StringBuilder();

                logMsg.Append("New IVR  Data");
                logMsg.Append(string.Format(" | UCID : {0}", UCID));
                logMsg.Append(string.Format(" | ANI : {0}", ANI));
                logMsg.Append(string.Format(" | Question No : {0}", QuestionNo));
                logMsg.Append(string.Format(" | Score : {0}", Score));

                logger.Log(LogLevel.Info, logMsg.ToString());

                if (!String.IsNullOrEmpty(UCID))
                {
                    /************************************************
                     *  Login Salesforce Production
                     * **********************************************/
                    LoginResult rs = new SalesforceServiceProvider().LoginProduction();

                    ToaSfAvayaService service = new ToaSfAvayaService();
                    service.SessionHeaderValue = new SForce.Avaya.Ivr.SessionHeader();
                    // Set Session Header for request data or services
                    service.SessionHeaderValue.sessionId = rs.sessionId;

                    var result = service.RecordIvrScore(UCID, ANI, QuestionNo.ToString(), Score.ToString());
                    logger.Log(LogLevel.Info,
                    "  IVR Process Status  =>    " + result.Value);

                    return "true";
                }
            }
            catch (Exception ex)
            {
                System.Threading.Thread.Sleep(1000);
                try
                {
                    //    fix try send again
                    logger.Log(LogLevel.Info, " Try to send data 1 ");
                    /************************************************
                       *  Login Salesforce Production
                       * **********************************************/
                    LoginResult rs = new SalesforceServiceProvider().LoginProduction();

                    ToaSfAvayaService service = new ToaSfAvayaService();
                    service.SessionHeaderValue = new SForce.Avaya.Ivr.SessionHeader();
                    // Set Session Header for request data or services
                    service.SessionHeaderValue.sessionId = rs.sessionId;

                    var result = service.RecordIvrScore(UCID, ANI, QuestionNo.ToString(), Score.ToString());
                    logger.Log(LogLevel.Info,
                    "  IVR Process Status  =>    " + result.Value);

                    return "true";
                }
                catch (Exception ex1)
                {
                    logger.Log(LogLevel.Info, "Fail to connect salesforce with error : " + ex1.Message);
                    return "false";
                }
            }
            return "false";
        }
    }
}

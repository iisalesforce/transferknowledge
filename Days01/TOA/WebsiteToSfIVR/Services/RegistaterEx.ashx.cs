﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteToSfIvr.SForce;
using NLog;
using WebsiteToSfIvr.Util;
using System.Text.RegularExpressions;
using WebsiteToSfIvr.SForce.Toa.RegisterExService;
using System.IO;
using System.Configuration;

namespace WebsiteToSfIvr.Services
{
    /// <summary>
    /// Summary description for RegistaterEx
    /// </summary>
    public class RegistaterEx : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (ConfigurationManager.AppSettings["deepLog"] == "1")
            {
                string filePath = context.Server.MapPath("~/log/RegisterExProcessLog.txt");

                if (!File.Exists(filePath))
                {
                    File.Create(filePath);
                }

                //    writer all parameter to file

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filePath))
                {
                    file.WriteLine("============================================================================");
                    file.WriteLine("==========          New Register  Ex Data  From TOA          ===============");
                    file.WriteLine("============================================================================");
                    foreach (string key in context.Request.Form.AllKeys)
                    {
                        file.WriteLine("key name : " + key + "||  \t value : " + SfUtility.windows874_unicode(context.Request.Form[key]));
                        if (key == "file1")
                        {
                            var fileurl = ToThai(context, "file1").Trim();
                            string[] files = fileurl.Substring(0, fileurl.Length - 1).Split(',');

                            file.WriteLine("file array size : " + files.Count());

                            int i = 1;
                            for (int j = 0; j < files.Length; j++)
                            {
                                if (files[j].Contains(".zip"))
                                {
                                    file.WriteLine("file zip : " + files[j]);

                                }
                                else
                                {
                                    if (i == 1)
                                    {
                                        file.WriteLine("file " + i + " : " + files[j]);
                                        i++;
                                    }
                                    else if (i == 2)
                                    {
                                        file.WriteLine("file " + i + " : " + files[j]);
                                        i++;
                                    }
                                    else if (i == 3)
                                    {
                                        file.WriteLine("file " + i + " : " + files[j]);
                                        i++;
                                    }
                                    else if (i == 4)
                                    {
                                        file.WriteLine("file " + i + " : " + files[j]);
                                        i++;
                                    }
                                    else if (i == 5)
                                    {
                                        file.WriteLine("file " + i + " : " + files[j]);
                                        i++;
                                    }
                                }
                            }

                        }

                    }
                    file.WriteLine("============================================================================");
                }
            }
            var logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "New Register Ex");

            var dto = new RegisterExDTO();
            try
            {
                if (context.Request.Form["jobcode"] != null)
                {
                    dto.jobcode = SfUtility.windows874_unicode(context.Request.Form["jobcode"]);
                }

                if (context.Request.Form["f_st2name"] != null)
                {
                    dto.f_st2name = SfUtility.windows874_unicode(context.Request.Form["f_st2name"]);
                    dto.f_st2name = SfUtility.CollapseSpaces(dto.f_st2name);

                    logger.Log(LogLevel.Info, "New Register Ex  Name  :  " + dto.f_st2name);

                }
                if (context.Request.Form["f_sex"] != null)
                {
                    dto.f_sex = SfUtility.windows874_unicode(context.Request.Form["f_sex"]);
                }

                if (context.Request.Form["f_birth"] != null)
                {
                    var tmpDate = context.Request.Form["f_birth"].Split('/').ToArray();
                    if (tmpDate.Length == 3)
                    {
                        dto.f_birth = tmpDate[2] + "-" + tmpDate[1] + "-" + tmpDate[0];
                    }
                    else
                    {
                        DateTime dt = DateTime.Now;
                        dto.f_birth = dt.Year + "-" + dt.Month + "-" + dt.Day;
                    }
                }
                if (context.Request.Form["f_phone"] != null)
                {
                    dto.f_phone = context.Request.Form["f_phone"];
                    var pattern = @"-";
                    Regex rgx = new Regex(pattern);
                    string result1 = rgx.Replace(dto.f_phone, "");
                    dto.f_phone2 = result1;

                    if (result1.Length == 10)
                    {
                        dto.f_phone = SfUtility.beautifyPhoneNumber(result1, "");
                    }


                    logger.Log(LogLevel.Info, "New Register Ex  Phone  :  " + dto.f_phone);

                }
                if (context.Request.Form["f_st2email"] != null)
                {
                    dto.f_st2email = context.Request.Form["f_st2email"];
                }
                if (context.Request.Form["f_province"] != null)
                {
                    dto.f_province = ToThai(context, "f_province");
                }
                if (context.Request.Form["f_st2status"] != null)
                {
                    dto.f_st2status = ToThai(context, "f_st2status");
                }
                if (context.Request.Form["f_st3know"] != null)
                {
                    dto.f_st3know = ToThai(context, "f_st3know");
                }
                if (context.Request.Form["f_sent_1"] != null)
                {
                    dto.f_sent_1 = ToThai(context, "f_sent_1");
                }
                if (context.Request.Form["f_sent_2"] != null)
                {
                    dto.f_sent_2 = ToThai(context, "f_sent_2");
                }
                if (context.Request.Form["f_st2type"] != null)
                {
                    dto.f_st2type = ToThai(context, "f_st2type");
                }
                if (context.Request.Form["f_st2pjtype"] != null)
                {
                    dto.f_st2pjtype = ToThai(context, "f_st2pjtype");
                }
                if (context.Request.Form["f_pd_ex"] != null)
                {
                    dto.f_pd_ex = ToThai(context, "f_pd_ex");
                }
                if (context.Request.Form["f_pd_in"] != null)
                {
                    dto.f_pd_in = ToThai(context, "f_pd_in");
                }
                if (context.Request.Form["f_pd_sp"] != null)
                {
                    dto.f_pd_sp = ToThai(context, "f_pd_sp");
                }
                if (context.Request.Form["f_req5"] != null)
                {

                    var f_req5 = ToThai(context, "f_req5");
                    if (!String.IsNullOrEmpty(f_req5))
                        dto.f_req5 = "1";
                    else
                        dto.f_req5 = "0";
                }
                else
                {
                    dto.f_req5 = "0";
                }

                logger.Log(LogLevel.Info, "dto.f_req5 : " + dto.f_req5);

                if (context.Request.Form["f_req4"] != null)
                {
                    dto.f_req4 = ToThai(context, "f_req4");
                }

                if (context.Request.Form["f_pdrequire"] != null)
                {
                    dto.f_pdrequire = ToThai(context, "f_pdrequire");
                }
                logger.Log(LogLevel.Info, "dto.f_req5 : " + dto.f_pdrequire);

                if (context.Request.Form["f_pdcolortone"] != null)
                {
                    dto.f_pdcolortone = ToThai(context, "f_pdcolortone");
                }
                logger.Log(LogLevel.Info, "dto.f_req5 : " + dto.f_pdcolortone);

                if (context.Request.Form["f_st3kb"] != null)
                {
                    dto.f_st3kb = ToThai(context, "f_st3kb");
                }
                logger.Log(LogLevel.Info, "dto.f_req5 : " + dto.f_st3kb);

                if (context.Request.Form["f_st3kblike"] != null)
                {
                    dto.f_st3kblike = ToThai(context, "f_st3kblike");
                }
                logger.Log(LogLevel.Info, "dto.f_req5 : " + dto.f_st3kblike);


                if (context.Request.Form["f_st3like"] != null)
                {
                    dto.f_st3like = ToThai(context, "f_st3like");
                }
                logger.Log(LogLevel.Info, "dto.f_req5 : " + dto.f_st3like);

                if (context.Request.Form["f_st3dislike"] != null)
                {
                    dto.f_st3dislike = ToThai(context, "f_st3dislike");
                }
                logger.Log(LogLevel.Info, "dto.f_req5 : " + dto.f_st3dislike);

                if (context.Request.Form["f_other"] != null)
                {
                    dto.f_other = ToThai(context, "f_other");
                }
                logger.Log(LogLevel.Info, "dto.f_req5 : " + dto.f_other);

                // TOA  ส่ง File  มาเป็น String ที่ Concat กัน
                if (context.Request.Form["file1"] != null)
                {
                    var fileurl = ToThai(context, "file1").Trim();
                    string[] files = fileurl.Substring(0, fileurl.Length - 1).Split(',');

                    logger.Log(LogLevel.Info, "files1 size  : " + files.Count());

                    int i = 1;
                    for (int j = 0; j < files.Length; j++)
                    {
                        if (files[j].Contains(".zip"))
                        {
                            dto.FILE_Z = files[j];
                            logger.Log(LogLevel.Info, "FILE_Z : " + dto.FILE_Z);
                        }
                        else
                        {
                            if (i == 1)
                            {
                                dto.FILE1 = files[j]; i++;
                                logger.Log(LogLevel.Info, "FILE1 : " + dto.FILE1);
                            }
                            else if (i == 2)
                            {
                                dto.FILE2 = files[j]; i++;
                                logger.Log(LogLevel.Info, "FILE2 : " + dto.FILE2);
                            }
                            else if (i == 3)
                            {
                                dto.FILE3 = files[j]; i++;
                                logger.Log(LogLevel.Info, "FILE3 : " + dto.FILE3);
                            }
                            else if (i == 4)
                            {
                                dto.FILE4 = files[j]; i++;
                                logger.Log(LogLevel.Info, "FILE4 : " + dto.FILE4);
                            }
                            else if (i == 5)
                            {
                                dto.FILE5 = files[j]; i++;
                                logger.Log(LogLevel.Info, "FILE5 : " + dto.FILE5);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Error, "Parameter Error >>  " + ex.Message);
                context.Response.ContentType = "text/plain";
                context.Response.Write(ex.Message);
                context.Response.End();
            }
            if (dto.IsValid())
            {
                try
                {





                    // Salesforce Login Section 
                    logger.Log(LogLevel.Info, "Login to Salesforce ");
                    LoginResult rs = new SalesforceServiceProvider().LoginProduction();
                    ToaRegisterExServiceService service = new ToaRegisterExServiceService();
                    service.SessionHeaderValue = new WebsiteToSfIvr.SForce.Toa.RegisterExService.SessionHeader();
                    // Set Session Header for request data or services
                    service.SessionHeaderValue.sessionId = rs.sessionId;
                    logger.Log(LogLevel.Info, "EX Send Data ");
                    logger.Log(LogLevel.Info, dto.f_st2email);
                    var result = service.ToaRegisterEx(dto);
                    logger.Log(LogLevel.Info,
                        "Data : jobcode => " + dto.jobcode + "  f_st2name => " + dto.f_st2name + " Date  => " + dto.f_birth + "  Process Result : " + result);


                }
                catch (Exception ex)
                {
                    logger.Log(LogLevel.Error, "Connect to salesforce error : " + ex.Message);
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(ex.Message);
                    context.Response.End();
                }
                context.Response.ContentType = "text/plain";
                context.Response.Write("true");
                context.Response.End();
            }
            else
            {
                logger.Log(LogLevel.Info, "ข้อมูลไม่ครบถ้วน");
                context.Response.ContentType = "text/plain";
                context.Response.Write("false");
                context.Response.End();
            }

        }
        public string ToThai(HttpContext context, string key)
        {
            // return context.Request.Form[key];
            return SfUtility.windows874_unicode(context.Request.Form[key]);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
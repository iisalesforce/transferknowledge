﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteToSfIvr.SForce;
using WebsiteToSfIvr.SForce.Toa.EventService;
using NLog;
using WebsiteToSfIvr.Util;
using System.Text.RegularExpressions;

namespace WebsiteToSfIvr.Services
{
    /// <summary>
    /// Summary description for ToaEvent
    /// </summary>
    public class ToaEvent : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "New Event Register");
            var dto = new EventDTO();

            if (context.Request.Form["Job_Code"] != null)
            {
                dto.Job_Code = ToThai(context, "Job_Code");
            }
            if (context.Request.Form["name_event"] != null)
            {
                dto.name_event = ToThai(context, "name_event");
            }
            if (context.Request.Form["phone_event"] != null)
            {
                dto.phone_event = context.Request.Form["phone_event"];
                var pattern = @"-";
                Regex rgx = new Regex(pattern);
                string result1 = rgx.Replace(dto.phone_event, "");
                if (result1.Length == 10)
                {
                    dto.phone_event = SfUtility.beautifyPhoneNumber(result1, "");
                }
            }
            if (context.Request.Form["age_event"] != null)
            {
                dto.age_event = ToThai(context, "age_event");
            }
            if (context.Request.Form["email_event"] != null)
            {
                dto.email_event = ToThai(context, "email_event");
            }

            if (context.Request.Form["building_type"] != null)
            {
                dto.building_type = ToThai(context, "building_type");
            }
            if (context.Request.Form["Other_type_building"] != null)
            {
                dto.Other_type_building = ToThai(context, "Other_type_building");
            }
            if (context.Request.Form["floor"] != null)
            {
                dto.floor = ToThai(context, "floor");
            }
            if (context.Request.Form["project_type"] != null)
            {
                dto.project_type = ToThai(context, "project_type");
            }
            if (context.Request.Form["construction_process"] != null)
            {
                dto.construction_process = ToThai(context, "construction_process");
            }
            if (context.Request.Form["ide_color"] != null)
            {
                dto.ide_color = ToThai(context, "idea_color");
            }
            if (context.Request.Form["magazine"] != null)
            {
                dto.magazine = ToThai(context, "magazine");
            }
            if (context.Request.Form["other_infomation_color"] != null)
            {
                dto.other_infomation_color = ToThai(context, "other_infomation_color");
            }
            if (context.Request.Form["other_brand_service"] != null)
            {
                dto.other_brand_service = ToThai(context, "other_brand_service");
            }
            if (context.Request.Form["service_brand"] != null)
            {
                dto.service_brand = ToThai(context, "service_brand");
            }
            if (context.Request.Form["other_brand"] != null)
            {
                dto.other_brand = ToThai(context, "other_brand");
            }
            if (context.Request.Form["information_color"] != null)
            {
                dto.information_color = ToThai(context, "information_color");
            }
            if (context.Request.Form["website_information_color"] != null)
            {
                dto.website_information_color = ToThai(context, "website_information_color");
            }

            if (context.Request.Form["magazine_information_color"] != null)
            {
                dto.magazine_information_color = ToThai(context, "magazine_information_color");
            }
            if (context.Request.Form["tv_information_color"] != null)
            {
                dto.tv_information_color = ToThai(context, "tv_information_color");
            }
            if (context.Request.Form["other_news"] != null)
            {
                dto.other_news = ToThai(context, "other_news");
            }
            if (context.Request.Form["designer_name"] != null)
            {
                dto.designer_name = ToThai(context, "designer_name");
            }
            if (context.Request.Form["requirement_customer"] != null)
            {
                dto.requirement_customer = ToThai(context, "requirement_customer");
            }
            if (context.Request.Form["product_choose"] != null)
            {
                dto.product_choose = ToThai(context, "product_choose");
            }

            if (context.Request.Form["shade_source"] != null)
            {
                dto.shade_source = ToThai(context, "shade_source");
            }

            if (context.Request.Form["date_design"] != null)
            {
                var tmpDate = context.Request.Form["date_design"].Split('/').ToArray();
                if (tmpDate.Length == 3)
                {
                    dto.date_design = tmpDate[2] + "-" + tmpDate[1] + "-" + tmpDate[0];
                }
                else
                {
                    DateTime dt = DateTime.Now;
                    dto.date_design = dt.Year + "-" + dt.Month + "-" + dt.Day;
                }
            }
            if (context.Request.Form["time_design_hour"] != null)
            {
                dto.time_design_hour = ToThai(context, "time_design_hour");
            }
            if (context.Request.Form["time_design_minute"] != null)
            {
                dto.time_design_minute = ToThai(context, "time_design_minute");
            }

            if (!String.IsNullOrEmpty(dto.date_design) && !String.IsNullOrEmpty(dto.name_event))
            {
                try
                {
                    // Salesforce Login Section 
                    LoginResult rs = new SalesforceServiceProvider().LoginProduction();
                    ToaEventServiceService service = new ToaEventServiceService();
                    service.SessionHeaderValue = new WebsiteToSfIvr.SForce.Toa.EventService.SessionHeader();
                    // Set Session Header for request data or services
                    service.SessionHeaderValue.sessionId = rs.sessionId;

                    var result = service.ToaEvent(dto);
                    logger.Log(LogLevel.Info,
                        "Data : jobcode => " + dto.Job_Code + "  name_event => " + dto.name_event + " phone_event  => " + dto.phone_event + "  Process Result : " + result);

                    context.Response.ContentType = "text/plain";
                    context.Response.Write("true");
                    context.Response.End();
                }
                catch (Exception ex)
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(ex.Message);
                    context.Response.End();
                }
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write("false");
            context.Response.End();
        }
        public string ToThai(HttpContext context, string key)
        {
            return SfUtility.windows874_unicode(context.Request.Form[key]);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using WebsiteToSfIvr.SForce;


namespace WebsiteToSfIvr.Services
{
    public class SalesforceServiceProvider
    {
        private string _username;
        public string Username
        {
            get
            {
                if (String.IsNullOrEmpty(_username))
                {
                    _username = ConfigurationManager.AppSettings["user"];
                }
                return _username;
            }
        }
        private string _prousername;
        public string ProductUsername
        {
            get
            {
                if (String.IsNullOrEmpty(_prousername))
                {
                    _prousername = ConfigurationManager.AppSettings["productuser"];
                }
                return _prousername;
            }
        }
        
        
        
        private string _pass;
        public string Password
        {
            get
            {
                if (String.IsNullOrEmpty(_pass))
                {
                    _pass = ConfigurationManager.AppSettings["pass"];
                }
                return _pass;
            }
        }
        private string _token;
        public string Token
        {
            get
            {
                if (String.IsNullOrEmpty(_token))
                {
                    _token = ConfigurationManager.AppSettings["token"];
                }
                return _token;
            }
        }

        private string _tokenPro;
        public string TokenPro
        {
            get
            {
                if (String.IsNullOrEmpty(_tokenPro))
                {
                    _tokenPro = ConfigurationManager.AppSettings["producttoken"];
                }
                return _tokenPro;
            }
        }

        public  LoginResult Login()
        {
            SforceService sf = new SforceService();
            LoginResult rs = sf.login(Username, Password+Token);
            return rs;
        }

        public LoginResult LoginProduction()
        {
            SforceService sf = new SforceService();
            LoginResult rs = sf.login(ProductUsername, Password + TokenPro);
            return rs;
        }

    }
}
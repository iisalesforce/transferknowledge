﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteToSfIvr.SForce;
using WebsiteToSfIvr.SForce.Toa.QueueService;
using System.Text.RegularExpressions;
using WebsiteToSfIvr.Util;
using NLog;

namespace WebsiteToSfIvr.Services
{
    /// <summary>
    /// Summary description for QueueService
    /// </summary>
    public class QueueService : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            var logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "New Queue");

            string Date = null;
            string Name = null;
            string Time = null;
            string Phone = null;
            string Email = null;
            string Detail = null;

            if (context.Request.Form["f_date"] != null)
            {

                var tmpDate = context.Request.Form["f_date"].Split('/').ToArray();
                if (tmpDate.Length == 3)
                {
                    Date = tmpDate[2] + "-" + tmpDate[1] + "-" + tmpDate[0];
                }
                else
                {

                    DateTime dt = DateTime.Now;
                    Date = dt.Year + "-" + dt.Month + "-" + dt.Day;
                }

            }
            if (context.Request.Form["f_time"] != null)
            {
                var _time = context.Request.Form["f_time"];
                if (_time == "9-12")
                {
                    Time = "9.00 - 12.00 น.";

                }
                else if (_time == "14-17")
                {
                    Time = "14.00 - 17.00 น.";
                }
            }

            if (context.Request.Form["f_name"] != null)
            {
                Name = SfUtility.tis620_unicode(context.Request.Form["f_name"]);
                Name = SfUtility.CollapseSpaces(Name);
            }

            if (context.Request.Form["f_tel"] != null)
            {
                Phone = context.Request.Form["f_tel"];
                var pattern = @"-";
                Regex rgx = new Regex(pattern);
                string result1 = rgx.Replace(Phone, "");

                if (result1.Length == 10)
                {
                    Phone = SfUtility.beautifyPhoneNumber(result1, "");
                }
            }

            if (context.Request.Form["f_email"] != null)
            {
                Email = context.Request.Form["f_email"];
            }
            if (context.Request.Form["f_des"] != null)
            {
                Detail = SfUtility.tis620_unicode(context.Request.Form["f_des"]);
            }

            if (!String.IsNullOrEmpty(Name)
                && !String.IsNullOrEmpty(Date)
                && !String.IsNullOrEmpty(Time)
                && !String.IsNullOrEmpty(Phone)
                && !String.IsNullOrEmpty(Email)
                && !String.IsNullOrEmpty(Detail)
                )
            {
                try
                {
                    // Salesforce Login Section 
                    LoginResult rs = new SalesforceServiceProvider().LoginProduction();
                    SForce.Toa.QueueService.ToaSfQueueServiceService service = new SForce.Toa.QueueService.ToaSfQueueServiceService();
                    service.SessionHeaderValue = new WebsiteToSfIvr.SForce.Toa.QueueService.SessionHeader();
                    // Set Session Header for request data or services
                    service.SessionHeaderValue.sessionId = rs.sessionId;

                    var result = service.ToaQueue(Name, Date, Time, Phone, Email, Detail);
                    logger.Log(LogLevel.Info,
                       "Data : Queue data  =>  Name : " + Name
                       + "  Date => " + Date
                       + " Time  => " + Time
                       + " Phone  => " + Phone
                       + " Process Result : " + result);


                }
                catch (Exception ex)
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(ex.Message);
                    context.Response.End();
                }
                context.Response.ContentType = "text/plain";
                context.Response.Write("true");
                context.Response.End();
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write("false");
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}
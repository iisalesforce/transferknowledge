﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteToSfIvr.SForce;
//using WebsiteToSfIvr.SForce.Toa.FaqService;
using WebsiteToSfIvr.SForce.Toa;
using NLog;
using WebsiteToSfIvr.Util;

namespace WebsiteToSfIvr.Services
{
    using WebsiteToSfIvr.SForce.Toa.FaqService;

    using WebsiteToSfIvr.SForce.Toa.FaqService;

    /// <summary>
    /// Summary description for FaqService
    /// </summary>
    public class FaqService : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            var logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "New Faq Data");

            string Name = null;
            string Email = null;
            string Faq = null;

          //  var res = context.Request.QueryString["f_cusemail"];

            if (context.Request.Form["f_cusname"] != null)
            {
                Name = SfUtility.tis620_unicode(context.Request.Form["f_cusname"]);
            }
            if (context.Request.Form["f_cusemail"] != null)
            {
                Email = context.Request.Form["f_cusemail"];
            }
            if (context.Request.Form["f_des"] != null)
            {
                Faq = SfUtility.tis620_unicode(context.Request.Form["f_des"]);
            }
            if (!String.IsNullOrEmpty(Name) && !String.IsNullOrEmpty(Email) && !String.IsNullOrEmpty(Faq))
            {
                try
                {

                    logger.Log(LogLevel.Info,
                        "Data : f_cusname => " + Name + "  f_cusemail => " + Email + " f_des => " + Faq);
                    // Salesforce Login Section 
                    LoginResult rs = new SalesforceServiceProvider().LoginProduction();
                    ToaSfFaqServiceService service = new ToaSfFaqServiceService();
                    service.SessionHeaderValue = new WebsiteToSfIvr.SForce.Toa.FaqService.SessionHeader();
                    // Set Session Header for request data or services
                    service.SessionHeaderValue.sessionId = rs.sessionId;
                    var result = service.ToaFaq(Name, Email, Faq);
                    logger.Log(LogLevel.Info,
                        "Data : f_cusname => " + Name + "  f_cusemail => " + Email + " f_des => " + Faq + "  Process Result : " + result);
                }
                catch (Exception ex)
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(ex.Message);
                    context.Response.End();
                    return;
                }
                context.Response.ContentType = "text/plain";
                context.Response.Write("true");
                context.Response.End();

            }
            else
            {

                logger.Log(LogLevel.Info, "ข้อมูลไม่ครบถ้วน");

                context.Response.ContentType = "text/plain";
                context.Response.Write("false");
                context.Response.End();
            } 

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteToSfIvr.SForce;
using NLog;
using WebsiteToSfIvr.Util;
using System.Text.RegularExpressions;
using WebsiteToSfIvr.SForce.Toa.RegisterInService;
using System.IO;
using System.Configuration;

namespace WebsiteToSfIvr.Services
{
    /// <summary>
    /// Summary description for RegisterIn
    /// </summary>
    public class RegisterIn : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            if (ConfigurationManager.AppSettings["deepLog"] == "1")
            {
                string filePath = context.Server.MapPath("~/log/RegisterInProcessLog.txt");

                if (!File.Exists(filePath))
                {
                    File.Create(filePath);
                }

                //    writer all parameter to file

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filePath))
                {
                    file.WriteLine("============================================================================");
                    file.WriteLine("==========          New Register  In Data  From TOA          ===============");
                    file.WriteLine("============================================================================");
                    foreach (string key in context.Request.Form.AllKeys)
                    {
                        file.WriteLine("key name : " + key + "||  \t value : " + SfUtility.windows874_unicode(context.Request.Form[key]));
                    }
                    file.WriteLine("============================================================================");
                }
            }


            var logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "New Register In");

            var dto = new RegisterInDTO();
            try
            {
                if (context.Request.Form["f_st2name"] != null)
                {
                    dto.f_st2name =SfUtility.windows874_unicode(context.Request.Form["f_st2name"]);
                    dto.f_st2name = SfUtility.CollapseSpaces(dto.f_st2name);

                }
                if (context.Request.Form["jobcode"] != null)
                {
                    dto.jobcode = context.Request.Form["jobcode"];
                }
                if (context.Request.Form["f_st2dep"] != null)
                {
                    dto.f_st2dep = SfUtility.windows874_unicode(context.Request.Form["f_st2name"]);
                }
                if (context.Request.Form["f_st2pjname"] != null)
                {
                    dto.f_st2pjname = ToThai(context, "f_st2pjname");
                }
                if (context.Request.Form["f_province"] != null)
                {
                    dto.f_province = ToThai(context, "f_province");
                }
                if (context.Request.Form["f_phone"] != null)
                {
                    dto.f_phone = context.Request.Form["f_phone"];
                    var pattern = @"-";
                    Regex rgx = new Regex(pattern);
                    string result1 = rgx.Replace(dto.f_phone, "");
                    dto.f_phone2 = result1;

                    if (result1.Length == 10)
                    {
                        dto.f_phone = SfUtility.beautifyPhoneNumber(result1, "");
                    }
                }
                if (context.Request.Form["f_st2email"] != null)
                {
                    dto.f_st2email = context.Request.Form["f_st2email"];
                }

                if (context.Request.Form["f_sent_1"] != null)
                {
                    dto.f_sent_1 = ToThai(context, "f_sent_1");
                }
                if (context.Request.Form["f_sent_2"] != null)
                {
                    dto.f_sent_2 = ToThai(context, "f_sent_2");
                }
                if (context.Request.Form["f_st2type"] != null)
                {
                    dto.f_st2type = ToThai(context, "f_st2type");
                }
                if (context.Request.Form["f_st2pjtype"] != null)
                {
                    dto.f_st2pjtype = ToThai(context, "f_st2pjtype");
                }
                if (context.Request.Form["f_pd_ex"] != null)
                {
                    dto.f_pd_ex = ToThai(context, "f_pd_ex");
                }
                if (context.Request.Form["f_pd_in"] != null)
                {
                    dto.f_pd_in = ToThai(context, "f_pd_in");
                }
                if (context.Request.Form["f_pd_sp"] != null)
                {
                    dto.f_pd_sp = ToThai(context, "f_pd_sp");
                }
                if (context.Request.Form["f_req5"] != null)
                {

                    var f_req5 = ToThai(context, "f_req5");
                    if (f_req5 == "f_req5")
                        dto.f_req5 = "1";
                    else
                        dto.f_req5 = "0";
                }
                else
                {
                    dto.f_req5 = "0";
                }

                if (context.Request.Form["f_req4"] != null)
                {
                    dto.f_req4 = ToThai(context, "f_req4");
                }

                if (context.Request.Form["f_pdrequire"] != null)
                {
                    dto.f_pdrequire = ToThai(context, "f_pdrequire");
                }
                if (context.Request.Form["f_pdcolortone"] != null)
                {
                    dto.f_pdcolortone = ToThai(context, "f_pdcolortone");
                }



                if (context.Request.Form["f_st3kb"] != null)
                {
                    dto.f_st3kb = ToThai(context, "f_st3kb");
                }
                if (context.Request.Form["f_st3kblike"] != null)
                {
                    dto.f_st3kblike = ToThai(context, "f_st3kblike");
                }
                if (context.Request.Form["f_st3like"] != null)
                {
                    dto.f_st3like = ToThai(context, "f_st3like");
                }
                if (context.Request.Form["f_st3dislike"] != null)
                {
                    dto.f_st3dislike = ToThai(context, "f_st3dislike");
                }


                if (context.Request.Form["f_other"] != null)
                {
                    dto.f_other = ToThai(context, "f_other");
                }

                //  TOA  ส่ง File  มาเป็น String ที่ Concat กัน
                if (context.Request.Form["file1"] != null)
                {
                    var fileurl = ToThai(context, "file1").Trim();
                    string[] files = fileurl.Split(',');

                    int i = 1;
                    for (int j = 0; j < files.Length; j++)
                    {
                        if (files[j].Contains(".zip"))
                        {
                            dto.FILE_Z = files[j];
                            
                        }
                        else
                        {
                            if (i == 1) { dto.FILE1 = files[j]; i++; }
                            else if (i == 2) { dto.FILE2 = files[j]; i++; }
                            else if (i == 3) { dto.FILE3 = files[j]; i++; }
                            else if (i == 4) { dto.FILE4 = files[j]; i++; }
                            else if (i == 5) { dto.FILE5 = files[j]; i++; }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                logger.Log(LogLevel.Error, "Parameter Error : " + ex.Message);
                context.Response.ContentType = "text/plain";
                context.Response.Write(ex.Message);
                context.Response.End();
            }
            if (dto.IsValid())
            {
                try
                {
                    // Salesforce Login Section 
                    LoginResult rs = new SalesforceServiceProvider().LoginProduction();
                    ToaRegisterInServiceService service = new ToaRegisterInServiceService();
                    service.SessionHeaderValue = new WebsiteToSfIvr.SForce.Toa.RegisterInService.SessionHeader();
                    // Set Session Header for request data or services
                    service.SessionHeaderValue.sessionId = rs.sessionId;

                    var result = service.ToaRegisterIn(dto);
                    logger.Log(LogLevel.Info,
                        "Data : jobcode => " + dto.jobcode + "  f_st2name => " + dto.f_st2name + " f_st2pjname  => " + dto.f_st2pjname + "  Process Result : " + result);


                }
                catch (Exception ex)
                {
                    logger.Log(LogLevel.Error, "Salesforce Process Error : " + ex.Message);
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(ex.Message);
                    context.Response.End();
                }
                context.Response.ContentType = "text/plain";
                context.Response.Write("true");
                context.Response.End();

            }
            else
            {
                logger.Log(LogLevel.Info, "ข้อมูลไม่ครบถ้วน");
                context.Response.ContentType = "text/plain";
                context.Response.Write("false");
                context.Response.End();
            }

        }
        public string ToThai(HttpContext context, string key)
        {
           // return context.Request.Form[key];
          return SfUtility.windows874_unicode(context.Request.Form[key]);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
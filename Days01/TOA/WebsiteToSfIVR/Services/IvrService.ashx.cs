﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteToSfIvr.SForce;
using WebsiteToSfIvr.SForce.ApexSOAP;

namespace WebsiteToSfIvr.Services
{
    /// <summary>
    /// Summary description for IvrService
    /// </summary>
    public class IvrService : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
           /*



            if (context.Request.QueryString["CallerId"] != null)
            {
                dto.CallerId = context.Request.QueryString["CallerId"];
            }
            if (context.Request.QueryString["AgentId"] != null)
            {
                dto.AgentId = context.Request.QueryString["AgentId"];
            }
            if (context.Request.QueryString["IvrExtension"] != null)
            {
                dto.IvrExtension = context.Request.QueryString["IvrExtension"];
            }
            if (context.Request.QueryString["Score"] != null)
            {
                dto.Score = context.Request.QueryString["Score"];
            }
            if (!String.IsNullOrEmpty(dto.CallerId)
                && !String.IsNullOrEmpty(dto.AgentId)
                && !String.IsNullOrEmpty(dto.IvrExtension)
                && !String.IsNullOrEmpty(dto.Score)
                )
            {
                try
                { 
                    // Salesforce Login Section 
                    LoginResult rs = new SalesforceServiceProvider().Login();
                    ToaSfApexServiceService service = new ToaSfApexServiceService();
                    service.SessionHeaderValue = new WebsiteToSfIvr.SForce.ApexSOAP.SessionHeader();
                    // Set Session Header for request data or services
                    service.SessionHeaderValue.sessionId = rs.sessionId;


                    var result = service.ToaIvr(dto);
                }
                catch (Exception ex)
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(ex.Message);
                    context.Response.End();
                }
                context.Response.ContentType = "text/plain";
                context.Response.Write("true");
                context.Response.End();

            }
            */
            context.Response.ContentType = "text/plain";
            context.Response.Write("false");
            context.Response.End();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
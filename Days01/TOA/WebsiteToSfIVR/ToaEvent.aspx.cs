﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteToSfIvr.SForce.Toa.EventService;
using NLog;
using WebsiteToSfIvr.SForce;
using WebsiteToSfIvr.Services;
using WebsiteToSfIvr.Util;
namespace WebsiteToSfIvr
{
    public partial class ToaEvent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnRegis_Click(object sender, EventArgs e)
        {
          
            var logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "New Event Register");
            // post data
            EventDTO dto = new EventDTO();
            try
            {
                var Name = name_event__c.Text;
                Name = SfUtility.CollapseSpaces(name_event__c.Text);
                dto.name_event = Name;
                if (Name.Split(' ').Length == 1)
                {
                    dto.name_event = Name + " " + Name;
                }


                dto.phone_event = phone_event__c.Text;
                dto.age_event = age_event__c.Text;
                dto.email_event = email_event__c.Text;
                dto.building_type = building_type__c.SelectedValue;


                dto.Other_type_building = Other_type_building__c.Text;
                dto.floor = floor__c.Text;
                dto.project_type = project_type__c.Text;
                dto.construction_process = construction_process__c.Text;
                dto.ide_color = ide_color__c.Text;
                dto.source_news = source_news__c.Text;
                dto.magazine = magazine__c.Text;
                dto.other_infomation_color = other_infomation_color__c.Text;


                dto.other_brand_service = other_brand_service__c.Text;
                dto.service_brand = service_brand__c.Text;
                dto.other_brand = other_brand__c.Text;
                dto.information_color = other_brand__c.Text;
                dto.website_information_color = website_information_color__c.Text;
                dto.magazine_information_color = magazine_information_color__c.Text;
                dto.tv_information_color = tv_information_color__c.Text;
                dto.other_news = other_news__c.Text;
                dto.designer_name = designer_name__c.Text;
                dto.requirement_customer = requirement_customer__c.Text;

                dto.product_choose = product_choose__c.Text;
                dto.shade_source = shade_source__c.SelectedValue;
                dto.date_design = date_design__c.Text;
                dto.time_design_hour = time_design_hour__c.SelectedValue;
                dto.time_design_minute = time_design_minute__c.SelectedValue;


                if (!String.IsNullOrEmpty(dto.name_event)
                    && !String.IsNullOrEmpty(dto.phone_event)
                    && !String.IsNullOrEmpty(dto.age_event)
                    && !String.IsNullOrEmpty(dto.email_event)
                    && !String.IsNullOrEmpty(dto.date_design)
                    && !String.IsNullOrEmpty(dto.time_design_hour)
                    && !String.IsNullOrEmpty(dto.time_design_minute)


                    )
                {
                    try
                    {
                        // Salesforce Login Section 
                        LoginResult rs = new SalesforceServiceProvider().LoginProduction();
                        ToaEventServiceService service = new ToaEventServiceService();
                        service.SessionHeaderValue = new WebsiteToSfIvr.SForce.Toa.EventService.SessionHeader();
                        // Set Session Header for request data or services
                        service.SessionHeaderValue.sessionId = rs.sessionId;

                        var re = service.ToaEvent(dto);
                        logger.Log(LogLevel.Info,
                            "Data : jobcode => " + dto.Job_Code + "  name_event => " + dto.name_event + " phone_event  => " + dto.phone_event + "  Process Result : " + re.Status.Value);

                        if (re.Status.Value)
                        {
                            string url = "<h3> Case Number Is : <a href='http://toagroup-survey.force.com/event'> " + re.CaseNumber + "</a> </h3>";
                            this.result.Text = url;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Log(LogLevel.Info, "Data : error => " + ex.Message);

                    }
                }
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Info, "Data : error => " + ex.Message);
            }

        }
    }
}
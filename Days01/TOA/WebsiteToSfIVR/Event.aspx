﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Event.aspx.cs" Inherits="WebsiteToSf.Event" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
    .style1
    {
        width: 395px;
        height: 53px;
    }
    .style2
    {
        width: 100%;
    }
    .style3
    {
        width: 96px;
    }
</style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <table class="style1">
            <tr>
                <td class="style3">
                    <span style="color: rgb(102, 102, 102); font-family: 'Microsoft Sans Serif'; font-size: 14px;
                        font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal;
                        line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px;
                        text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;
                        background-color: rgb(241, 243, 244); display: inline !important; float: none;">
                        Job Code </span><span class="text15r" style="font-family: 'Microsoft Sans Serif';
                            font-size: 15px; color: rgb(255, 0, 0); font-style: normal; font-variant: normal;
                            font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto;
                            text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal;
                            widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 243, 244);">
                            *</span>
                </td>
                <td class="style4">
                    <asp:TextBox ID="txtJobCode" runat="server" Width="270px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <span style="color: rgb(102, 102, 102); font-family: 'Microsoft Sans Serif'; font-size: 14px;
                        font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal;
                        line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px;
                        text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;
                        background-color: rgb(241, 243, 244); display: inline !important; float: none;">
                        ชื่อผู้ติดต่อ</span><span class="text15r" style="font-family: 'Microsoft Sans Serif';
                            font-size: 15px; color: rgb(255, 0, 0); font-style: normal; font-variant: normal;
                            font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto;
                            text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal;
                            widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 243, 244);">*</span>
                </td>
                <td class="style4">
                    <asp:TextBox ID="txtName" runat="server" Width="270px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <span style="color: rgb(102, 102, 102); font-family: 'Microsoft Sans Serif'; font-size: 14px;
                        font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal;
                        line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px;
                        text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;
                        background-color: rgb(241, 243, 244); display: inline !important; float: none;">
                        เบอร์โทรศัพท์</span><span class="text15r" style="font-family: 'Microsoft Sans Serif';
                            font-size: 15px; color: rgb(255, 0, 0); font-style: normal; font-variant: normal;
                            font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto;
                            text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal;
                            widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 243, 244);">*</span>
                </td>
                <td>
                    <asp:TextBox ID="txtTel" runat="server" Width="270px"></asp:TextBox>
                </td>
            </tr>
            <tr>
            <td class="style2">
                <span style="color: rgb(102, 102, 102); font-family: 'Microsoft Sans Serif'; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 243, 244); display: inline !important; float: none;">
                อายุ</span><span class="text15r" 
                    style="font-family: 'Microsoft Sans Serif'; font-size: 15px; color: rgb(255, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 243, 244);">*</span></td>
            <td>
                <asp:TextBox ID="txtAge" runat="server" Width="270px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <span style="color: rgb(102, 102, 102); font-family: 'Microsoft Sans Serif'; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 243, 244); display: inline !important; float: none;">
                อีเมล์</span><span class="text15r" 
                    style="font-family: 'Microsoft Sans Serif'; font-size: 15px; color: rgb(255, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 243, 244);">*</span></td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" Width="270px"></asp:TextBox>
            </td>
        </tr>
            <tr>
                <td class="style3">
                </td>
                <td class="style4">
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Event Register" />
                </td>
            </tr>
            <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Label ID="txtResult" runat="server"></asp:Label>
            </td>
        </tr>
        </table>
        <br />
    </div>
</asp:Content>
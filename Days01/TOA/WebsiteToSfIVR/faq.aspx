﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="faq.aspx.cs" Inherits="WebsiteToSf.faq" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
    .style1
    {
        width: 395px;
        height: 53px;
    }
    .style2
    {
        width: 100%;
    }
    .style3
    {
        width: 96px;
    }
</style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        <img alt="" class="style1" src="faqbg.png" /></h2>
<table class="style2">
    <tr>
        <td class="style3">
            <span style="color: rgb(0, 149, 199); font-family: 'ms Sans Serif'; font-size: 14px; font-style: normal; font-variant: normal; font-weight: bold; letter-spacing: normal; line-height: 16px; orphans: auto; text-align: -webkit-right; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
            ชื่อ :</span></td>
        <td>
            <asp:TextBox ID="txtName" runat="server" Width="260px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="style3">
            <span style="color: rgb(0, 149, 199); font-family: 'ms Sans Serif'; font-size: 14px; font-style: normal; font-variant: normal; font-weight: bold; letter-spacing: normal; line-height: 16px; orphans: auto; text-align: -webkit-right; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
            อีเมลล์ :</span></td>
        <td>
            <asp:TextBox ID="txtEmail" runat="server" Width="260px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="style3">
            <span style="color: rgb(0, 149, 199); font-family: 'ms Sans Serif'; font-size: 14px; font-style: normal; font-variant: normal; font-weight: bold; letter-spacing: normal; line-height: 16px; orphans: auto; text-align: -webkit-right; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
            คำถาม :</span></td>
        <td>
            <asp:TextBox ID="txtFaq" runat="server" Rows="10" TextMode="MultiLine" 
                Width="260px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="style3">
            &nbsp;</td>
        <td>
            <asp:Button ID="btnFaq" runat="server" onclick="btnFaq_Click" Text="ส่งคมถาม" 
                Width="96px" />
        </td>
    </tr>
    <tr>
        <td class="style3">
            &nbsp;</td>
        <td>
            <asp:Label ID="txtResult" runat="server"></asp:Label>
        </td>
    </tr>
</table>
<br />
</asp:Content>

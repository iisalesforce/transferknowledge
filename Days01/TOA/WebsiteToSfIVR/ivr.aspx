﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ivr.aspx.cs" Inherits="WebsiteToSf.ivr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 106px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">



    <table class="style1">
        <tr>
            <td class="style2">
                Caller Id</td>
            <td>
                <asp:TextBox ID="txtCallerId" runat="server" Width="238px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Agent Id</td>
            <td>
                <asp:TextBox ID="txtAgentId" runat="server" Width="238px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Score</td>
            <td>
                <asp:TextBox ID="txtScore" runat="server" Width="238px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                IVR Extension</td>
            <td>
                <asp:TextBox ID="txtIvrEx" runat="server" Width="238px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btnSend" runat="server" onclick="btnSend_Click" Text="Send" 
                    Width="86px" />
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td>
                <asp:Label ID="txtResult" runat="server"></asp:Label>
            </td>
        </tr>
    </table>



</asp:Content>

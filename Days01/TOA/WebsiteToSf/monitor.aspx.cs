﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Configuration;
using NLog;
using WebsiteToSf.MySql;

namespace WebsiteToSf
{
    public partial class monitor : System.Web.UI.Page
    {
        public DataTable CSVTable;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //  ddl.Items.Add("---  กรุณาเลือก File ---");
                DirectoryInfo dir = new DirectoryInfo(Server.MapPath("log//"));
                int count = int.Parse(ConfigurationManager.AppSettings["logcount"]);
                var lstFiles = dir.GetFiles().OrderByDescending(x => x.LastWriteTime).Take(count);

                foreach (FileInfo files in lstFiles)
                {
                    if (files.Name.Contains(".csv"))
                    {
                        ddl.Items.Add(files.Name);
                    }
                }
                ddl.SelectedIndex = 0;
                StreamReader streamReader = new StreamReader(Server.MapPath("log//" + ddl.SelectedValue));
                //create datatable with column names
                CreateCSVTable(streamReader.ReadLine());
                String SingleLine;
                while ((SingleLine = streamReader.ReadLine()) != null)
                {
                    try
                    {
                        AddRowCSVTable(SingleLine);
                    }
                    catch
                    {
                        continue;
                    }
                    //adding rows to datatable
                }

                // var view = CSVTable.AsDataView();
                // view.Sort = "Time";
                streamReader.Dispose();
                if (CSVTable.Rows.Count > 0)
                {
                    grdList.DataSource = CSVTable;
                    grdList.DataBind();
                }
                if (streamReader != null) streamReader.Close();
            }

            if (ConfigurationManager.AppSettings["deepLog"] == "1")
            {
                string filePathEx = Server.MapPath("~/log/RegisterExProcessLog.txt");

                if (!File.Exists(filePathEx))
                {
                    File.Create(filePathEx);
                }


                var exstr = File.ReadLines(filePathEx);

                foreach (string item in exstr)
                {
                    ltlEx.Text += "<p>" + item + "</p>";
                }

                string filePathIn = Server.MapPath("~/log/RegisterInProcessLog.txt");


                if (!File.Exists(filePathIn))
                {
                    File.Create(filePathIn);
                }
                var instr = File.ReadLines(filePathIn);

                foreach (string item in instr)
                {
                    ltlIn.Text += "<p>" + item + "</p>";
                }

            }




        }
        //to create the data table with columns
        public void CreateCSVTable(string TableColumnList)
        {
            if (TableColumnList != null)
            {
                CSVTable = new DataTable("CSVTable");
                DataColumn myDataColumn;
                string[] ColumnName = TableColumnList.Split('|');
                for (int i = 0; i < ColumnName.Length - 1; i++)
                {
                    myDataColumn = new DataColumn();
                    myDataColumn.DataType =
                       Type.GetType("System.String");
                    myDataColumn.ColumnName = ColumnName[i];
                    CSVTable.Columns.Add(myDataColumn);
                }
            }
        }
        public void AddRowCSVTable(string RowValueList)
        {
            try
            {
                string[] RowValue = RowValueList.Split('|');
                DataRow myDataRow;
                myDataRow = CSVTable.NewRow();
                for (int i = 0; i < RowValue.Length - 1; i++)
                {
                    myDataRow[i] = RowValue[i];
                }
                CSVTable.Rows.Add(myDataRow);
            }
            catch (Exception ex)
            {
                // throw ex;
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                File.Delete(Server.MapPath("log.csv"));

            }
            catch (Exception ex)
            {
            }

        }

        protected void ddl_SelectedIndexChanged(object sender, EventArgs e)
        {

            StreamReader streamReader = new StreamReader(Server.MapPath("log//" + ddl.SelectedValue));
            //create datatable with column names
            CreateCSVTable(streamReader.ReadLine());
            String SingleLine;
            while ((SingleLine = streamReader.ReadLine()) != null)
            {
                AddRowCSVTable(SingleLine);
                //adding rows to datatable
            }
            grdList.DataSource = CSVTable;
            grdList.DataBind();

            if (streamReader != null) streamReader.Close();


        }

        protected void btnClear_Click1(object sender, EventArgs e)
        {
            DirectoryInfo dir = new DirectoryInfo(Server.MapPath("log//"));
            StreamReader streamReader = new StreamReader(Server.MapPath("log//" + ddl.SelectedValue));
            String head;
            head = streamReader.ReadLine();
            streamReader.Dispose();
            if (head != null)
            {

                File.WriteAllText(Server.MapPath("log//" + ddl.SelectedValue), head + Environment.NewLine);
            }

            // DirectoryInfo dir = new DirectoryInfo(Server.MapPath("log//"));
            int count = int.Parse(ConfigurationManager.AppSettings["logcount"]);
            var lstFiles = dir.GetFiles().OrderByDescending(x => x.LastWriteTime).Take(count);
            ddl.Items.Clear();
            foreach (FileInfo files in lstFiles)
            {
                if (files.Name.Contains(".csv"))
                {
                    ddl.Items.Add(files.Name);
                }
            }
            ddl.SelectedIndex = 0;
            streamReader = new StreamReader(Server.MapPath("log//" + ddl.SelectedValue));
            //create datatable with column names
            CreateCSVTable(streamReader.ReadLine());
            String SingleLine;
            while ((SingleLine = streamReader.ReadLine()) != null)
            {
                try
                {
                    AddRowCSVTable(SingleLine);
                }
                catch
                {
                    continue;
                }
                //adding rows to datatable
            }

            // var view = CSVTable.AsDataView();
            // view.Sort = "Time";
            streamReader.Dispose();
            if (CSVTable.Rows.Count > 0)
            {
                grdList.DataSource = CSVTable;
                grdList.DataBind();
            }
            else
            {
                grdList.DataSource = null;
                grdList.DataBind();
            }
            if (streamReader != null) streamReader.Close();

        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            FileInfo file = new FileInfo(Server.MapPath("log//") + ddl.SelectedValue);
            if (file.Exists)
            {
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "text/plain";
                Response.TransmitFile(file.FullName);
                Response.End();
            }
        }
    }
}
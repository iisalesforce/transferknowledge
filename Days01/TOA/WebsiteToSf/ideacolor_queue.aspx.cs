﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteToSf.SForce;
using WebsiteToSf.SForce.ApexSOAP;
using System.Text;
using WebsiteToSf.Util;
using NLog;

namespace WebsiteToSf
{
    public partial class ideacolor_queue : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Charset = "utf-8";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "New Queue Data");

            String strdate;
            try
            {
                var tmpDate = txtDate.Text.Split('/').ToArray();
                var name = txtName.Text;

                var phone = SfUtility.CollapseSpaces(txtPhone.Text);
                if (phone.Length == 10)
                {
                    phone = SfUtility.beautifyPhoneNumber(phone,"");
                }

                strdate = tmpDate[2] + "-" + tmpDate[1] + "-" + tmpDate[0];
                SforceService sf = new SforceService();
                LoginResult rs = sf.login("ii_support@toagroup.com.developer", "superice10hI5ciUaf5epvPOtDmNYDxty4");
                ToaSfApexServiceService service = new ToaSfApexServiceService();
                service.SessionHeaderValue = new WebsiteToSf.SForce.ApexSOAP.SessionHeader();
                service.SessionHeaderValue.sessionId = rs.sessionId;


                var result = service.ToaQueue(name, strdate, ddlTime.SelectedValue, phone, txtEmail.Text, txtDetial.Text);
                txtResult.Text = " Return from salesforce is : " + result.ToString();

                logger.Log(LogLevel.Info,
                      "Data : f_cusname => " + name + "  f_cusemail => " + txtEmail.Text + "  f_tel => " + phone + " f_des => " + txtDetial.Text + "  Process Result : " + result);
                
            }
            catch (Exception ex)
            {
                txtResult.Text = " Return from salesforce is : " + ex.Message;
            }
        }

        protected void txtDate_TextChanged(object sender, EventArgs e)
        {

        }

    }
}
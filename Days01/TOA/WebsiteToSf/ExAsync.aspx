﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExAsync.aspx.cs" Inherits="WebsiteToSf.ExAsync" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 181px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="status" runat="server" Text=""></asp:Label>
        
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">f_st2name</td>
                <td>
                    <asp:TextBox ID="f_st2name" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">jobcode</td>
                <td>
                    <asp:TextBox ID="jobcode" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_sex</td>
                <td>
                    <asp:TextBox ID="f_sex" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_birth</td>
                <td>
                    <asp:TextBox ID="f_birth" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_phone</td>
                <td>
                    <asp:TextBox ID="f_phone" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_st2email</td>
                <td>
                    <asp:TextBox ID="f_st2email" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_province</td>
                <td>
                    <asp:TextBox ID="f_province" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_st2status</td>
                <td>
                    <asp:TextBox ID="f_st2status" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_st3know</td>
                <td>
                    <asp:TextBox ID="f_st3know" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_sent_1</td>
                <td>
                    <asp:TextBox ID="f_sent_1" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_sent_2</td>
                <td>
                    <asp:TextBox ID="f_sent_2" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_st2type</td>
                <td>
                    <asp:TextBox ID="f_st2type" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_st2pjtype</td>
                <td>
                    <asp:TextBox ID="f_st2pjtype" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_pd_ex</td>
                <td>
                    <asp:TextBox ID="f_pd_ex" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_pd_in</td>
                <td>
                    <asp:TextBox ID="f_pd_in" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_pd_sp</td>
                <td>
                    <asp:TextBox ID="f_pd_sp" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_req5</td>
                <td>
                    <asp:TextBox ID="f_req5" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_req4</td>
                <td>
                    <asp:TextBox ID="f_req4" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_pdrequire</td>
                <td>
                    <asp:TextBox ID="f_pdrequire" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_pdcolortone</td>
                <td>
                    <asp:TextBox ID="f_pdcolortone" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_st3kb</td>
                <td>
                    <asp:TextBox ID="f_st3kb" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_st3kblike</td>
                <td>
                    <asp:TextBox ID="f_st3kblike" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_st3like</td>
                <td>
                    <asp:TextBox ID="f_st3like" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">f_st3dislike</td>
                <td style="margin-left: 40px">
                    <asp:TextBox ID="f_st3dislike" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
              <tr>
                <td class="auto-style2">f_other</td>
                <td style="margin-left: 40px">
                    <asp:TextBox ID="f_other" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">file 1</td>
                <td>
                    <asp:TextBox ID="file1" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">file 2</td>
                <td>
                    <asp:TextBox ID="file2" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">file 3</td>
                <td>
                    <asp:TextBox ID="file3" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">file 4</td>
                <td>
                    <asp:TextBox ID="file4" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">file 5</td>
                <td>
                    <asp:TextBox ID="file5" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">file zip</td>
                <td>
                    <asp:TextBox ID="filezip" runat="server" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>
                    <asp:Button ID="btnSend" runat="server" OnClick="btnSend_Click" Text="ส่งข้อมูล" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

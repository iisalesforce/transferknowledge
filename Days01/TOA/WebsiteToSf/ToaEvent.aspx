﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ToaEvent.aspx.cs" Inherits="WebsiteToSf.ToaEvent" %>

<!DOCTYPE html>
<html>
<head>
    <title>Toa Event UAT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="container">
        <form id="form1" runat="server">
        <div class="row" style="text-align: center;">
            <img src="Images/head4.png" alt="TOA Register image header" />
        </div>
        <asp:Literal ID="result" runat="server"></asp:Literal>
        <div class="form-horizontal row">
            <div style="text-align: center;">
                <h3>
                    TOA Event Form</h3>
            </div>
            <div class="form-group">
                <label for="name_event__c" class="col-lg-4 control-label">
                    ชื่อ-สกุล</label>
                <div class="col-lg-5">
                    <asp:TextBox ID="name_event__c" class="form-control" placeholder="ชื่อ-สกุล" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="phone_event__c" class="col-lg-4 control-label">
                    เบอร์โทรศัพท์</label>
                <div class="col-lg-5">
                    <asp:TextBox ID="phone_event__c" class="form-control" placeholder="เบอร์โทรศัพท์"
                        runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="age_event__c" class="col-lg-4 control-label">
                    อายุ</label>
                <div class="col-lg-5">
                    <asp:TextBox ID="age_event__c" class="form-control" placeholder="อายุ" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="email_event__c" class="col-lg-4 control-label">
                    อีเมลล์</label>
                <div class="col-lg-5">
                    <asp:TextBox ID="email_event__c" class="form-control" placeholder="อีเมลล์" runat="server"></asp:TextBox>
                </div>
            </div>            
            <div class="form-group">
                <label for="email_event__c" class="col-lg-4 control-label">
                    ลักษณะอาคาร</label>
                <div class="col-lg-5">
                    <asp:DropDownList ID="building_type__c" runat="server" class="form-control">
                        <asp:ListItem Selected="True" Text="กรุณาเลือกลักษณะอาคาร" Value="กรุณาเลือกลักษณะอาคาร"></asp:ListItem>
                        <asp:ListItem Text="กรุณาเลือกลักษณะอาคาร A" Value="กรุณาเลือกลักษณะอาคาร A"></asp:ListItem>
                        <asp:ListItem Text="กรุณาเลือกลักษณะอาคาร B" Value="กรุณาเลือกลักษณะอาคาร B"></asp:ListItem>
                        <asp:ListItem Text="กรุณาเลือกลักษณะอาคาร C" Value="กรุณาเลือกลักษณะอาคาร C"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="Other_type_building__c" class="col-lg-4 control-label">
                    ลักษณะอาคารอื่นๆ</label>
                <div class="col-lg-5">
                    <asp:TextBox ID="Other_type_building__c" class="form-control" placeholder="ลักษณะอาคารอื่นๆ"
                        runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="floor__c" class="col-lg-4 control-label">
                    ชั้น</label>
                <div class="col-lg-5">
                    <asp:TextBox ID="floor__c" class="form-control" placeholder="ชั้น" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="project_type__c" class="col-lg-4 control-label">
                    ประเภทโครงการ</label>
                <div class="col-lg-5">
                    <asp:DropDownList ID="project_type__c" runat="server" class="form-control">
                        <asp:ListItem Selected="True" Text="กรุณาเลือกประเภทโครงการ" Value="กรุณาเลือกประเภทโครงการ"></asp:ListItem>
                        <asp:ListItem Text="ประเภทโครงการ A" Value="ประเภทโครงการ A"></asp:ListItem>
                        <asp:ListItem Text="ประเภทโครงการ B" Value="ประเภทโครงการ B"></asp:ListItem>
                        <asp:ListItem Text="ประเภทโครงการ C" Value="ประเภทโครงการ C"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="construction_process__c" class="col-lg-4 control-label">
                    ขั้นตอนก่อสร้าง</label>
                <div class="col-lg-5">
                    <asp:DropDownList ID="construction_process__c" runat="server" class="form-control">
                        <asp:ListItem Selected="True" Text="กรุณาเลือกขั้นตอนก่อสร้าง" Value="กรุณาเลือกขั้นตอนก่อสร้าง"></asp:ListItem>
                        <asp:ListItem Text="ขั้นตอนก่อสร้าง A" Value="ขั้นตอนก่อสร้าง A"></asp:ListItem>
                        <asp:ListItem Text="ขั้นตอนก่อสร้าง B" Value="ขั้นตอนก่อสร้าง B"></asp:ListItem>
                        <asp:ListItem Text="ขั้นตอนก่อสร้าง C" Value="ขั้นตอนก่อสร้าง C"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="ide_color__c" class="col-lg-4 control-label">
                    เคยรู้จักบริการออกแบบสีบ้าน ide@color</label>
                <div class="col-lg-5">
                    <asp:DropDownList ID="ide_color__c" runat="server" class="form-control">
                        <asp:ListItem Selected="True" Text="กรุณาเลือกคำตอบ" Value="กรุณาเลือกคำตอบ"></asp:ListItem>
                        <asp:ListItem Text="คำตอบ A" Value="คำตอบ A"></asp:ListItem>
                        <asp:ListItem Text="คำตอบ B" Value="คำตอบ B"></asp:ListItem>
                        <asp:ListItem Text="คำตอบ C" Value="คำตอบ C"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="source_news__c" class="col-lg-4 control-label">
                    ท่านได้ทราบข่าวจากแหล่งใดบ้าง</label>
                <div class="col-lg-5">
                    <asp:ListBox ID="source_news__c" runat="server" SelectionMode="Multiple">
                        <asp:ListItem Selected="True" Text="กรุณาเลือกแหล่งข่าว" Value="กรุณาเลือกแหล่งข่าว"></asp:ListItem>
                        <asp:ListItem Text="ท่านได้ทราบข่าวจากแหล่ง A" Value="ท่านได้ทราบข่าวจากแหล่ง A"></asp:ListItem>
                        <asp:ListItem Text="ท่านได้ทราบข่าวจากแหล่ง B" Value="ท่านได้ทราบข่าวจากแหล่ง B"></asp:ListItem>
                        <asp:ListItem Text="ท่านได้ทราบข่าวจากแหล่ง C" Value="ท่านได้ทราบข่าวจากแหล่ง C"></asp:ListItem>
                    </asp:ListBox>
                </div>
            </div>
            <div class="form-group">
                <label for="magazine__c" class="col-lg-4 control-label">
                    นิตยสาร/รายการทีวี( ระบุ)</label>
                <div class="col-lg-5">
                    <asp:TextBox ID="magazine__c" class="form-control" placeholder="นิตยสาร/รายการทีวี( ระบุ)"
                        runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="other_infomation_color__c" class="col-lg-4 control-label">
                    ข้อมูลสีจากแหล่งอื่นๆ</label>
                <div class="col-lg-5">
                    <asp:TextBox ID="other_infomation_color__c" class="form-control" placeholder="ข้อมูลสีจากแหล่งอื่นๆ"
                        runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="other_brand_service__c" class="col-lg-4 control-label">
                    เคยใช้บริการออกแบบสีบ้านจากสียี่ห้ออื่นๆ</label>
                <div class="col-lg-5">
                    <asp:DropDownList ID="other_brand_service__c" runat="server" class="form-control">
                        <asp:ListItem Selected="True" Text="กรุณาเลือกลัสีบ้านจากสียี่ห้ออื่น" Value="กรุณาเลือกสีบ้านจากสียี่ห้ออื่น"></asp:ListItem>
                        <asp:ListItem Text="สีบ้านจากสียี่ห้ออื่น A" Value="สีบ้านจากสียี่ห้ออื่น A"></asp:ListItem>
                        <asp:ListItem Text="สีบ้านจากสียี่ห้ออื่น B" Value="สีบ้านจากสียี่ห้ออื่น B"></asp:ListItem>
                        <asp:ListItem Text="สีบ้านจากสียี่ห้ออื่น C" Value="สีบ้านจากสียี่ห้ออื่น C"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="service_brand__c" class="col-lg-4 control-label">
                    เคยใช้บริการออกแบบสียี่ห้อ</label>
                <div class="col-lg-5">
                    <asp:ListBox ID="service_brand__c" runat="server" SelectionMode="Multiple">
                        <asp:ListItem Selected="True" Text="กรุณาเลือกบริการออกแบบสียี่ห้อ" Value="กรุณาเลือกบริการออกแบบสียี่ห้อ"></asp:ListItem>
                        <asp:ListItem Text="บริการออกแบบสียี่ห้อ A" Value="บริการออกแบบสียี่ห้อ A"></asp:ListItem>
                        <asp:ListItem Text="บริการออกแบบสียี่ห้อ B" Value="บริการออกแบบสียี่ห้อ B"></asp:ListItem>
                        <asp:ListItem Text="บริการออกแบบสียี่ห้อ C" Value="บริการออกแบบสียี่ห้อ C"></asp:ListItem>
                    </asp:ListBox>
                </div>
            </div>
            <div class="form-group">
                <label for="other_brand__c" class="col-lg-4 control-label">
                    สียี่ห้ออื่นๆ
                </label>
                <div class="col-lg-5">
                    <asp:TextBox ID="other_brand__c" class="form-control" placeholder="สียี่ห้ออื่นๆ"
                        runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="information_color__c" class="col-lg-4 control-label">
                    ท่านหาข้อมูลตกแต่งสีบ้านจากแหล่งใด</label>
                <div class="col-lg-5">
                    <asp:ListBox ID="information_color__c" runat="server" SelectionMode="Multiple">
                        <asp:ListItem Selected="True" Text="กรุณาเลือกหาข้อมูลตกแต่งสีบ้านจากแหล่งใด" Value="กรุณาเลือกหาข้อมูลตกแต่งสีบ้านจากแหล่งใด"></asp:ListItem>
                        <asp:ListItem Text="หาข้อมูลตกแต่งสีบ้านจากแหล่งใด A" Value="หาข้อมูลตกแต่งสีบ้านจากแหล่งใด A"></asp:ListItem>
                        <asp:ListItem Text="หาข้อมูลตกแต่งสีบ้านจากแหล่งใด B" Value="หาข้อมูลตกแต่งสีบ้านจากแหล่งใด B"></asp:ListItem>
                        <asp:ListItem Text="หาข้อมูลตกแต่งสีบ้านจากแหล่งใด C" Value="หาข้อมูลตกแต่งสีบ้านจากแหล่งใด C"></asp:ListItem>
                    </asp:ListBox>
                </div>
            </div>
            <div class="form-group">
                <label for="website_information_color__c" class="col-lg-4 control-label">
                    ข้อมูลสีจากเว็บไซต์
                </label>
                <div class="col-lg-5">
                    <asp:TextBox ID="website_information_color__c" class="form-control" placeholder="ข้อมูลสีจากเว็บไซต์"
                        runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="magazine_information_color__c" class="col-lg-4 control-label">
                    ข้อมูลสีจากนิตยสาร
                </label>
                <div class="col-lg-5">
                    <asp:TextBox ID="magazine_information_color__c" class="form-control" placeholder="ข้อมูลสีจากนิตยสาร"
                        runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="tv_information_color__c" class="col-lg-4 control-label">
                    ข้อมูลสีจากรายการทีวี
                </label>
                <div class="col-lg-5">
                    <asp:TextBox ID="tv_information_color__c" class="form-control" placeholder="ข้อมูลสีจากรายการทีวี"
                        runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="other_news__c" class="col-lg-4 control-label">
                    แหล่งข่าวอื่นๆ
                </label>
                <div class="col-lg-5">
                    <asp:TextBox ID="other_news__c" class="form-control" placeholder="แหล่งข่าวอื่นๆ"
                        runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="designer_name__c" class="col-lg-4 control-label">
                    ชื่อพนักงานผู้ออกแบบ
                </label>
                <div class="col-lg-5">
                    <asp:TextBox ID="designer_name__c" class="form-control" placeholder="ชื่อพนักงานผู้ออกแบบ"
                        runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="requirement_customer__c" class="col-lg-4 control-label">
                    บรรยากาศ/เฉดสี ที่ลูกค้าให้ออกแบบ
                </label>
                <div class="col-lg-5">
                    <asp:TextBox ID="requirement_customer__c" TextMode="MultiLine" Rows="5" class="form-control"
                        placeholder="บรรยากาศ/เฉดสี ที่ลูกค้าให้ออกแบบ" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="product_choose__c" class="col-lg-4 control-label">
                    ผลิตภัณฑ์ที่ลูกค้าเลือก
                </label>
                <div class="col-lg-5">
                    <asp:TextBox ID="product_choose__c" class="form-control" placeholder="ผลิตภัณฑ์ที่ลูกค้าเลือก"
                        runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="shade_source__c" class="col-lg-4 control-label">
                    เฉดสีจาก</label>
                <div class="col-lg-5">
                    <asp:DropDownList ID="shade_source__c" runat="server" class="form-control">
                        <asp:ListItem Selected="True" Text="กรุณาเฉดสีจาก" Value="กรุณาเลือกเฉดสีจาก"></asp:ListItem>
                        <asp:ListItem Text="เฉดสีจาก A" Value="เฉดสีจาก A"></asp:ListItem>
                        <asp:ListItem Text="เฉดสีจาก B" Value="เฉดสีจาก B"></asp:ListItem>
                        <asp:ListItem Text="เฉดสีจาก C" Value="เฉดสีจาก C"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="date_design__c" class="col-lg-4 control-label">
                    วันที่ออกแบบ
                </label>
                <div class="col-lg-5">
                    <asp:TextBox id="date_design__c" runat="server" Text="2013-09-26" class="form-control" placeholder="วันที่ออกแบบ"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="time_design_hour__c" class="col-lg-4 control-label">
                    ระยะเวลาออกแบบ (ชั่วโมง)
                </label>
                <div class="col-lg-5">
                    <asp:DropDownList ID="time_design_hour__c" runat="server" class="form-control">
                        <asp:ListItem Selected="True" Text="กรุณาระยะเวลาออกแบบ (ชั่วโมง)" Value="กรุณาเลือกระยะเวลาออกแบบ (ชั่วโมง)"></asp:ListItem>
                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="time_design_hour__c" class="col-lg-4 control-label">
                    ระยะเวลาออกแบบ (นาที)
                </label>
                <div class="col-lg-5">
                    <asp:DropDownList ID="time_design_minute__c" runat="server" class="form-control">
                        <asp:ListItem Selected="True" Text=" ระยะเวลาออกแบบ (นาที)" Value=" ระยะเวลาออกแบบ (นาที)"></asp:ListItem>
                        <asp:ListItem Text="5" Value="5"> </asp:ListItem>
                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                        <asp:ListItem Text="35" Value="35"></asp:ListItem>
                        <asp:ListItem Text="40" Value="40"></asp:ListItem>
                        <asp:ListItem Text="45" Value="45"></asp:ListItem>
                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                        <asp:ListItem Text="55" Value="55"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="time_design_hour__c" class="col-lg-4 control-label">
                </label>
                <div class="col-lg-5">
                    <asp:Button ID="BtnRegis" CssClass="btn btn-primary" runat="server" Text="Register"
                        OnClick="BtnRegis_Click" />
                </div>
            </div>
        </div>
        </form>
    </div>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
   <%-- <script src="dist/js/bootstrap.js" type="text/javascript"></script>--%>
</body>
</html>

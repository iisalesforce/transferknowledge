﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteToSf.SForce;
using WebsiteToSf.SForce.ApexSOAP;

namespace WebsiteToSf
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (SalesforceServiceProvider sf = new SalesforceServiceProvider())
            {
              
                LoginResult rs = sf.LoginProduction();
             
            }
        }



        protected void Button1_Click1(object sender, EventArgs e)
        {
            SforceService sf = new SforceService();
            LoginResult rs = sf.login("ii_support@toagroup.com.developer", "superice10hI5ciUaf5epvPOtDmNYDxty4");

            ToaSfApexServiceService service = new ToaSfApexServiceService();

            service.SessionHeaderValue = new WebsiteToSf.SForce.ApexSOAP.SessionHeader();
            service.SessionHeaderValue.sessionId = rs.sessionId;

            var dto = new RegisterExDTO();





            dto.jobcode = "EX000001";
            dto.f_st2name = txtName.Text;
            dto.f_sex = "ชาย";

            var tmpDate = "28/08/2013".Split('/').ToArray();
            var strdate = tmpDate[2] + "-" + tmpDate[1] + "-" + tmpDate[0];

            dto.f_birth = strdate;
            dto.f_phone = txtTel.Text;
            dto.f_st2email = txtEmail.Text;
            dto.f_province = "สมุทรปราการ";
            dto.f_st2status = "เจ้าของโครงการ";
            dto.f_st3know = "กรุณาเลือกแหล่งข่าวบริการออกแบบ ";
            dto.f_sent_1 = "pommatt@gmail.com";
            dto.f_sent_2 = "มารับงานออกแบบ";
            dto.f_st2type = "ภายนอก";
            dto.f_st2pjtype = "อาคารพาณิชย์/สำนักงาน";
            dto.f_pd_ex = "SuperShield (ทนทานนาน 15 ปี)";
            dto.f_pd_in = "ExtraShield (ทนชื้น)";
            dto.f_pd_sp = "SuperShield Aqua Gloss (สีเคลือบเงาสูตรน้ำไร้กลิ่นฉุน สำหรับงานเหล็ก)";
            //ต้องการให้ทีโอเอแนะนำ
            dto.f_req5 = "1";
            dto.f_req4 = "อื่นๆ - pommatt";
            dto.f_pdrequire = "สีจากแคตตาล็อกที่ท่านเลือก,สีผสมจากเครื่องผสมสี ทีโอเอ คัลเลอร์เวิล์ด,ต้องการให้ทีโอเอแนะนำ";
            dto.f_pdcolortone = "";
            dto.f_st3kb = "";
            dto.f_st3kblike = "";
            dto.f_st3like = "";
            dto.f_st3dislike = "";
            dto.f_other = "อยากได้สีสวยๆครับ";
            dto.FILE_Z = "http://www.google.com/file1";
            dto.FILE1 = "http://www.google.com/file2";
            dto.FILE2 = "http://www.google.com/file3";
            dto.FILE3 = "http://www.google.com/file4";
            dto.FILE4 = "http://www.google.com/file5";
            dto.FILE5 = "http://www.google.com/file6";
            Boolean? result = false;
            try
            {
                result = service.ToaRegisterEx(dto);
            }
            catch (Exception ex)
            {
                txtResult.Text = ex.Message;
                return;
            }
            txtResult.Text = " Return from salesforce is : " + result;

        }

        protected void ButtonIn_Click(object sender, EventArgs e)
        {
            SforceService sf = new SforceService();
            LoginResult rs = sf.login("ii_support@toagroup.com.developer", "superice10hI5ciUaf5epvPOtDmNYDxty4");

            ToaSfApexServiceService service = new ToaSfApexServiceService();

            service.SessionHeaderValue = new WebsiteToSf.SForce.ApexSOAP.SessionHeader();
            service.SessionHeaderValue.sessionId = rs.sessionId;

            var dto = new RegisterInDTO();





            dto.jobcode = txtJobCode.Text;
            dto.f_st2name = txtName.Text;
            dto.f_st2dep = "Idea Color Tecnical";
            dto.f_province = "กรุงเทพ";
            dto.f_st2pjname = "โครงการ ทำสวนผักปลอดสารพิษ";
            dto.f_phone = txtTel.Text;
            dto.f_st2email = txtEmail.Text;
            
         
            dto.f_sent_1 = "pommatt@gmail.com";
            dto.f_sent_2 = "มารับงานออกแบบ";
            dto.f_st2type = "ภายนอก";
            dto.f_st2pjtype = "อาคารพาณิชย์/สำนักงาน";
            dto.f_pd_ex = "SuperShield (ทนทานนาน 15 ปี)";
            dto.f_pd_in = "ExtraShield (ทนชื้น)";
            dto.f_pd_sp = "SuperShield Aqua Gloss (สีเคลือบเงาสูตรน้ำไร้กลิ่นฉุน สำหรับงานเหล็ก)";
            dto.f_req5 = "1";// "ต้องการให้ทีโอเอแนะนำ";
            dto.f_req4 = "อื่นๆ - pommatt";
            dto.f_pdrequire = "สีจากแคตตาล็อกที่ท่านเลือก,สีผสมจากเครื่องผสมสี ทีโอเอ คัลเลอร์เวิล์ด,ต้องการให้ทีโอเอแนะนำ";
            dto.f_pdcolortone = "";
            dto.f_st3kb = "";
            dto.f_st3kblike = "";
            dto.f_st3like = "";
            dto.f_st3dislike = "";
            dto.f_other = "อยากได้สีสวยๆครับ";
            dto.FILE_Z = "http://www.google.com/file1";
            dto.FILE1 = "http://www.google.com/file2";
            dto.FILE2 = "http://www.google.com/file3";
            dto.FILE3 = "http://www.google.com/file4";
            dto.FILE4 = "http://www.google.com/file5";
            dto.FILE5 = "http://www.google.com/file6";
            Boolean? result = false;
            try
            {
                result = service.ToaRegisterIn(dto);
            }
            catch (Exception ex)
            {
                txtResult.Text = ex.Message;
                return;
            }
            txtResult.Text = " Return from salesforce is : " + result;
        }


    }
}
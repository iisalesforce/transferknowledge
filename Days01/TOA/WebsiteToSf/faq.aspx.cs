﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteToSf.SForce;
using WebsiteToSf.SForce.ApexSOAP;
using NLog;


namespace WebsiteToSf
{
    public partial class faq : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "New Faq Data");
        }

        protected void btnFaq_Click(object sender, EventArgs e)
        {
            
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            SforceService sf = new SforceService();
            LoginResult rs = sf.login("ii_support@toagroup.com.developer", "superice10hI5ciUaf5epvPOtDmNYDxty4");

            ToaSfApexServiceService service = new ToaSfApexServiceService();

            service.SessionHeaderValue = new WebsiteToSf.SForce.ApexSOAP.SessionHeader();
            service.SessionHeaderValue.sessionId = rs.sessionId;
            var result = service.ToaFaq(txtName.Text, txtEmail.Text, txtFaq.Text);
            txtResult.Text = " Return from salesforce is : " + result.ToString();

        }
    }
}
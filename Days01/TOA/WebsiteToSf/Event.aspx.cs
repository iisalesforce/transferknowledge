﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteToSf.SForce;
using WebsiteToSf.SForce.ApexSOAP;

namespace WebsiteToSf
{
    public partial class Event : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SforceService sf = new SforceService();
            LoginResult rs = sf.login("ii_support@toagroup.com.developer", "superice10hI5ciUaf5epvPOtDmNYDxty4");

            ToaSfApexServiceService service = new ToaSfApexServiceService();

            service.SessionHeaderValue = new WebsiteToSf.SForce.ApexSOAP.SessionHeader();
            service.SessionHeaderValue.sessionId = rs.sessionId;

            var dto = new EventDTO();
            dto.Job_Code = txtJobCode.Text;

            dto.name_event = txtName.Text;
            dto.phone_event = txtTel.Text;
            dto.age_event = txtAge.Text;
            dto.email_event = txtEmail.Text;
            dto.building_type = "บ้านพักอาศัย";
            dto.Other_type_building = "ลักษณะอาคารอื่นๆ  B";
            dto.floor = "ชั้น C ";
            dto.project_type = "ภายใน";
            dto.construction_process = "กำลังทำแบบก่อสร้าง";
            dto.ide_color = "เคย";
            dto.source_news = "เคยใช้บริการ,ผู้รับเหมาแนะนำ";
            dto.magazine = "เปรียว";
            dto.other_infomation_color = "ร้านขายสี";
            dto.other_brand_service = "เคย";
            dto.service_brand = "ICI,เบเยอร์";
            dto.other_brand = "จระเข้";
            dto.information_color = "นิตยสาร";
            dto.website_information_color = "www.google.com";
            dto.magazine_information_color = "";
            dto.tv_information_color = "";
            dto.other_news = "";
            dto.designer_name = "";
            dto.requirement_customer = "";
            dto.product_choose = "";
            dto.shade_source = "เฉดแคตาล๊อก";
            var tmpDate = "28/08/2013".Split('/').ToArray();
            var strdate = tmpDate[2] + "-" + tmpDate[1] + "-" + tmpDate[0];
            dto.date_design = strdate;
            dto.time_design_hour = "10";
            dto.time_design_minute = "50";
            EventResult result;
            try
            {
                result = service.ToaEvent(dto);
            }
            catch (Exception ex)
            {
                txtResult.Text = ex.Message;
                return;
            }
            txtResult.Text = " Return from salesforce is case number  : " + result.CaseNumber;
        }
    }
}
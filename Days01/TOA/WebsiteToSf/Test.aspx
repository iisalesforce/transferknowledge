﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="WebsiteToSf.Test"
    Culture="th-TH" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:DropDownList ID="ddlDate" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDate_SelectedIndexChanged">
        </asp:DropDownList>
        <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_Click" />
    </div>
    <div>
        // Id, HttpHeaders, JobCode, Description, CreateDate
        <asp:GridView ID="grdList" runat="server" AutoGenerateColumns="False" CellPadding="4"
            ForeColor="#333333" GridLines="None" Width="100%">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField AccessibleHeaderText="Id" ItemStyle-VerticalAlign="Top" DataField="Id"
                    HeaderText="Id">
                    <HeaderStyle Width="50px" />
                    <ItemStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField AccessibleHeaderText="JobCode" HtmlEncode="false" DataField="JobCode"
                    HeaderText="JobCode">
                    <ItemStyle Width="80px" />
                </asp:BoundField>
                <asp:BoundField AccessibleHeaderText="HttpHeaders" ItemStyle-VerticalAlign="Top"
                    DataField="HttpHeaders" HeaderText="HttpHeaders">
                    <HeaderStyle Width="250px" />
                    <ItemStyle HorizontalAlign="Center" Width="250px" />
                </asp:BoundField>
                <asp:BoundField AccessibleHeaderText="Description" ItemStyle-VerticalAlign="Top"
                    DataField="Description" HeaderText="Description">
                    <HeaderStyle Width="250px" />
                    <ItemStyle HorizontalAlign="Center" Width="150px" />
                </asp:BoundField>
                <asp:BoundField AccessibleHeaderText="CreateDate" ItemStyle-VerticalAlign="Top" DataField="CreateDate"
                    HeaderText="CreateDate">
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                </asp:BoundField>
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    </div>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteToSf.DataAccess;
using WebsiteToSf.MySql;
using System.Data;

namespace WebsiteToSf
{
    public partial class Test : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {

        }
        protected void Page_Load(object sender, EventArgs e)
        {

            var date = DateTime.Now;//.ToString("yyyy-MM-dd");


            if (!IsPostBack)
            {
                ddlDate.Items.Add(new ListItem(date.ToString("yyyy-MM-dd"), date.ToString("yyyy-MM-dd")));
                ddlDate.Items.Add(new ListItem(date.AddDays(-1).ToString("yyyy-MM-dd"), date.AddDays(-1).ToString("yyyy-MM-dd")));
                ddlDate.Items.Add(new ListItem(date.AddDays(-2).ToString("yyyy-MM-dd"), date.AddDays(-2).ToString("yyyy-MM-dd")));
                ddlDate.Items.Add(new ListItem(date.AddDays(-3).ToString("yyyy-MM-dd"), date.AddDays(-3).ToString("yyyy-MM-dd")));
                ddlDate.Items.Add(new ListItem(date.AddDays(-4).ToString("yyyy-MM-dd"), date.AddDays(-4).ToString("yyyy-MM-dd")));
                ddlDate.Items.Add(new ListItem(date.AddDays(-5).ToString("yyyy-MM-dd"), date.AddDays(-5).ToString("yyyy-MM-dd")));
                ddlDate.Items.Add(new ListItem(date.AddDays(-6).ToString("yyyy-MM-dd"), date.AddDays(-6).ToString("yyyy-MM-dd")));
                ddlDate.Items.Add(new ListItem(date.AddDays(-7).ToString("yyyy-MM-dd"), date.AddDays(-7).ToString("yyyy-MM-dd")));
                ddlDate.Items.Add(new ListItem(date.AddDays(-8).ToString("yyyy-MM-dd"), date.AddDays(-8).ToString("yyyy-MM-dd")));
                ddlDate.Items.Add(new ListItem(date.AddDays(-9).ToString("yyyy-MM-dd"), date.AddDays(-9).ToString("yyyy-MM-dd")));
                ddlDate.Items.Add(new ListItem(date.AddDays(-10).ToString("yyyy-MM-dd"), date.AddDays(-10).ToString("yyyy-MM-dd")));
                ddlDate.SelectedIndex = 0;
                using (DBConnect db = new DBConnect())
                {
                    DataTable dt = db.SelectJobCode(ddlDate.SelectedValue);
                    grdList.DataSource = dt;
                    grdList.DataBind();
                }
            }


        }

        private void SelectData()
        {
            using (DBConnect db = new DBConnect())
            {
                DataTable dt = db.SelectJobCode("2557-02-10");
            }
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
        }
        protected void ddlDate_SelectedIndexChanged(object sender, EventArgs e)
        {


            using (DBConnect db = new DBConnect())
            {

                DataTable dt = db.SelectJobCode(ddlDate.SelectedValue);
                grdList.DataSource = dt;
                grdList.DataBind();
            }
        }
    }
}
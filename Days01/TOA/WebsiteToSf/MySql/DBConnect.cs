﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using WebsiteToSf.SForce.Toa.RegisterExService;
using NLog;


namespace WebsiteToSf.MySql
{
    public class DBConnect : IDisposable
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;

        //Constructor
        public DBConnect()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            server = "crm.toagroup.com";
            database = "db_webcrm";
            uid = "db_webcrm";
            password = "db@webcrm";



            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";Charset=utf8;";//  fix Thai

            connection = new MySqlConnection(connectionString);
        }

        //open connection to database
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        // MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        // MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                // MessageBox.Show(ex.Message);
                return false;
            }
        }

        //Insert statement
        public void Insert(string JobCode, string Description, string HttpHeaders = "")
        {
            string query = " INSERT INTO toajobcode (JobCode,Description,CreateDate,HttpHeaders) VALUES (@JobCode,@Description,@CreateDate,@HttpHeaders) ";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.Parameters.AddWithValue("@JobCode", JobCode);
                cmd.Parameters.AddWithValue("@Description", Description);
                cmd.Parameters.AddWithValue("@CreateDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                cmd.Parameters.AddWithValue("@HttpHeaders", HttpHeaders);
                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }


        public DataTable SelectJobCode(String date)
        {

            DataTable dt = new DataTable();
            if (this.OpenConnection() == true)
            {
                string select = " select * from toajobcode where (CreateDate BETWEEN '" + date + " 00:00:00' AND '" + date + " 23:59:59') ";
                MySqlCommand cmd = new MySqlCommand(select, connection);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
            }
            return dt;
        }
        //Count statement
        public int Count(string jobCode)
        {
            string query = "SELECT Count(*) FROM toajobcode WHERE JobCode = '" + jobCode + "'";
            int Count = -1;

            //Open Connection
            if (this.OpenConnection() == true)
            {
                //Create Mysql Command
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //ExecuteScalar will return one value
                Count = int.Parse(cmd.ExecuteScalar() + "");

                //close Connection
                this.CloseConnection();

                return Count;
            }
            else
            {
                return Count;
            }
        }
        /// <summary>
        ///  Check Job was Done before or nat
        /// </summary>
        /// <param name="jobcode"></param>
        /// <returns>
        ///  2 : Not DO Before
        ///  1 : Done
        ///  0 : Do Before But Not Successful
        /// 
        /// </returns>
        public int DoneExJobBefore(string jobcode)
        {
            int status = 2;
            string query = "SELECT id ,jobcode ,isdone FROM registerex WHERE jobcode = '" + jobcode + "'";
            DataTable dt = new DataTable();
            //Open Connection
            if (this.OpenConnection() == true)
            {

                if (this.OpenConnection() == true)
                {

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    da.Fill(dt);
                }
                if (dt.Rows.Count > 0)
                {
                    status = int.Parse(dt.Rows[0]["isdone"].ToString());
                }
            }
            return status;
        }

        public void InsertRegisterEx(RegisterExDTO dto)
        {
            var logger = LogManager.GetCurrentClassLogger();      // Log Objct
            try
            {
                string query = " INSERT INTO registerex (jobcode, createdate, f_st2name, f_sex, f_birth, f_phone, f_st2email, f_province, f_st2status, f_st3know, f_sent_1, f_sent_2, f_st2type, f_st2pjtype, f_pd_ex, f_pd_in, f_pd_sp, f_req5, f_req4, f_pdrequire, f_pdcolortone, f_st3kb, f_st3kblike, f_st3like, f_st3dislike, FILE1, FILE2, FILE3, FILE4, FILE5, FILE_Z, isdone) "
                    + " VALUES (@jobcode, @createdate, @f_st2name, @f_sex, @f_birth, @f_phone, @f_st2email, @f_province, @f_st2status, @f_st3know, @f_sent_1, @f_sent_2, @f_st2type, @f_st2pjtype, @f_pd_ex, @f_pd_in, @f_pd_sp, @f_req5, @f_req4, @f_pdrequire, @f_pdcolortone, @f_st3kb, @f_st3kblike, @f_st3like, @f_st3dislike, @FILE1, @FILE2, @FILE3, @FILE4, @FILE5, @FILE_Z, @isdone) ";
                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.Parameters.AddWithValue("@jobcode", dto.jobcode);
                    cmd.Parameters.AddWithValue("@createdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@f_st2name", dto.f_st2name);
                    cmd.Parameters.AddWithValue("@f_sex", dto.f_sex);
                    cmd.Parameters.AddWithValue("@f_birth", dto.f_birth);
                    cmd.Parameters.AddWithValue("@f_phone", dto.f_phone);
                    cmd.Parameters.AddWithValue("@f_st2email", dto.f_st2email);
                    cmd.Parameters.AddWithValue("@f_province", dto.f_province);
                    cmd.Parameters.AddWithValue("@f_st2status", dto.f_st2status);
                    cmd.Parameters.AddWithValue("@f_st3know", dto.f_st3know);
                    cmd.Parameters.AddWithValue("@f_sent_1", dto.f_sent_1);
                    cmd.Parameters.AddWithValue("@f_sent_2", dto.f_sent_2);
                    cmd.Parameters.AddWithValue("@f_st2type", dto.f_st2type);
                    cmd.Parameters.AddWithValue("@f_st2pjtype", dto.f_st2pjtype);
                    cmd.Parameters.AddWithValue("@f_pd_ex", dto.f_pd_ex);
                    cmd.Parameters.AddWithValue("@f_pd_in", dto.f_pd_in);
                    cmd.Parameters.AddWithValue("@f_pd_sp", dto.f_pd_sp);
                    cmd.Parameters.AddWithValue("@f_req5", dto.f_req5);
                    cmd.Parameters.AddWithValue("@f_req4", dto.f_req4);
                    cmd.Parameters.AddWithValue("@f_pdrequire", dto.f_pdrequire);
                    cmd.Parameters.AddWithValue("@f_pdcolortone", dto.f_pdcolortone);
                    cmd.Parameters.AddWithValue("@f_st3kb", dto.f_st3kb);
                    cmd.Parameters.AddWithValue("@f_st3kblike", dto.f_st3kblike);
                    cmd.Parameters.AddWithValue("@f_st3like", dto.f_st3like);
                    cmd.Parameters.AddWithValue("@f_st3dislike", dto.f_st3dislike);
                    cmd.Parameters.AddWithValue("@FILE1", dto.FILE1);
                    cmd.Parameters.AddWithValue("@FILE2", dto.FILE2);
                    cmd.Parameters.AddWithValue("@FILE3", dto.FILE3);
                    cmd.Parameters.AddWithValue("@FILE4", dto.FILE4);
                    cmd.Parameters.AddWithValue("@FILE5", dto.FILE5);
                    cmd.Parameters.AddWithValue("@FILE_Z", dto.FILE_Z);
                    cmd.Parameters.AddWithValue("@isdone", 0);
                    //Execute command
                    cmd.ExecuteNonQuery();
                }

                //close connection
                this.CloseConnection();
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Error,"Mysql Inser Data Error : " + ex.Message);
                 
            }
        }
        public void UpdateInsertRegisterExStatus(RegisterExDTO dto, int status)
        {

        }

        public void Dispose()
        {
            CloseConnection();
        }
    }
}
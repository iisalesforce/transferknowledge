﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebsiteToSf.dto
{
    #region In-Memory Object Classes
    public class ToaJobDTO
    {

        public String JobCode { get; set; }
        public DateTime CreateDate { get; set; }
    }
    #endregion
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="ideacolor_queue.aspx.cs" Inherits="WebsiteToSf.ideacolor_queue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 812px;
            height: 255px;
        }
        .style2
        {
            width: 100%;
        }
        .style3
        {
            width: 192px;
            text-align: right;
            padding-right: 5px;
        }
        .style4
        {
            text-align: right;
        }
    </style>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <img alt="" class="style1" src="img_queue_banner.jpg" /></p>
    <table class="style2">
        <tr>
            <td class="style3">
                <span style="color: rgb(142, 142, 142); font-family: Tahoma; font-size: 12px; font-style: normal;
                    font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal;
                    orphans: auto; text-align: -webkit-left; text-indent: 0px; text-transform: none;
                    white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;
                    display: inline !important; float: none;">ชื่อ</span><span class="text12r" style="font-family: 'Microsoft Sans Serif';
                        font-size: 12px; color: rgb(255, 0, 0); font-weight: normal; font-style: normal;
                        font-variant: normal; letter-spacing: normal; line-height: normal; orphans: auto;
                        text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;">*</span>
            </td>
            <td>
                <asp:TextBox ID="txtName" runat="server" Width="225px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style3">
                <span class="normal_txt12_g1" style="font-family: Tahoma; font-size: 12px; color: rgb(142, 142, 142);
                    font-weight: normal; font-style: normal; font-variant: normal; letter-spacing: normal;
                    line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px;
                    text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;
                    padding-top: 10px;">วันที่นัดหมาย</span><span class="mainfonts_b2" style="color: rgb(142, 142, 142);
                        font-family: Tahoma; font-size: 12px; font-style: normal; font-variant: normal;
                        font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto;
                        text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span class="text12r"
                            style="font-family: 'Microsoft Sans Serif'; font-size: 12px; color: rgb(255, 0, 0);
                            font-weight: normal;">*</span></span>
            </td>
            <td>
                <asp:TextBox ID="txtDate" runat="server" Width="225px" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style3">
                <span class="normal_txt12_g1" style="font-family: Tahoma; font-size: 12px; color: rgb(142, 142, 142);
                    font-weight: normal; font-style: normal; font-variant: normal; letter-spacing: normal;
                    line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px;
                    text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;
                    padding-top: 10px;">ช่วงเวลานัดหมาย</span><span class="mainfonts_b2" style="color: rgb(142, 142, 142);
                        font-family: Tahoma; font-size: 12px; font-style: normal; font-variant: normal;
                        font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto;
                        text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span class="text12r"
                            style="font-family: 'Microsoft Sans Serif'; font-size: 12px; color: rgb(255, 0, 0);
                            font-weight: normal;">*</span><table border="0" cellpadding="0" cellspacing="0" style="color: rgb(0, 0, 0);
                                font-family: Tahoma, Geneva, sans-serif; font-size: 16px; font-style: normal;
                                font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal;
                                orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto;
                                word-spacing: 0px; -webkit-text-stroke-width: 0px; width: 95%; margin-right: 2px;">
                                <tr>
                                    <td bgcolor="#F3F3F3" class="style4" height="25" style="font-family: Tahoma; font-size: 12px;
                                        color: rgb(142, 142, 142); font-weight: normal;" valign="middle">
                                        <span class="text12r" style="font-family: 'Microsoft Sans Serif'; font-size: 12px;
                                            color: rgb(255, 0, 0); font-weight: normal;">วัน -&nbsp;เวลาทำการ<span class="Apple-converted-space">&nbsp;</span><br />
                                            วันจันทร์ - ศุกร์ เวลา 8.30 - 17.30 น.</span>
                                    </td>
                                </tr>
                            </table>
                    </span>
            </td>
            <td>
                <asp:DropDownList ID="ddlTime" runat="server">
                    <asp:ListItem>9.00 - 12.00 น.</asp:ListItem>
                    <asp:ListItem>14.00 - 17.00 น.</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style3">
                <span class="mainfonts_b2" style="color: rgb(142, 142, 142); font-family: Tahoma;
                    font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal;
                    letter-spacing: normal; line-height: normal; orphans: auto; text-align: -webkit-left;
                    text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px;
                    -webkit-text-stroke-width: 0px;"><span class="text12r" style="font-family: 'Microsoft Sans Serif';
                        font-size: 12px; color: rgb(255, 0, 0); font-weight: normal;"><span class="normal_txt12_g1"
                            style="font-family: Tahoma; font-size: 12px; color: rgb(142, 142, 142); font-weight: normal;
                            padding-top: 10px;">เบอร์โทร.(ที่สามารถติดต่อได้)</span>*</span></span>
            </td>
            <td>
                <asp:TextBox ID="txtPhone" runat="server" Width="225px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style3">
                <span class="normal_txt12_g1" style="font-family: Tahoma; font-size: 12px; color: rgb(142, 142, 142);
                    font-weight: normal; font-style: normal; font-variant: normal; letter-spacing: normal;
                    line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px;
                    text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;
                    padding-top: 10px;">อีเมล์</span><span class="mainfonts_b2" style="color: rgb(142, 142, 142);
                        font-family: Tahoma; font-size: 12px; font-style: normal; font-variant: normal;
                        font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto;
                        text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; padding-top: 10px;"><span
                            class="text12r" style="font-family: 'Microsoft Sans Serif'; font-size: 12px;
                            color: rgb(255, 0, 0); font-weight: normal;">*</span></span>
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" Width="225px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style3">
                <span style="color: rgb(142, 142, 142); font-family: Tahoma; font-size: 12px; font-style: normal;
                    font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal;
                    orphans: auto; text-align: -webkit-left; text-indent: 0px; text-transform: none;
                    white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;
                    display: inline !important; float: none;">รายละเอียด</span>
            </td>
            <td>
                <asp:TextBox ID="txtDetial" runat="server" Rows="6" TextMode="MultiLine" Width="225px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;
            </td>
            <td>
                <asp:Button ID="Button1" runat="server" Text="ส่ง" Width="107px" OnClick="Button1_Click" />
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;
            </td>
            <td>
                <asp:Label ID="txtResult" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <br />

     <script type="text/javascript" >
         $(function () {
             $.datepicker.formatDate("dd/mm/yy");
           

             $("#<%=txtDate.ClientID %>").datepicker();
             $("#<%=txtDate.ClientID %>").datepicker("option", "dateFormat", "dd/mm/yy");
         });
    
    
    </script>
</asp:Content>

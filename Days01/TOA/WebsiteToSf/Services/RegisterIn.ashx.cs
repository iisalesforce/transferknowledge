﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteToSf.SForce;
using NLog;
using WebsiteToSf.Util;
using System.Text.RegularExpressions;
using WebsiteToSf.SForce.Toa.RegisterInService;
using System.IO;
using System.Configuration;
using System.Text;
using WebsiteToSf.MySql;

namespace WebsiteToSf.Services
{
    /// <summary>
    /// Summary description for RegisterIn
    /// </summary>
    public class RegisterIn : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {



            var jobcode = "";
            var header = context.Request.Headers;
            var logger = LogManager.GetCurrentClassLogger();
            #region Old Code
            //if (ConfigurationManager.AppSettings["deepLog"] == "1")
            //{
            //    string filePath = context.Server.MapPath("~/log/RegisterInProcessLog.txt");

            //    if (!File.Exists(filePath))
            //    {
            //        File.Create(filePath);
            //    }

            //    //    writer all parameter to file

            //    using (System.IO.StreamWriter file = new System.IO.StreamWriter(filePath))
            //    {


            //        file.WriteLine("============================================================================");
            //        file.WriteLine("==========          New Register  In Data  From TOA          ===============");
            //        file.WriteLine("============================================================================");
            //        foreach (string key in context.Request.Form.AllKeys)
            //        {
            //            file.WriteLine("key name : " + key + "||  \t value : " + SfUtility.windows874_unicode(context.Request.Form[key]));
            //            if (key == "file1")
            //            {
            //                var fileurl = ToThai(context, "file1").Trim();
            //                string[] files = fileurl.Substring(0, fileurl.Length - 1).Split(',');

            //                file.WriteLine("file array size : " + files.Count());

            //                int i = 1;
            //                for (int j = 0; j < files.Length; j++)
            //                {
            //                    if (files[j].Contains(".zip"))
            //                    {
            //                        file.WriteLine("file zip : " + files[j]);

            //                    }
            //                    else
            //                    {
            //                        if (i == 1)
            //                        {
            //                            file.WriteLine("file " + i + " : " + files[j]);
            //                            i++;
            //                        }
            //                        else if (i == 2)
            //                        {
            //                            file.WriteLine("file " + i + " : " + files[j]);
            //                            i++;
            //                        }
            //                        else if (i == 3)
            //                        {
            //                            file.WriteLine("file " + i + " : " + files[j]);
            //                            i++;
            //                        }
            //                        else if (i == 4)
            //                        {
            //                            file.WriteLine("file " + i + " : " + files[j]);
            //                            i++;
            //                        }
            //                        else if (i == 5)
            //                        {
            //                            file.WriteLine("file " + i + " : " + files[j]);
            //                            i++;
            //                        }
            //                    }
            //                }

            //            }
            //        }
            //        file.WriteLine("============================================================================");
            //    }
            //}

            #endregion
            Boolean IsDoBefore = false;
            using (DBConnect db = new DBConnect())
            {
                 jobcode = SfUtility.windows874_unicode(context.Request.Form["jobcode"]);


                if (db.Count(jobcode) <= 0)
                {
                    /**************************************************
                     * fixed  เวลามี job ใหม่มา
                     * ************************************************/
                    StringBuilder deepLogMessage = new StringBuilder();
                    StringBuilder httpheaders = new StringBuilder();

                    try
                    {


                        foreach (string  httpkey in header.AllKeys)
                        {
                            httpheaders.AppendLine("Key "+httpkey+ " = "+   header[httpkey].ToString());
                        }                        

                        deepLogMessage.AppendLine("==================================");
                        deepLogMessage.AppendLine(" New Register  IN Data  From TOA  ");
                        deepLogMessage.AppendLine("----------------------------------");
                        deepLogMessage.AppendLine("Date : " + DateTime.Now.ToString("O"));
                        foreach (string key in context.Request.Form.AllKeys)
                        {
                            deepLogMessage.AppendLine("key name : " + key + "||  \t value : " + SfUtility.windows874_unicode(context.Request.Form[key]));
                            if (key == "file1")
                            {
                                var fileurl = ToThai(context, "file1").Trim();
                                string[] files = fileurl.Substring(0, fileurl.Length - 1).Split(',');
                                deepLogMessage.AppendLine("file array size : " + files.Count());
                                int i = 1;
                                for (int j = 0; j < files.Length; j++)
                                {
                                    if (files[j].Contains(".zip"))
                                    {
                                        deepLogMessage.AppendLine("file zip : " + files[j]);
                                    }
                                    else
                                    {
                                        if (i == 1)
                                        {
                                            deepLogMessage.AppendLine("file " + i + " : " + files[j]);
                                            i++;
                                        }
                                        else if (i == 2)
                                        {
                                            deepLogMessage.AppendLine("file " + i + " : " + files[j]);
                                            i++;
                                        }
                                        else if (i == 3)
                                        {
                                            deepLogMessage.AppendLine("file " + i + " : " + files[j]);
                                            i++;
                                        }
                                        else if (i == 4)
                                        {
                                            deepLogMessage.AppendLine("file " + i + " : " + files[j]);
                                            i++;
                                        }
                                        else if (i == 5)
                                        {
                                            deepLogMessage.AppendLine("file " + i + " : " + files[j]);
                                            i++;
                                        }
                                    }
                                }

                            }
                        }

                        db.Insert(jobcode, deepLogMessage.ToString(), httpheaders.ToString());
                    }
                    catch (Exception ex)
                    {
                        logger.Log(LogLevel.Error, "Can't Log Deep Log with error >> " + ex.Message);
                    }
                }
                else
                {
                    IsDoBefore = true;
                }
            }

            if (!IsDoBefore)
            {
                
                /****************************************************
                * เราใช้ StringBuild ในการสร้าง Log Strgin แทนเลย
                * **************************************************/
                StringBuilder logMessage = new StringBuilder();
                //logger.Log(LogLevel.Info, "New Register In");
                logMessage.Append("New Register In");
                var dto = new RegisterInDTO();
                #region
                try
                {
                    #region
                    /* **************************************
                     *  Fixed Error
                     * **************************************/
                    dto = initDefualtValue(dto);

                    if (context.Request.Form["f_st2name"] != null)
                    {
                        dto.f_st2name = SfUtility.windows874_unicode(context.Request.Form["f_st2name"]);
                        dto.f_st2name = SfUtility.CollapseSpaces(dto.f_st2name);
                        logMessage.Append(String.Format("f_st2name : {0}", dto.f_st2name));
                    } 

                    if (context.Request.Form["jobcode"] != null)
                    {
                        dto.jobcode = context.Request.Form["jobcode"];

                        logMessage.Append(String.Format("jobcode : {0}", dto.jobcode));
                    }
                    if (context.Request.Form["f_st2dep"] != null)
                    {
                        dto.f_st2dep = SfUtility.windows874_unicode(context.Request.Form["f_st2dep"]);
                        logMessage.Append(String.Format("f_st2dep : {0}", dto.f_st2dep));
                    }
                    if (context.Request.Form["f_st2pjname"] != null)
                    {
                        dto.f_st2pjname = ToThai(context, "f_st2pjname");
                        logMessage.Append(String.Format("f_st2pjname : {0}", dto.f_st2pjname));
                    }
                    if (context.Request.Form["f_province"] != null)
                    {
                        dto.f_province = ToThai(context, "f_province");
                        logMessage.Append(String.Format("f_province : {0}", dto.f_province));
                    }
                    if (context.Request.Form["f_phone"] != null)
                    {
                        dto.f_phone = context.Request.Form["f_phone"];
                        var pattern = @"-";
                        Regex rgx = new Regex(pattern);
                        string result1 = rgx.Replace(dto.f_phone, "");
                        dto.f_phone2 = result1;

                        if (result1.Length == 10)
                        {
                            dto.f_phone = SfUtility.beautifyPhoneNumber(result1, "");
                        }


                        logMessage.Append(String.Format("f_phone : {0}", dto.f_phone));
                    }
                    if (context.Request.Form["f_st2email"] != null)
                    {
                        dto.f_st2email = context.Request.Form["f_st2email"];
                        logMessage.Append(String.Format("f_st2email : {0}", dto.f_st2email));

                    }

                    if (context.Request.Form["f_sent_1"] != null)
                    {
                        dto.f_sent_1 = ToThai(context, "f_sent_1");
                        logMessage.Append(String.Format("f_sent_1 : {0}", dto.f_sent_1));
                    }
                    if (context.Request.Form["f_sent_2"] != null)
                    {
                        dto.f_sent_2 = ToThai(context, "f_sent_2");
                        logMessage.Append(String.Format("f_sent_2 : {0}", dto.f_sent_2));
                    }
                    if (context.Request.Form["f_st2type"] != null)
                    {
                        dto.f_st2type = ToThai(context, "f_st2type");
                        logMessage.Append(String.Format("f_st2type : {0}", dto.f_st2type));
                    }
                    if (context.Request.Form["f_st2pjtype"] != null)
                    {
                        dto.f_st2pjtype = ToThai(context, "f_st2pjtype");
                        logMessage.Append(String.Format("f_st2pjtype : {0}", dto.f_st2pjtype));
                    }
                    if (context.Request.Form["f_pd_ex"] != null)
                    {
                        dto.f_pd_ex = ToThai(context, "f_pd_ex");
                        logMessage.Append(String.Format("f_pd_ex : {0}", dto.f_pd_ex));
                    }
                    if (context.Request.Form["f_pd_in"] != null)
                    {
                        dto.f_pd_in = ToThai(context, "f_pd_in");
                        logMessage.Append(String.Format("f_pd_in : {0}", dto.f_pd_in));
                    }
                    if (context.Request.Form["f_pd_sp"] != null)
                    {
                        dto.f_pd_sp = ToThai(context, "f_pd_sp");
                        logMessage.Append(String.Format("f_pd_sp : {0}", dto.f_pd_sp));
                    }
                    if (context.Request.Form["f_req5"] != null)
                    {

                        var f_req5 = ToThai(context, "f_req5");
                        if (f_req5 == "f_req5")
                            dto.f_req5 = "1";
                        else
                            dto.f_req5 = "0";


                        logMessage.Append(String.Format("f_req5 : {0}", dto.f_req5));
                    }
                    else
                    {
                        dto.f_req5 = "0";
                        logMessage.Append(String.Format("f_req5 : {0}", dto.f_req5));
                    }

                    if (context.Request.Form["f_req4"] != null)
                    {
                        dto.f_req4 = ToThai(context, "f_req4");
                        logMessage.Append(String.Format("f_req4 : {0}", dto.f_req4));
                    }

                    if (context.Request.Form["f_pdrequire"] != null)
                    {
                        dto.f_pdrequire = ToThai(context, "f_pdrequire");
                        logMessage.Append(String.Format("f_pdrequire : {0}", dto.f_pdrequire));
                    }
                    if (context.Request.Form["f_pdcolortone"] != null)
                    {
                        dto.f_pdcolortone = ToThai(context, "f_pdcolortone");
                        logMessage.Append(String.Format("f_pdcolortone : {0}", dto.f_pdcolortone));
                    }



                    if (context.Request.Form["f_st3kb"] != null)
                    {
                        dto.f_st3kb = ToThai(context, "f_st3kb");
                        logMessage.Append(String.Format("f_st3kb : {0}", dto.f_st3kb));
                    }
                    if (context.Request.Form["f_st3kblike"] != null)
                    {
                        dto.f_st3kblike = ToThai(context, "f_st3kblike");
                        logMessage.Append(String.Format("f_st3kblike : {0}", dto.f_st3kblike));
                    }
                    if (context.Request.Form["f_st3like"] != null)
                    {
                        dto.f_st3like = ToThai(context, "f_st3like");
                        logMessage.Append(String.Format("f_st3like : {0}", dto.f_st3like));
                    }
                    if (context.Request.Form["f_st3dislike"] != null)
                    {
                        dto.f_st3dislike = ToThai(context, "f_st3dislike");
                        logMessage.Append(String.Format("f_st3dislike : {0}", dto.f_st3dislike));
                    }

                    if (context.Request.Form["f_other"] != null)
                    {
                        dto.f_other = ToThai(context, "f_other");
                        logMessage.Append(String.Format("f_other : {0}", dto.f_other));
                    }

                    //  TOA  ส่ง File  มาเป็น String ที่ Concat กัน
                    if (context.Request.Form["file1"] != null)
                    {
                        var fileurl = ToThai(context, "file1").Trim();
                        string[] files = fileurl.Substring(0, fileurl.Length - 1).Split(',');

                        int i = 1;
                        for (int j = 0; j < files.Length; j++)
                        {
                            if (files[j].Contains(".zip"))
                            {
                                dto.FILE_Z = files[j];

                            }
                            else
                            {
                                if (i == 1)
                                {
                                    dto.FILE1 = files[j]; i++;
                                    logMessage.Append(String.Format("FILE1 : {0}", dto.FILE1));
                                }
                                else if (i == 2)
                                {
                                    dto.FILE2 = files[j]; i++;
                                    logMessage.Append(String.Format("FILE2 : {0}", dto.FILE2));
                                }
                                else if (i == 3)
                                {
                                    dto.FILE3 = files[j]; i++;
                                    logMessage.Append(String.Format("FILE3 : {0}", dto.FILE3));
                                }
                                else if (i == 4)
                                {
                                    dto.FILE4 = files[j]; i++;
                                    logMessage.Append(String.Format("FILE4 : {0}", dto.FILE4));
                                }
                                else if (i == 5)
                                {
                                    dto.FILE5 = files[j]; i++;
                                    logMessage.Append(String.Format("FILE5 : {0}", dto.FILE5));
                                }
                            }
                        }
                    }
                #endregion
                }
                catch (Exception ex)
                {

                    logger.Log(LogLevel.Error, "Parameter Error : " + ex.Message);
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("false");
                    context.Response.End();
                }
                #endregion

                #region
                if (dto.IsValid())
                {
                    try
                    {
                        // Salesforce Login Section 
                        using (SalesforceServiceProvider sf = new SalesforceServiceProvider())
                        {
                            LoginResult rs = sf.LoginProduction();
                            ToaRegisterInServiceService service = new ToaRegisterInServiceService();
                            service.SessionHeaderValue = new WebsiteToSf.SForce.Toa.RegisterInService.SessionHeader();
                            // Set Session Header for request data or services
                            service.SessionHeaderValue.sessionId = rs.sessionId;

                            var result = service.ToaRegisterIn(dto);
                            logger.Log(LogLevel.Info,
                                "Data : jobcode => " + dto.jobcode + "  f_st2name => " + dto.f_st2name + " f_st2pjname  => " + dto.f_st2pjname + "  Process Result : " + result);
                        }

                    }
                    catch (Exception ex)
                    {
                        logger.Log(LogLevel.Error, "Salesforce Process Error : " + ex.Message);
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("false");
                        context.Response.End();
                    }
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("true");
                    context.Response.End();

                }
                else
                {
                    logger.Log(LogLevel.Info, "ข้อมูลไม่ครบถ้วน");
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("false");
                    context.Response.End();
                }
                #endregion
            }

            logger.Log(LogLevel.Error, jobcode+" รายการซ้ำ");
            context.Response.ContentType = "text/plain";
            context.Response.Write("false");
            context.Response.End();

        }
        public string ToThai(HttpContext context, string key)
        {
            // return context.Request.Form[key];
            return SfUtility.windows874_unicode(context.Request.Form[key]);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public RegisterInDTO initDefualtValue(RegisterInDTO dto)
        {
            dto.f_other = " ";
            dto.f_pd_ex = " ";
            dto.f_pd_in = " ";
            dto.f_pd_sp = " ";
            dto.f_pdcolortone = " ";
            dto.f_pdrequire = " ";
            dto.f_province = " ";
            dto.f_req4 = " ";
            dto.f_req5 = " ";
            dto.f_req5 = " ";
            dto.f_sent_1 = " ";
            dto.f_sent_2 = " ";
            dto.f_st2dep = " ";
            dto.f_st2name = " ";
            dto.f_st2pjname = " ";
            dto.f_st2pjtype = " ";
            dto.f_st2type = " ";
            dto.f_st3dislike = " ";
            dto.f_st3kb = " ";
            dto.f_st3kb = " ";
            dto.f_st3like = " ";
            return dto;
        }
    }
}
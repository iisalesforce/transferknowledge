﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using NLog;
using System.Text;
using WebsiteToSf.SForce;
using WebsiteToSf.SForce.ApexSOAP;
using WebsiteToSf.SForce.Avaya.Ivr;

namespace WebsiteToSf.Services
{

    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class avayaivr : System.Web.Services.WebService
    {

        [WebMethod]
        public string SaveIvrScore(string UCID, string ANI, int QuestionNo = 0, int Score = 0)
        {
            var logger = LogManager.GetCurrentClassLogger();
            try
            {

                StringBuilder logMsg = new StringBuilder();

                logger.Log(LogLevel.Info, "  New IVR  Data  ");
                logger.Log(LogLevel.Info, string.Format(" | UCID : {0}", UCID));
                logger.Log(LogLevel.Info, string.Format(" | ANI : {0}", ANI));
                logger.Log(LogLevel.Info, string.Format(" | Question No : {0}", QuestionNo));
                logger.Log(LogLevel.Info, string.Format(" | Score : {0}", Score));
                logger.Log(LogLevel.Info, logMsg.ToString());

                if (!String.IsNullOrEmpty(UCID))
                {
                    /************************************************
                     *  Login Salesforce Production
                     *  Fix bug : use  using for dispose object prvent many login object
                     * **********************************************/
                    using (SalesforceServiceProvider sf = new SalesforceServiceProvider())
                    {
                        LoginResult rs = sf.LoginProduction();
                        ToaSfAvayaService service = new ToaSfAvayaService();
                        service.SessionHeaderValue = new SForce.Avaya.Ivr.SessionHeader();
                        service.SessionHeaderValue.sessionId = rs.sessionId;
                        var result = service.RecordIvrScore(UCID, ANI, QuestionNo.ToString(), Score.ToString());
                        logger.Log(LogLevel.Info,"  IVR Process Status => " + result.Value);
                        return "true";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Info, "Fail to sent ivr data to  salesforce with error : " + ex.Message);
                System.Threading.Thread.Sleep(2000);
                try
                {
                    logger.Log(LogLevel.Info, " Try to send data again ");
                    logger.Log(LogLevel.Info, string.Format(" | UCID : {0}", UCID));
                    logger.Log(LogLevel.Info, string.Format(" | ANI : {0}", ANI));
                    logger.Log(LogLevel.Info, string.Format(" | Question No : {0}", QuestionNo));
                    logger.Log(LogLevel.Info, string.Format(" | Score : {0}", Score));

                    /************************************************
                     *  Login Salesforce Production
                     * **********************************************/
                    using (SalesforceServiceProvider sf = new SalesforceServiceProvider())
                    {
                        LoginResult rs = sf.LoginProduction();
                        ToaSfAvayaService service = new ToaSfAvayaService();
                        service.SessionHeaderValue = new SForce.Avaya.Ivr.SessionHeader();
                        // Set Session Header for request data or services
                        service.SessionHeaderValue.sessionId = rs.sessionId;
                        var result = service.RecordIvrScore(UCID, ANI, QuestionNo.ToString(), Score.ToString());
                        logger.Log(LogLevel.Info,"IVR Process step 2 Status  =>    " + result.Value);
                        return "true";
                    }
                }
                catch (Exception ex1)
                {
                    logger.Log(LogLevel.Info, "Fail to connect salesforce with error : " + ex1.Message);
                    return "false";
                }
            }
            return "false";
        }
    }
}

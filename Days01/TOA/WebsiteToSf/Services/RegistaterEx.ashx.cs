﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteToSf.SForce;
using NLog;
using WebsiteToSf.Util;
using System.Text.RegularExpressions;
using WebsiteToSf.SForce.Toa.RegisterExService;
using System.IO;
using System.Configuration;
using System.Xml.Linq;
using WebsiteToSf.dto;
using WebsiteToSf.DataAccess;
using System.Text;
using WebsiteToSf.MySql;

namespace WebsiteToSf.Services
{
    public class RegistaterEx : IHttpHandler
    {
        private string ns = "http://ii.co.th";
        public void ProcessRequest(HttpContext context)
        {
            var header = context.Request.Headers;
            var logger = LogManager.GetCurrentClassLogger();      // Log Objct
            Boolean IsDoBefore = false;                         // Ch 
            var jobcode = SfUtility.windows874_unicode(context.Request.Form["jobcode"]);// check ว่าเคยทำไปหรือยัง

            #region  Log
            DBConnect db = new DBConnect();
            if (db.Count(jobcode) <= 0)
            {
                /**************************************************
                 * fixed  เวลามี job ใหม่มา
                 * ************************************************/
                StringBuilder deepLogMessage = new StringBuilder();
                StringBuilder httpheaders = new StringBuilder();
                try
                {
                    foreach (string httpkey in header.AllKeys)
                    {
                        httpheaders.AppendLine("Key " + httpkey + " = " + header[httpkey].ToString());
                    }

                    deepLogMessage.AppendLine("==================================");
                    deepLogMessage.AppendLine(" New Register  Ex Data  From TOA  ");
                    deepLogMessage.AppendLine("----------------------------------");
                    deepLogMessage.AppendLine("Date : " + DateTime.Now.ToString("O"));
                    foreach (string key in context.Request.Form.AllKeys)
                    {
                        deepLogMessage.AppendLine("key name : " + key + "||  \t value : " + SfUtility.windows874_unicode(context.Request.Form[key]));
                        if (key == "file1")
                        {
                            var fileurl = ToThai(context, "file1").Trim();
                            string[] files = fileurl.Substring(0, fileurl.Length - 1).Split(',');
                            deepLogMessage.AppendLine("file array size : " + files.Count());
                            int i = 1;
                            for (int j = 0; j < files.Length; j++)
                            {
                                if (files[j].Contains(".zip"))
                                {
                                    deepLogMessage.AppendLine("file zip : " + files[j]);
                                }
                                else
                                {
                                    if (i == 1)
                                    {
                                        deepLogMessage.AppendLine("file " + i + " : " + files[j]);
                                        i++;
                                    }
                                    else if (i == 2)
                                    {
                                        deepLogMessage.AppendLine("file " + i + " : " + files[j]);
                                        i++;
                                    }
                                    else if (i == 3)
                                    {
                                        deepLogMessage.AppendLine("file " + i + " : " + files[j]);
                                        i++;
                                    }
                                    else if (i == 4)
                                    {
                                        deepLogMessage.AppendLine("file " + i + " : " + files[j]);
                                        i++;
                                    }
                                    else if (i == 5)
                                    {
                                        deepLogMessage.AppendLine("file " + i + " : " + files[j]);
                                        i++;
                                    }
                                }
                            }

                        }
                    }

                    db.Insert(jobcode, deepLogMessage.ToString(), httpheaders.ToString());
                }
                catch (Exception ex)
                {
                    logger.Log(LogLevel.Error, "Can't Log Deep Log with error >> " + ex.Message);
                    IsDoBefore = true;
                }
            }
            else
            {
                 IsDoBefore = true;
            }
            #endregion
            try
            {

                var IsDoBefore2 = db.DoneExJobBefore(jobcode);
                logger.Log(LogLevel.Error, "IsDoBefore  " + jobcode + "  For  " + IsDoBefore2 +" Time ");
            }
            catch (Exception ex)
            {
            }
            /* **************************************************
             * เราใช้ StringBuild ในการสร้าง Log Strgin แทนเลย
             * **************************************************/
            StringBuilder logMessage = new StringBuilder();
            logMessage.Append(String.Format("<p> {0} </p>", "New Register Ex"));
            if (!IsDoBefore)
            {
                var dto = new RegisterExDTO();
                try
                {
                    if (context.Request.Form["jobcode"] != null)
                    {
                        dto.jobcode = SfUtility.windows874_unicode(context.Request.Form["jobcode"]);
                    }

                    if (context.Request.Form["f_st2name"] != null)
                    {
                        dto.f_st2name = SfUtility.windows874_unicode(context.Request.Form["f_st2name"]);
                        dto.f_st2name = SfUtility.CollapseSpaces(dto.f_st2name);

                        logMessage.Append(String.Format("<p>f_st2name   :{0} </p>", dto.f_st2name));
                        //logger.Log(LogLevel.Info, "New Register Ex  Name  :  " + dto.f_st2name);

                    }
                    if (context.Request.Form["f_sex"] != null)
                    {
                        dto.f_sex = SfUtility.windows874_unicode(context.Request.Form["f_sex"]);
                        logMessage.Append(String.Format("<p>f_sex   :{0} </p>", dto.f_sex));
                    }

                    if (context.Request.Form["f_birth"] != null)
                    {
                        var tmpDate = context.Request.Form["f_birth"].Split('/').ToArray();
                        if (tmpDate.Length == 3)
                        {
                            dto.f_birth = tmpDate[2] + "-" + tmpDate[1] + "-" + tmpDate[0];
                        }
                        else
                        {
                            DateTime dt = DateTime.Now;
                            dto.f_birth = dt.Year + "-" + dt.Month + "-" + dt.Day;
                        }
                        logMessage.Append(String.Format("<p>f_birth   :{0} </p>", dto.f_birth));
                    }
                    if (context.Request.Form["f_phone"] != null)
                    {
                        dto.f_phone = context.Request.Form["f_phone"];
                        var pattern = @"-";
                        Regex rgx = new Regex(pattern);
                        string result1 = rgx.Replace(dto.f_phone, "");
                        dto.f_phone2 = result1;

                        if (result1.Length == 10)
                        {
                            dto.f_phone = SfUtility.beautifyPhoneNumber(result1, "");
                        }


                        //  logger.Log(LogLevel.Info, "New Register Ex  Phone  :  " + dto.f_phone);
                        logMessage.Append(String.Format("<p>f_phone   :{0} </p>", dto.f_phone));

                    }
                    if (context.Request.Form["f_st2email"] != null)
                    {
                        dto.f_st2email = context.Request.Form["f_st2email"];
                        logMessage.Append(String.Format("<p>f_st2email   :{0} </p>", dto.f_st2email));

                    }
                    if (context.Request.Form["f_province"] != null)
                    {
                        dto.f_province = ToThai(context, "f_province");
                        logMessage.Append(String.Format("<p>f_province   :{0} </p>", dto.f_province));

                    }
                    if (context.Request.Form["f_st2status"] != null)
                    {
                        dto.f_st2status = ToThai(context, "f_st2status");
                        logMessage.Append(String.Format("<p>f_st2status   :{0} </p>", dto.f_st2status));

                    }
                    if (context.Request.Form["f_st3know"] != null)
                    {
                        dto.f_st3know = ToThai(context, "f_st3know");
                        logMessage.Append(String.Format("<p>f_st3know   :{0} </p>", dto.f_st3know));

                    }
                    if (context.Request.Form["f_sent_1"] != null)
                    {
                        dto.f_sent_1 = ToThai(context, "f_sent_1");
                        logMessage.Append(String.Format("<p>f_sent_1   :{0} </p>", dto.f_sent_1));

                    }
                    if (context.Request.Form["f_sent_2"] != null)
                    {
                        dto.f_sent_2 = ToThai(context, "f_sent_2");
                        logMessage.Append(String.Format("<p>f_sent_2   :{0} </p>", dto.f_sent_2));

                    }
                    if (context.Request.Form["f_st2type"] != null)
                    {
                        dto.f_st2type = ToThai(context, "f_st2type");
                        logMessage.Append(String.Format("<p>f_st2type   :{0} </p>", dto.f_st2type));

                    }
                    if (context.Request.Form["f_st2pjtype"] != null)
                    {
                        dto.f_st2pjtype = ToThai(context, "f_st2pjtype");
                        logMessage.Append(String.Format("<p>f_st2pjtype   :{0} </p>", dto.f_st2pjtype));
                    }
                    if (context.Request.Form["f_pd_ex"] != null)
                    {
                        dto.f_pd_ex = ToThai(context, "f_pd_ex");
                        logMessage.Append(String.Format("<p>f_pd_ex   :{0} </p>", dto.f_pd_ex));
                    }
                    if (context.Request.Form["f_pd_in"] != null)
                    {
                        dto.f_pd_in = ToThai(context, "f_pd_in");
                        logMessage.Append(String.Format("<p>f_pd_in   :{0} </p>", dto.f_pd_in));
                    }
                    if (context.Request.Form["f_pd_sp"] != null)
                    {
                        dto.f_pd_sp = ToThai(context, "f_pd_sp");
                        logMessage.Append(String.Format("<p>f_pd_sp   :{0} </p>", dto.f_pd_sp));
                    }
                    if (context.Request.Form["f_req5"] != null)
                    {

                        var f_req5 = ToThai(context, "f_req5");
                        if (!String.IsNullOrEmpty(f_req5))
                            dto.f_req5 = "1";
                        else
                            dto.f_req5 = "0";
                    }
                    else
                    {
                        dto.f_req5 = "0";
                    }
                    logMessage.Append(String.Format("<p>f_req5   :{0} </p>", dto.f_req5));
                    //logger.Log(LogLevel.Info, "dto.f_req5 : " + dto.f_req5);

                    if (context.Request.Form["f_req4"] != null)
                    {
                        dto.f_req4 = ToThai(context, "f_req4");
                        logMessage.Append(String.Format("<p>f_req4   :{0} </p>", dto.f_req4));
                    }

                    if (context.Request.Form["f_pdrequire"] != null)
                    {
                        dto.f_pdrequire = ToThai(context, "f_pdrequire");
                        logMessage.Append(String.Format("<p>f_pdrequire   :{0} </p>", dto.f_pdrequire));
                    }
                    //  logger.Log(LogLevel.Info, "dto.f_req5 : " + dto.f_pdrequire);
                    logMessage.Append(String.Format("<p> {0} </p>", "dto.f_req5 : " + dto.f_pdrequire));
                    if (context.Request.Form["f_pdcolortone"] != null)
                    {
                        dto.f_pdcolortone = ToThai(context, "f_pdcolortone");
                        logMessage.Append(String.Format("<p>f_pdcolortone   :{0} </p>", dto.f_pdcolortone));
                    }
                    //logMessage.Append(String.Format("<p> {0} </p>", "dto.f_req5 : " + dto.f_pdcolortone));
                    //   logger.Log(LogLevel.Info, "dto.f_req5 : " + dto.f_pdcolortone);

                    if (context.Request.Form["f_st3kb"] != null)
                    {
                        dto.f_st3kb = ToThai(context, "f_st3kb");
                        logMessage.Append(String.Format("<p>f_st3kb   :{0} </p>", dto.f_st3kb));
                    }

                    if (context.Request.Form["f_st3kblike"] != null)
                    {
                        dto.f_st3kblike = ToThai(context, "f_st3kblike");
                        logMessage.Append(String.Format("<p>f_st3kblike   :{0} </p>", dto.f_st3kblike));
                    }
                    if (context.Request.Form["f_st3like"] != null)
                    {
                        dto.f_st3like = ToThai(context, "f_st3like");
                        logMessage.Append(String.Format("<p>f_st3like   :{0} </p>", dto.f_st3like));
                    }
                    if (context.Request.Form["f_st3dislike"] != null)
                    {
                        dto.f_st3dislike = ToThai(context, "f_st3dislike");
                        logMessage.Append(String.Format("<p>f_st3dislike   :{0} </p>", dto.f_st3dislike));
                    }


                    if (context.Request.Form["f_other"] != null)
                    {
                        dto.f_other = ToThai(context, "f_other");
                        logMessage.Append(String.Format("<p>f_other   :{0} </p>", dto.f_other));
                    }
                    //logger.Log(LogLevel.Info, "dto.f_req5 : " + dto.f_other);

                    // TOA  ส่ง File  มาเป็น String ที่ Concat กัน
                    if (context.Request.Form["file1"] != null)
                    {
                        var fileurl = ToThai(context, "file1").Trim();
                        string[] files = fileurl.Substring(0, fileurl.Length - 1).Split(',');

                        logMessage.Append(String.Format("<p>files1 size   :{0} </p>", files.Count()));
                        int i = 1;
                        for (int j = 0; j < files.Length; j++)
                        {
                            if (files[j].Contains(".zip"))
                            {
                                dto.FILE_Z = files[j];
                                logMessage.Append(String.Format("<p>FILE_Z   :{0} </p>", dto.FILE_Z));
                                //logger.Log(LogLevel.Info, "FILE_Z : " + dto.FILE_Z);
                            }
                            else
                            {
                                if (i == 1)
                                {
                                    dto.FILE1 = files[j]; i++;
                                    //logger.Log(LogLevel.Info, "FILE1 : " + dto.FILE1);
                                    logMessage.Append(String.Format("<p>FILE1   :{0} </p>", dto.FILE1));
                                }
                                else if (i == 2)
                                {
                                    dto.FILE2 = files[j]; i++;
                                    // logger.Log(LogLevel.Info, "FILE2 : " + dto.FILE2);
                                    logMessage.Append(String.Format("<p>FILE2   :{0} </p>", dto.FILE2));
                                }
                                else if (i == 3)
                                {
                                    dto.FILE3 = files[j]; i++;
                                    // logger.Log(LogLevel.Info, "FILE3 : " + dto.FILE3);
                                    logMessage.Append(String.Format("<p>FILE3   :{0} </p>", dto.FILE3));
                                }
                                else if (i == 4)
                                {
                                    dto.FILE4 = files[j]; i++;
                                    // logger.Log(LogLevel.Info, "FILE4 : " + dto.FILE4);
                                    logMessage.Append(String.Format("<p>FILE4   :{0} </p>", dto.FILE4));
                                }
                                else if (i == 5)
                                {
                                    dto.FILE5 = files[j]; i++;
                                    //logger.Log(LogLevel.Info, "FILE5 : " + dto.FILE5);
                                    logMessage.Append(String.Format("<p>FILE5   :{0} </p>", dto.FILE5));
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                    logMessage.Append(String.Format("<p> Parameter Error >> {0} </p>", ex.Message));
                    logger.Log(LogLevel.Error, logMessage.ToString());
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(ex.Message);
                    context.Response.End();
                }
                // Insert RegisterEx Data
               // db.InsertRegisterEx(dto);

                if (dto.IsValid())
                {
                    try
                    {
                        using (SalesforceServiceProvider sf = new SalesforceServiceProvider())
                        {
                            //Salesforce Login Section 
                            //logger.Log(LogLevel.Info, "Login to Salesforce ");
                            logMessage.Append("<p>Login to Salesforce</p>");
                            LoginResult rs = sf.LoginProduction();
                            ToaRegisterExServiceService service = new ToaRegisterExServiceService();
                            service.SessionHeaderValue = new WebsiteToSf.SForce.Toa.RegisterExService.SessionHeader();
                            // Set Session Header for request data or services
                            service.SessionHeaderValue.sessionId = rs.sessionId;
                            //logger.Log(LogLevel.Info, "EX Send Data ");
                            logMessage.Append("EX Send Data");
                            var result = service.ToaRegisterEx(dto);
                            logMessage.Append("<p> Data : jobcode => " + dto.jobcode + "  f_st2name => " + dto.f_st2name + " Date  => " + dto.f_birth + "  Process Result : " + result + "</p>");
                            logger.Log(LogLevel.Info, "Data : jobcode => " + dto.jobcode + "  f_st2name => " + dto.f_st2name + " Date  => " + dto.f_birth + "  Process Result : " + result);
                            // db.UpdateInsertRegisterExStatus(dto, 1);
                           // db.Insert(jobcode, deepLogMessage.ToString(), httpheaders.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        logMessage.Append("<p>Connect to salesforce error : " + ex.Message + "</p>");
                        logger.Log(LogLevel.Error, logMessage.ToString());
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(ex.Message);
                        context.Response.End();
                    }
                    logger.Log(LogLevel.Info, logMessage.ToString().Replace(System.Environment.NewLine, ""));
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("true");
                    context.Response.End();
                }
                else
                {
                    logger.Log(LogLevel.Info, "ข้อมูลไม่ครบถ้วน");
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("false");
                    context.Response.End();
                }
            }
        }
        public string ToThai(HttpContext context, string key)
        {
            // return context.Request.Form[key];
            return SfUtility.windows874_unicode(context.Request.Form[key]);
        }
        public void InsertExdata(RegisterExDTO dto)
        {

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
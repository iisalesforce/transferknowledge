﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using WebsiteToSf.SForce;

namespace WebsiteToSf.Services
{
    public class SalesforceServiceProvider : IDisposable
    {
        private string _username;
        public string Username
        {
            get
            {
                if (String.IsNullOrEmpty(_username))
                {
                    _username = ConfigurationManager.AppSettings["user"];
                }
                return _username;
            }
        }
        private string _prousername;
        public string ProductUsername
        {
            get
            {
                if (String.IsNullOrEmpty(_prousername))
                {
                    _prousername = ConfigurationManager.AppSettings["productuser"];
                }
                return _prousername;
            }
        }
        private string _productpassword;
        public string Productpassword
        {
            get
            {
                if (String.IsNullOrEmpty(_productpassword))
                {
                    _productpassword = ConfigurationManager.AppSettings["productpassword"];
                }
                return _productpassword;
            }
        }
        private string _tokenPro;
        public string TokenPro
        {
            get
            {
                if (String.IsNullOrEmpty(_tokenPro))
                {
                    _tokenPro = ConfigurationManager.AppSettings["producttoken"];
                }
                return _tokenPro;
            }
        }
        private string _pass;
        public string Password
        {
            get
            {
                if (String.IsNullOrEmpty(_pass))
                {
                    _pass = ConfigurationManager.AppSettings["pass"];
                }
                return _pass;
            }
        }
        private string _token;
        public string Token
        {
            get
            {
                if (String.IsNullOrEmpty(_token))
                {
                    _token = ConfigurationManager.AppSettings["token"];
                }
                return _token;
            }
        }
        private SforceService sf;
        private LoginResult rs;
        public LoginResult Login()
        {
            sf = new SforceService();
            rs = sf.login(Username, Password + Token);
            return rs;
        }
        public LoginResult LoginProduction()
        {
            sf = new SforceService();
            rs = sf.login(ProductUsername, Productpassword + TokenPro);

            return rs;
        }
        public void Dispose()
        {

            sf.Dispose();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteToSf.SForce;
using WebsiteToSf.SForce.Toa.EventService;
using NLog;
using WebsiteToSf.Util;
using System.Text.RegularExpressions;

namespace WebsiteToSf.Services
{
    /// <summary>
    /// Summary description for ToaEvent
    /// </summary>
    public class ToaEvent : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Log(LogLevel.Info, "New Event Register");
            var dto = new EventDTO();

            if (context.Request.Form["Job_Code__c"] != null)
            {
                dto.Job_Code = ToThai(context, "Job_Code__c");
            }
            if (context.Request.Form["name_event__c"] != null)
            {
                dto.name_event = ToThai(context, "name_event__c");
            }
            if (context.Request.Form["phone_event__c"] != null)
            {
                dto.phone_event = context.Request.Form["phone_event__c"];
                var pattern = @"-";
                Regex rgx = new Regex(pattern);
                string result1 = rgx.Replace(dto.phone_event, "");
                if (result1.Length == 10)
                {
                    dto.phone_event = SfUtility.beautifyPhoneNumber(result1, "");
                }
            }
            if (context.Request.Form["age_event__c"] != null)
            {
                dto.age_event = ToThai(context, "age_event__c");
            }
            if (context.Request.Form["email_event__c"] != null)
            {
                dto.email_event = ToThai(context, "email_event__c");
            }

            if (context.Request.Form["building_type__c"] != null)
            {
                dto.building_type = ToThai(context, "building_type__c");
            }
            if (context.Request.Form["Other_type_building__c"] != null)
            {
                dto.Other_type_building = ToThai(context, "Other_type_building__c");
            }
            if (context.Request.Form["floor__c"] != null)
            {
                dto.floor = ToThai(context, "floor__c");
            }
            if (context.Request.Form["project_type__c"] != null)
            {
                dto.project_type = ToThai(context, "project_type__c");
            }
            if (context.Request.Form["construction_process__c"] != null)
            {
                dto.construction_process = ToThai(context, "construction_process__c");
            }
            if (context.Request.Form["ide_color__c"] != null)
            {
                dto.ide_color = ToThai(context, "ide_color__c");
            }

            if (context.Request.Form["source_news__c"] != null)
            {
                dto.source_news = ToThai(context, "source_news__c");
            }
            if (context.Request.Form["magazine__c"] != null)
            {
                dto.magazine = ToThai(context, "magazine__c");
            }
            if (context.Request.Form["other_infomation_color__c"] != null)
            {
                dto.other_infomation_color = ToThai(context, "other_infomation_color__c");
            }
            if (context.Request.Form["other_brand_service__c"] != null)
            {
                dto.other_brand_service = ToThai(context, "other_brand_service__c");
            }
            if (context.Request.Form["service_brand__c"] != null)
            {
                dto.service_brand = ToThai(context, "service_brand__c");
            }
            if (context.Request.Form["other_brand__c"] != null)
            {
                dto.other_brand = ToThai(context, "other_brand__c");
            }
            if (context.Request.Form["information_color__c"] != null)
            {
                dto.information_color = ToThai(context, "information_color__c");
            }
            if (context.Request.Form["website_information_color__c"] != null)
            {
                dto.website_information_color = ToThai(context, "website_information_color__c");
            }

            if (context.Request.Form["magazine_information_color__c"] != null)
            {
                dto.magazine_information_color = ToThai(context, "magazine_information_color__c");
            }
            if (context.Request.Form["tv_information_color__c"] != null)
            {
                dto.tv_information_color = ToThai(context, "tv_information_color__c");
            }
            if (context.Request.Form["other_news__c"] != null)
            {
                dto.other_news = ToThai(context, "other_news__c");
            }
            if (context.Request.Form["designer_name__c"] != null)
            {
                dto.designer_name = ToThai(context, "designer_name__c");
            }
            if (context.Request.Form["requirement_customer__c"] != null)
            {
                dto.requirement_customer = ToThai(context, "requirement_customer__c");
            }
            if (context.Request.Form["product_choose__c"] != null)
            {
                dto.product_choose = ToThai(context, "product_choose__c");
            }

            if (context.Request.Form["shade_source__c"] != null)
            {
                dto.shade_source = ToThai(context, "shade_source__c");
            }

            if (context.Request.Form["date_design__c"] != null)
            {
                var tmpDate = context.Request.Form["date_design__c"].Split('/').ToArray();
                if (tmpDate.Length == 3)
                {
                    dto.date_design = tmpDate[2] + "-" + tmpDate[1] + "-" + tmpDate[0];
                }
                else
                {
                    DateTime dt = DateTime.Now;
                    dto.date_design = dt.Year + "-" + dt.Month + "-" + dt.Day;
                }
            }
            if (context.Request.Form["time_design_hour__c"] != null)
            {
                dto.time_design_hour = ToThai(context, "time_design_hour__c");
            }
            if (context.Request.Form["time_design_minute__c"] != null)
            {
                dto.time_design_minute = ToThai(context, "time_design_minute__c");
            }

            if (!String.IsNullOrEmpty(dto.date_design) && !String.IsNullOrEmpty(dto.name_event))
            {
                try
                {
                    // Salesforce Login Section 
                   
                    using (SalesforceServiceProvider sf = new SalesforceServiceProvider())
                    {
                        LoginResult rs = sf.LoginProduction();
                        ToaEventServiceService service = new ToaEventServiceService();
                        service.SessionHeaderValue = new WebsiteToSf.SForce.Toa.EventService.SessionHeader();
                        // Set Session Header for request data or services
                        service.SessionHeaderValue.sessionId = rs.sessionId;

                        var result = service.ToaEvent(dto);
                        logger.Log(LogLevel.Info,
                            "Data : jobcode => " + dto.Job_Code + "  name_event => " + dto.name_event + " phone_event  => " + dto.phone_event + "  Process Result : " + result);

                    }
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("true");
                    context.Response.End();
                }
                catch (Exception ex)
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(ex.Message);
                    context.Response.End();
                }
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write("false");
            context.Response.End();
        }
        public string ToThai(HttpContext context, string key)
        {
            return SfUtility.windows874_unicode(context.Request.Form[key]);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
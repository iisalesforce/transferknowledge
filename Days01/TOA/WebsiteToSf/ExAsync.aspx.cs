﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteToSf.Services;
using WebsiteToSf.SForce;
using WebsiteToSf.SForce.Toa.RegisterExService;
using WebsiteToSf.Util;

namespace WebsiteToSf
{
    public partial class ExAsync : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            using (SalesforceServiceProvider sf = new SalesforceServiceProvider())
            {

                LoginResult rs = sf.LoginProduction();
                ToaRegisterExServiceService service = new ToaRegisterExServiceService();
                service.SessionHeaderValue = new WebsiteToSf.SForce.Toa.RegisterExService.SessionHeader();
                service.SessionHeaderValue.sessionId = rs.sessionId;
                var dto = new RegisterExDTO();

                dto.jobcode = jobcode.Text;
                dto.f_st2name = f_st2name.Text;
                dto.f_st2name = SfUtility.CollapseSpaces(dto.f_st2name);

                if (!String.IsNullOrEmpty(f_sex.Text))
                    dto.f_sex = f_sex.Text;

                if (!String.IsNullOrEmpty(f_birth.Text))
                {
                    dto.f_birth = f_birth.Text;
                    var tmpDate = f_birth.Text.Split('/').ToArray();
                    if (tmpDate.Length == 3)
                    {
                        dto.f_birth = tmpDate[2] + "-" + tmpDate[1] + "-" + tmpDate[0];
                    }
                    else
                    {
                        DateTime dt = DateTime.Now;
                        dto.f_birth = dt.Year + "-" + dt.Month + "-" + dt.Day;
                    }
                }

                if (!String.IsNullOrEmpty(f_phone.Text))
                {
                    dto.f_phone = f_phone.Text;
                    var pattern = @"-";
                    Regex rgx = new Regex(pattern);
                    string result1 = rgx.Replace(dto.f_phone, "");
                    dto.f_phone2 = result1;

                    if (result1.Length == 10)
                    {
                        dto.f_phone = SfUtility.beautifyPhoneNumber(result1, "");
                    }
                }

                if (!String.IsNullOrEmpty(f_st2email.Text))
                    dto.f_st2email = f_st2email.Text;


                if (!String.IsNullOrEmpty(f_province.Text))
                    dto.f_province = f_province.Text;

                if (!String.IsNullOrEmpty(f_st2status.Text))
                    dto.f_st2status = f_st2status.Text;

                if (!String.IsNullOrEmpty(f_st3know.Text))
                    dto.f_st3know = f_st3know.Text;

                if (!String.IsNullOrEmpty(f_sent_1.Text))
                    dto.f_sent_1 = f_sent_1.Text;

                if (!String.IsNullOrEmpty(f_st2type.Text))
                    dto.f_st2type = f_st2type.Text;

                if (!String.IsNullOrEmpty(f_st2pjtype.Text))
                    dto.f_st2pjtype = f_st2pjtype.Text;

                if (!String.IsNullOrEmpty(f_pd_ex.Text))
                    dto.f_pd_ex = f_pd_ex.Text;

                if (!String.IsNullOrEmpty(f_pd_in.Text))
                    dto.f_pd_in = f_pd_in.Text;

                if (!String.IsNullOrEmpty(f_pd_sp.Text))
                    dto.f_pd_sp = f_pd_sp.Text;

                if (!String.IsNullOrEmpty(f_req5.Text))
                    dto.f_req5 = f_req5.Text;
                else
                    dto.f_req5 = "0";

                if (!String.IsNullOrEmpty(f_req4.Text))
                    dto.f_req4 = f_req4.Text;

                if (!String.IsNullOrEmpty(f_pdrequire.Text))
                    dto.f_pdrequire = f_pdrequire.Text;

                if (!String.IsNullOrEmpty(f_pdcolortone.Text))
                    dto.f_pdcolortone = f_pdcolortone.Text;


                if (!String.IsNullOrEmpty(f_st3kb.Text))
                    dto.f_st3kb = f_st3kb.Text;

                if (!String.IsNullOrEmpty(f_st3kblike.Text))
                    dto.f_st3kblike = f_st3kblike.Text;

                if (!String.IsNullOrEmpty(f_st3like.Text))
                    dto.f_st3like = f_st3like.Text;

                if (!String.IsNullOrEmpty(f_st3dislike.Text))
                    dto.f_st3dislike = f_st3dislike.Text;


                if (!String.IsNullOrEmpty(f_other.Text))
                    dto.f_other = f_other.Text;

                dto.FILE1 = file1.Text;
                dto.FILE2 = file2.Text;
                dto.FILE3 = file3.Text;
                dto.FILE4 = file4.Text;
                dto.FILE5 = file5.Text;
                dto.FILE_Z = filezip.Text;
                var result = service.ToaRegisterEx(dto);
                status.Text = "Complate";
            }
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="monitor.aspx.cs" Inherits="WebsiteToSf.monitor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">SysTem Log</a></li>
            <li><a href="#tabs-2">Web Register EX Detail Log</a></li>
            <li><a href="#tabs-3">Web Register IN Detail Lo</a></li>
        </ul>
        <div id="tabs-1">
            <p>
                <asp:DropDownList ID="ddl" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_SelectedIndexChanged"
                    CausesValidation="True">
                </asp:DropDownList>
                &nbsp;<asp:Button ID="btnDownload" runat="server" Text="Dowdload" 
                    onclick="btnDownload_Click" />
              <%--  <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click1" Text="ล้าง log" />--%>
            </p>
            <asp:GridView ID="grdList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                ForeColor="#333333" GridLines="None" Width="100%">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField AccessibleHeaderText="Time" ItemStyle-VerticalAlign="Top" DataField="time" HeaderText="Time">
                        <HeaderStyle Width="100px" />
                        <ItemStyle Width="150px" />
                    </asp:BoundField>
                    <asp:BoundField AccessibleHeaderText="level" ItemStyle-VerticalAlign="Top" DataField="level" HeaderText="Level">
                        <HeaderStyle Width="100px" />
                        <ItemStyle HorizontalAlign="Center" Width="150px" />
                    </asp:BoundField>
                    <asp:BoundField AccessibleHeaderText="message" HtmlEncode="false" DataField="message" HeaderText="Message">
                        <ItemStyle Width="" />
                    </asp:BoundField>
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
        </div>
        <div id="tabs-2">
            <asp:Literal ID="ltlEx" runat="server"></asp:Literal>
          </div>
        <div id="tabs-3">
           <asp:Literal ID="ltlIn" runat="server"></asp:Literal>
        </div>
    </div>
    <script src="Scripts/jquery-2.0.3.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui-1.10.3.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(function () {
            $("#tabs").tabs();
        });
  
    </script>
</asp:Content>
